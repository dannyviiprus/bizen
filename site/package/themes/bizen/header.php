<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bizen
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&family=Noto+Serif+JP:wght@500&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body class="fadein">
    <div class="loading-bg">
        <div id="loading-icon">
            <img src="<?php echo img_url(); ?>logo.svg">
        </div>
    </div>
    <!-- SITE WRAPPER -->
    <div id="site-wrapper">
    	<!-- HEADER -->
        <header>
            <div id="menu-toggle" class="d-lg-none">
                <a href="javascript:void(0);" id="button-menu" class=""><!--active-->
                    <span></span>
                </a>
            </div>
            <div class="container">
                <div class="top-nav text-center">
                    <div class="logo-section">
                        <span><?php echo get_bloginfo ( 'description' ); ?></span>
                        <h2><a href="<?php echo site_url(); ?>"><img src="<?php echo img_url(); ?>logo.svg" alt="Bizen"></a></h2>
                    </div>
                    <div class="top-nav__links d-flex align-items-end">
                        <div class="top-nav__link--left social-links desktop-content">
                            <a href="https://www.facebook.com/bizen.teijyuu/" target="_blank" rel="nofollow" class="social-icon">
                                <img src="<?php echo img_url(); ?>ico_fb.png" alt="facebook">
                            </a>
                            <a href="https://www.instagram.com/bizen_life/" target="_blank" rel="nofollow" class="social-icon">
                                <img src="<?php echo img_url(); ?>ico_insta.png" alt="instagram">
                            </a>

                            </a>
                            <a href="https://works.do/R/ti/p/toshi@works-218871" target="_blank" rel="nofollow" class="social-icon">
                                <img src="<?php echo img_url(); ?>ico_line.png" alt="line">
                            </a>
                        </div>
                        <div class="top-nav__link--right">
                            <a href="<?php echo !empty(get_field('pdf_download', 'options')) ? get_field('pdf_download', 'options') : '#'  ?>" class="desktop-content border-nav-btn header-btn"><span><?php _e('パンフレットを', 'bizen'); ?> <br>
                            <?php _e('ダウンロード', 'bizen'); ?></span> <svg xmlns="http://www.w3.org/2000/svg" width="43.333" height="31.994" viewBox="0 0 43.333 31.994"> <g id="Group_325" data-name="Group 325" transform="translate(-539.98 -580.682)"> <path id="Path_9" data-name="Path 9" d="M583.314,609.724c-.11.224-.224.446-.33.672a2.852,2.852,0,0,1-3.665,1.534,39.423,39.423,0,0,0-6.04-1.522,18.138,18.138,0,0,0-10.088,1.891,3.349,3.349,0,0,1-3.054.018,19.061,19.061,0,0,0-16.052-.454,2.883,2.883,0,0,1-4.08-2.456,7.068,7.068,0,0,1-.023-.846q0-10.754,0-21.508a4.479,4.479,0,0,1,3.072-4.578,21.327,21.327,0,0,1,17.869.343,1.5,1.5,0,0,0,1.419,0,21.309,21.309,0,0,1,17.945-.305,5.145,5.145,0,0,1,3.027,3.326Zm-22.721.467c.015-.246.037-.441.037-.637,0-7.99-.007-15.98.018-23.97a1.011,1.011,0,0,0-.724-1.059,19.492,19.492,0,0,0-16.165-.173,2.691,2.691,0,0,0-1.823,2.743c.053,7.171.023,14.343.025,21.514,0,1.456.471,1.778,1.83,1.24a19.978,19.978,0,0,1,11.948-1.087C557.352,609.122,558.918,609.691,560.593,610.191Zm2.034.1c.533-.194.927-.332,1.316-.479a20.066,20.066,0,0,1,10.957-1.228,44.654,44.654,0,0,1,4.872,1.377c.964.3,1.465,0,1.524-1,.01-.169,0-.339,0-.508q0-10.672,0-21.344a2.614,2.614,0,0,0-1.865-2.792,19.37,19.37,0,0,0-16.007.179,1.153,1.153,0,0,0-.817,1.246c.027,7.849.017,15.7.017,23.546Z" fill="#fff" /> <path id="Path_10" data-name="Path 10" d="M581.588,674.752a38.209,38.209,0,0,0-12.4-.032c-.227-1.378-.2-1.386,1.074-1.544a36.291,36.291,0,0,1,10.958.152c.109.02.215.053.368.091Z" transform="translate(-24.15 -76.481)" fill="#fff" /> <path id="Path_11" data-name="Path 11" d="M569.2,697.2c-.187-1.368-.192-1.4.986-1.556a38.2,38.2,0,0,1,10.866.081c1,.156.49.86.6,1.489A36.97,36.97,0,0,0,569.2,697.2Z" transform="translate(-24.17 -95.123)" fill="#fff" /> <path id="Path_12" data-name="Path 12" d="M577.672,628.416h-4.521c-.158-.709.213-.995.769-1.214.987-.389,1.01-.519.565-1.5a3.269,3.269,0,0,1-.3-1.042,1.3,1.3,0,0,1,1.23-1.506,1.386,1.386,0,0,1,1.28,1.575,2.806,2.806,0,0,1-.242.8c-.518,1.191-.523,1.188.616,1.716C577.637,627.518,577.7,627.636,577.672,628.416Z" transform="translate(-27.499 -35.253)" fill="#fff" /> <path id="Path_13" data-name="Path 13" d="M616.454,628.449v1.38c-1.624-.143-3.212-.236-4.778-.475-.223-.034-.339-.775-.6-1.416Z" transform="translate(-59.009 -39.22)" fill="#fff" /> <path id="Path_14" data-name="Path 14" d="M612.493,652.03c-.023-.244-.05-.409-.05-.574,0-.195.024-.389.05-.75,1.183.093,2.344.182,3.506.278.167.014.331.065.5.086,1.237.149,1.237.148,1.037,1.484Z" transform="translate(-60.139 -58.115)" fill="#fff" /> <path id="Path_15" data-name="Path 15" d="M691.534,629.916v-1.342a35.7,35.7,0,0,1,12.389.019v1.383A39.417,39.417,0,0,0,691.534,629.916Z" transform="translate(-125.779 -39.305)" fill="#fff" /> <path id="Path_16" data-name="Path 16" d="M704.1,673.362v1.358a38.42,38.42,0,0,0-12.4,0v-1.378A36.188,36.188,0,0,1,704.1,673.362Z" transform="translate(-125.925 -76.466)" fill="#fff" /> <path id="Path_17" data-name="Path 17" d="M691.446,697.047a1.034,1.034,0,0,1-.04-.23c-.008-.309,0-.618,0-1.021a37.473,37.473,0,0,1,12.434.027c.019.278.041.473.043.668s-.017.389-.027.6c-2.1-.139-4.159-.377-6.224-.384C695.581,696.7,693.532,696.922,691.446,697.047Z" transform="translate(-125.669 -95.117)" fill="#fff" /> <path id="Path_18" data-name="Path 18" d="M704.076,652.448a36.441,36.441,0,0,0-12.315-.017V650.9c2.149-.129,4.3-.344,6.462-.361a39.824,39.824,0,0,1,4.887.37C704.3,651.051,704.29,651.114,704.076,652.448Z" transform="translate(-125.968 -57.98)" fill="#fff" /> </g> </svg></a>
                        </div>
                    </div>
                </div>
                <nav id="main-nav">
                    <?php
                    wp_nav_menu(
                    	array(
                    		'theme_location' => is_front_page() ? 'main-menu' : 'main-menu-subpage',
                    		'menu_class' => 'menu d-lg-flex justify-content-between',
                    	)
                    );
                    ?>
                    <a href="<?php echo !empty(get_field('pdf_download', 'options')) ? get_field('pdf_download', 'options') : '#'  ?>" class="mobile-content border-nav-btn header-btn" target="_blank"><?php _e('パンフレットを', 'bizen'); ?> <br>
                    <?php _e('ダウンロード', 'bizen'); ?> <svg xmlns="http://www.w3.org/2000/svg" width="43.333" height="31.994"
                            viewBox="0 0 43.333 31.994">
                            <g id="Group_325" data-name="Group 325" transform="translate(-539.98 -580.682)">
                                <path id="Path_9" data-name="Path 9"
                                    d="M583.314,609.724c-.11.224-.224.446-.33.672a2.852,2.852,0,0,1-3.665,1.534,39.423,39.423,0,0,0-6.04-1.522,18.138,18.138,0,0,0-10.088,1.891,3.349,3.349,0,0,1-3.054.018,19.061,19.061,0,0,0-16.052-.454,2.883,2.883,0,0,1-4.08-2.456,7.068,7.068,0,0,1-.023-.846q0-10.754,0-21.508a4.479,4.479,0,0,1,3.072-4.578,21.327,21.327,0,0,1,17.869.343,1.5,1.5,0,0,0,1.419,0,21.309,21.309,0,0,1,17.945-.305,5.145,5.145,0,0,1,3.027,3.326Zm-22.721.467c.015-.246.037-.441.037-.637,0-7.99-.007-15.98.018-23.97a1.011,1.011,0,0,0-.724-1.059,19.492,19.492,0,0,0-16.165-.173,2.691,2.691,0,0,0-1.823,2.743c.053,7.171.023,14.343.025,21.514,0,1.456.471,1.778,1.83,1.24a19.978,19.978,0,0,1,11.948-1.087C557.352,609.122,558.918,609.691,560.593,610.191Zm2.034.1c.533-.194.927-.332,1.316-.479a20.066,20.066,0,0,1,10.957-1.228,44.654,44.654,0,0,1,4.872,1.377c.964.3,1.465,0,1.524-1,.01-.169,0-.339,0-.508q0-10.672,0-21.344a2.614,2.614,0,0,0-1.865-2.792,19.37,19.37,0,0,0-16.007.179,1.153,1.153,0,0,0-.817,1.246c.027,7.849.017,15.7.017,23.546Z"
                                    fill="#fff" />
                                <path id="Path_10" data-name="Path 10"
                                    d="M581.588,674.752a38.209,38.209,0,0,0-12.4-.032c-.227-1.378-.2-1.386,1.074-1.544a36.291,36.291,0,0,1,10.958.152c.109.02.215.053.368.091Z"
                                    transform="translate(-24.15 -76.481)" fill="#fff" />
                                <path id="Path_11" data-name="Path 11"
                                    d="M569.2,697.2c-.187-1.368-.192-1.4.986-1.556a38.2,38.2,0,0,1,10.866.081c1,.156.49.86.6,1.489A36.97,36.97,0,0,0,569.2,697.2Z"
                                    transform="translate(-24.17 -95.123)" fill="#fff" />
                                <path id="Path_12" data-name="Path 12"
                                    d="M577.672,628.416h-4.521c-.158-.709.213-.995.769-1.214.987-.389,1.01-.519.565-1.5a3.269,3.269,0,0,1-.3-1.042,1.3,1.3,0,0,1,1.23-1.506,1.386,1.386,0,0,1,1.28,1.575,2.806,2.806,0,0,1-.242.8c-.518,1.191-.523,1.188.616,1.716C577.637,627.518,577.7,627.636,577.672,628.416Z"
                                    transform="translate(-27.499 -35.253)" fill="#fff" />
                                <path id="Path_13" data-name="Path 13"
                                    d="M616.454,628.449v1.38c-1.624-.143-3.212-.236-4.778-.475-.223-.034-.339-.775-.6-1.416Z"
                                    transform="translate(-59.009 -39.22)" fill="#fff" />
                                <path id="Path_14" data-name="Path 14"
                                    d="M612.493,652.03c-.023-.244-.05-.409-.05-.574,0-.195.024-.389.05-.75,1.183.093,2.344.182,3.506.278.167.014.331.065.5.086,1.237.149,1.237.148,1.037,1.484Z"
                                    transform="translate(-60.139 -58.115)" fill="#fff" />
                                <path id="Path_15" data-name="Path 15"
                                    d="M691.534,629.916v-1.342a35.7,35.7,0,0,1,12.389.019v1.383A39.417,39.417,0,0,0,691.534,629.916Z"
                                    transform="translate(-125.779 -39.305)" fill="#fff" />
                                <path id="Path_16" data-name="Path 16"
                                    d="M704.1,673.362v1.358a38.42,38.42,0,0,0-12.4,0v-1.378A36.188,36.188,0,0,1,704.1,673.362Z"
                                    transform="translate(-125.925 -76.466)" fill="#fff" />
                                <path id="Path_17" data-name="Path 17"
                                    d="M691.446,697.047a1.034,1.034,0,0,1-.04-.23c-.008-.309,0-.618,0-1.021a37.473,37.473,0,0,1,12.434.027c.019.278.041.473.043.668s-.017.389-.027.6c-2.1-.139-4.159-.377-6.224-.384C695.581,696.7,693.532,696.922,691.446,697.047Z"
                                    transform="translate(-125.669 -95.117)" fill="#fff" />
                                <path id="Path_18" data-name="Path 18"
                                    d="M704.076,652.448a36.441,36.441,0,0,0-12.315-.017V650.9c2.149-.129,4.3-.344,6.462-.361a39.824,39.824,0,0,1,4.887.37C704.3,651.051,704.29,651.114,704.076,652.448Z"
                                    transform="translate(-125.968 -57.98)" fill="#fff" />
                            </g>
                        </svg></a>
                    <div class="mobile-content social-links text-center">
                        <a href="https://www.facebook.com/bizen.teijyuu/" target="_blank" rel="nofollow" class="social-icon">
                            <img src="<?php echo img_url(); ?>ico_fb.png" alt="facebook">
                        </a>
                        <a href="https://www.instagram.com/bizen_life/" target="_blank" rel="nofollow" class="social-icon">
                            <img src="<?php echo img_url(); ?>ico_insta.png" alt="instagram">
                        </a>

                        </a>
                        <a href="https://works.do/R/ti/p/toshi@works-218871" target="_blank" rel="nofollow" class="social-icon">
                            <img src="<?php echo img_url(); ?>ico_line.png" alt="line">
                        </a>
                    </div>
                </nav>
            </div>
        </header>
        <!-- .HEADER -->

        <!-- MAIN -->
        <main id="main-content">
