<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Bizen
 */

get_header();
?>
<?php 
	$wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $term = get_term( $wpseo_primary_term );
    $first_cat = !is_wp_error( $term ) ? $term : '';

    $prev = get_previous_post(true);
    $next = get_next_post(true);
?>
<section id="article-detail" class="content-section-60">
    <div class="container">
        <div class="text-center">
            <div class="entry-meta">
                <span class="item-date"><?php echo get_the_date(); ?></span> <span class="category-tag"><?php echo !empty($first_cat) ? $first_cat->name : 'NEWS'; ?></span>
            </div>
            <h1 class="article-title"><?php the_title(); ?></h1>
        </div>
        <div class="content-detail">
            <div class="content-wrapper">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="post-nav text-center">
            <a href="<?php echo !empty($prev) ? get_permalink($prev->ID) : 'javascript:;' ?>" class="btn-default blue-btn">← &nbsp; &nbsp; &nbsp; BACK</a>
            <a href="<?php echo !empty($next) ? get_permalink($next->ID) : 'javascript:;' ?>" class="btn-default blue-btn">NEXT &nbsp; &nbsp; &nbsp; →</a>
        </div>
        <div class="text-center  mg-btm-50">
            <a href="<?php echo site_url(); ?>" class="btn-default"><?php _e('TOPに戻る', 'bizen'); ?></a>
        </div>
    </div>
</section>

<?php
get_footer();
