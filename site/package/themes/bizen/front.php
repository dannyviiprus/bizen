<?php 

/**
 * TEMPLATE NAME: HOME
 */

get_header();

?>
<?php if (have_rows('main_slider')): ?>
<section id="main-visual">
    <img src="<?php echo img_url(); ?>illustration.svg" class="slider-illustration wow fadeInRight" data-wow-duration="2s">
    <?php if (get_field('poster_image')): ?>
    <img src="<?php the_field( 'poster_image' ); ?>" class="banner-asset wow pulse" data-wow-duration="2s">
	<?php endif; ?>
    <div class="main-slider">
    	<?php while (have_rows('main_slider')): the_row(); ?>
    		<?php if (get_sub_field('image')): ?>
	        <div class="slide-item">
	            <div class="bg bg-30 bg-cover desktop-content" style="background: url(<?php the_sub_field( 'image' ); ?>);">
	            	<?php if (get_sub_field('image')): ?>
	                <span class="slide-text wow fadeInUp" data-wow-duration="3s"><?php the_sub_field( 'slider_text' ); ?></span>
                    <?php endif; ?>
	            </div>
	            <div class="bg bg-30 bg-cover mobile-content" style="background: url(<?php get_sub_field('image_mobile') ? the_sub_field( 'image_mobile' ) : the_sub_field( 'image' );; ?>">
	                <?php if (get_sub_field('image')): ?>
	                <span class="slide-text wow fadeInUp" data-wow-duration="3s"><?php the_sub_field( 'slider_text' ); ?></span>
                    <?php endif; ?>
	            </div>
	        </div>
        	<?php endif; ?>
    	<?php endwhile; ?>
    </div>
</section>
<?php endif; ?>
<section id="pick-up" class="content-section-60">
    <div class="container">
        <div class="section-header wow fadeInUp">
            <h3 class="section-title"><?php _e('MOVIE', 'bizen'); ?></h3>
        </div>
        <div class="pickup-content">
            <div class="row align-items-center">
            	<?php if (get_field('pickup_text')): ?>
                <div class="col-12 col-sm-6 col-lg-3 note-column">
                    <div class="pickup-note posr wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">
                        <img src="<?php echo img_url(); ?>bird.svg" class="posa bird-icon">
                        <img src="<?php echo img_url(); ?>guitar.svg" class="posa guitar-icon">
                        <p><?php the_field( 'pickup_text' ); ?></p>
                    </div>
                </div>
            	<?php endif; ?>
            	<?php 
            		$video_1 = get_field('video_1'); 
            		$video_2 = get_field('video_2'); 
            	?>
            	<?php if (!empty($video_1['embed_url']) || !empty($video_2['embed_url'])): ?>
                <div class="col-lg-9 pl-lg-4">
                    <div class="row">
                    	<?php if (!empty($video_1['embed_url'])): ?>
                        <div class="col-lg-6 wow fadeInUp" data-wow-delay="1s">
                            <div class="video-item">
                                <iframe width="100%" height="216" src="<?php echo $video_1['embed_url']; ?>?controls=0" frameborder="0"
                                allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                                <?php if (!empty($video_1['title'])): ?>
                            		<h4 class="video-title"><?php echo $video_1['title'] ?></h4>
                                <?php endif; ?>
                                <?php if (!empty($video_1['description'])): ?>
                                	<p><?php echo $video_1['description'] ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if (!empty($video_1['embed_url'])): ?>
                        <div class="col-lg-6 wow fadeInUp" data-wow-delay="1s">
                            <div class="video-item">
                                <iframe width="100%" height="216" src="<?php echo $video_2['embed_url']; ?>?controls=0" frameborder="0"
                                    allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <?php if (!empty($video_2['title'])): ?>
                            		<h4 class="video-title"><?php echo $video_2['title'] ?></h4>
                                <?php endif; ?>
                                <?php if (!empty($video_2['description'])): ?>
                                	<p><?php echo $video_2['description'] ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php 
	$latest_news = get_wp_object_query('post', [
		'posts_per_page' => 4,
		'cat' => 2
	]);
?>
<?php if ($latest_news->have_posts()): ?>
<?php $delay = 0.1; ?>
<section id="information" class="content-section-40 section-blue-bg">
    <div class="container">
        <div class="section-header wow fadeInUp">
            <h3 class="section-title"><?php _e('Information', 'bizen'); ?></h3>
        </div>
        <div class="information-list row">
        	<?php while ($latest_news->have_posts()): $latest_news->the_post();?>
            	<?php include_template('content-news-item', ['delay' => $delay]); ?>
            	<?php $delay += 0.1; ?>
        	<?php endwhile; ?>
        	<?php wp_reset_postdata(); ?>
        </div>
        <div class="text-center">
            <a href="<?php echo get_category_link(2); ?>" class="btn-default"><?php _e('もっと見る', 'bizen'); ?></a>
        </div>
    </div>
</section>
<?php endif; ?>
<section id="bizen-city" class="content-section-120">
    <div class="container">
        <div class="intro-section">
            <div class="border-header small-padding wow fadeInUp">
            	<?php if (get_field('small_title')): ?>
                	<p><?php the_field( 'small_title' ); ?></p>
            	<?php endif; ?>
            	<?php 
            		$title_with_icon = get_field('title_with_icon');
            	?>
            	<?php if (!empty($title_with_icon['left_text']) && !empty($title_with_icon['right_text'])): ?>
	                <h4 class="section-title">
	                	<?php echo $title_with_icon['left_text'] . ' &nbsp'; ?> 
	                		<?php if (!empty($title_with_icon['icon'])): ?>
		                	<img src="<?php echo $title_with_icon['icon']; ?>"> 
		            		<?php endif; ?>
		                <?php echo '&nbsp ' . $title_with_icon['right_text']; ?>
	            	</h4>
                <?php endif; ?>
            </div>
        </div>
        <div class="intro-section">
            <div class="section-header-small wow fadeInUp">
            	<?php if (get_field('section_title')): ?>
                <h5 class="section-title"><?php the_field( 'section_title' ); ?></h5>
            	<?php endif; ?>
            </div>
            <div class="intro-content">
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-6 wow fadeInLeft map-col" data-wow-duration="2s">
    	            	<?php if (get_field('map')): ?>
	                        <img src="<?php the_field( 'map' ); ?>" alt="map">
                    	<?php endif; ?>
                    </div>
                    <div class="map-info col-lg-6 wow fadeInRight" data-wow-duration="2s">
                    	<?php if (get_field('map_infomation')): ?>
	                        <?php the_field( 'map_infomation' ); ?>
                    	<?php endif; ?>
                        <div class="info-boxes">
                            <div class="row align-items-stretch">
                                <div class="col-12 col-sm-4 wow fadeInUp">
                                	<?php if (get_field( 'map_info_box_1' )): ?>
                                    <div class="info-box blue-box text-center">
                                        <?php the_field( 'map_info_box_1' ); ?>
                                    </div>
                                	<?php endif; ?>
                                </div>
                                <div class="col-12 col-sm-8 wow fadeInUp">
                                	<?php if (get_field( 'map_info_box_2' )): ?>
                                    <div class="info-box info-box blue-box ">
                                        <?php the_field( 'map_info_box_2' ); ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            $yt_banner_mobile = get_field('youtube_banner_mobile');
            $yt_banner = !is_mobile() 
                ? get_field('youtube_banner') 
                : ($yt_banner_mobile ? $yt_banner_mobile : get_field('youtube_banner'));
        ?>
        <?php if ($yt_banner): ?>
            <div class="youtube-banner">
                <?php if (get_field('youtube_banner_link')): ?>
                    <a href="<?php the_field('youtube_banner_link'); ?>" target="_blank">
                <?php endif; ?>
                    <img src="<?php echo $yt_banner; ?>" alt="youtube banner">
                <?php if (get_field('youtube_banner_link')): ?>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="intro-section">
            <div class="section-header-small wow fadeInUp">
                <h5 class="section-title"><?php the_field( 'information_boxes_title' ); ?></h5>
            </div>
            <?php if (have_rows('information_boxes')): ?>
            <div class="detail-boxes">
                <div class="row">
                	<?php while (have_rows('information_boxes')): the_row(); ?>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="info-box <?php echo get_sub_field('box_background') ? 'blue-box' : ''; ?> text-center wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.2s">
                        	<?php if (!empty(get_sub_field('icon'))): ?>
                            	<img src="<?php the_sub_field( 'icon' ); ?>" class="posa <?php echo get_sub_field('icon_class') ?: ''; ?>">
                            <?php endif; ?>
                            <?php if (get_sub_field('info_text')): ?>
                            	<span><?php the_sub_field( 'info_text' ); ?></span>
                        	<?php endif; ?>
                        </div>
                    </div>
                	<?php endwhile; ?>
                </div>
            </div>
        	<?php endif; ?>
        </div>
    </div>
</section>
<?php 
	$interview = get_wp_object_query('post', [
		'posts_per_page' => 4,
		'cat' => 3
	]);
?>
<?php if ($interview->have_posts()): ?>
<section id="bizen-life" class="content-section-40 section-blue-bg">
    <div class="container">
        <div class="border-header wow fadeInUp">
            <p><?php echo get_cat_name(3);  ?></p>
            <h4 class="section-title"><?php _e('備前市での生活ってどうですか？', 'bizen'); ?></h4>
        </div>
        <div class="life-slider">
        	<?php while ($interview->have_posts()): $interview->the_post();?>
            <div class="slider-item">
                <div class="bg bg-cover bg-100 posr" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>)">
                	<?php if (get_field('area')): ?>
                    <div class="circle-stamp posa <?php the_field( 'area_color' ); ?>-color">
                        <span>
                            <?php the_field( 'area' ); ?>
                            <small><?php _e( 'エリア', 'bizen' ); ?></small>
                        </span>    
                    </div>
                	<?php endif; ?>
                </div>
                <h5><?php the_title(); ?></h5>
                <p><?php echo get_the_excerpt(); ?> <br> <?php the_field( 'more_information' ); ?></p>
            </div>
       	 <?php endwhile; ?>
       	 <?php wp_reset_postdata(); ?>
        </div>
        <div class="text-center">
            <a href="<?php echo get_category_link(3); ?>" class="btn-default"><?php _e('もっと見る', 'bizen'); ?></a>
        </div>
    </div>
</section>
<?php endif; ?>
<section id="how-to" class="content-section-100">
    <div class="container">
        <div class="border-header wow fadeInUp">
        	<?php if (get_field('small_title_1')): ?>
            	<p><?php the_field( 'small_title_1' ); ?></p>
        	<?php endif; ?>
        	<?php if (get_field('section_title_1')): ?>
            	<h4 class="section-title"><?php the_field( 'section_title_1' ); ?></h4>
            <?php endif; ?>
        </div>
        <div class="section-header-small wow fadeInUp">
        	<?php if (get_field('section_title_en')): ?>
            	<span><?php the_field( 'section_title_en' ); ?></span>
            <?php endif; ?>
            <?php if (get_field('section_title_2')): ?>
            	<h5 class="section-title"><?php the_field( 'section_title_2' ); ?></h5>
            <?php endif; ?>
        </div>
        <div class="how-to-section posr wow fadeInUp">
            <img src="<?php echo img_url(); ?>store.svg" class="icon-store posa wow fadeIn" data-wow-delay="1.2s" data-wow-duration="3s">
            <div class="row">
                <div class="posa line-draw line4"></div>
                <div class="col-lg-4">
                    <div class="how-to-step step-1 posr">
                        <div class="step-icon posa wow pulse" data-wow-duration="2s" data-wow-delay="0.4s">
                            <div class="wrapper"><?php _e('STEP', 'bizen'); ?><span>1</span></div>
                        </div>
                        <h5><?php the_field( 'step_1_title' ); ?></h5>
                        <span><?php the_field( 'description_1' ); ?></span>
                        <div class="posa line-draw line1"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="how-to-step step-2 posr">
                        <div class="step-icon posa wow pulse" data-wow-duration="2s" data-wow-delay="0.6s">
                            <div class="wrapper"><?php _e('STEP', 'bizen'); ?><span>2</span></div>
                        </div>
                        <h5><?php the_field( 'step_2_title' ); ?></h5>
                        <span><?php the_field( 'description_2' ); ?></span>
                        <div class="posa line-draw line2"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="how-to-step step-3 posr">
                        <div class="step-icon posa wow pulse" data-wow-duration="2s" data-wow-delay="0.8s">
                            <div class="wrapper"><?php _e('STEP', 'bizen'); ?><span>3</span></div>
                        </div>
                        <h5><?php the_field( 'step_3_title' ); ?></h5>
                        <span><?php the_field( 'description_3' ); ?></span>
                        <div class="posa line-draw line3"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="how-to-step step-4 posr">
                        <div class="step-icon posa wow pulse" data-wow-duration="2s" data-wow-delay="1s">
                            <div class="wrapper"><?php _e('STEP', 'bizen'); ?><span>4</span></div>
                        </div>
                        <h5><?php the_field( 'step_4_title' ); ?></h5>
                        <span><?php the_field( 'description_4' ); ?></span>
                        <div class="posa line-draw line5"></div>
                        <div class="posa line-draw line6"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="how-to-step step-5 posr">
                        <div class="step-icon posa wow pulse" data-wow-duration="2s" data-wow-delay="1.2s">
                            <div class="wrapper"><?php _e('STEP', 'bizen'); ?><span>5</span></div>
                        </div>
                        <h5><?php the_field( 'step_5_title' ); ?></h5>
                        <span><?php the_field( 'description_5' ); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="support" class="content-section-100">
    <div class="container">
        <div class="section-header-small wow fadeInUp">
            <h5 class="section-title">支援・補助</h5>
        </div>
        <div class="support-content">
            <div class="row">
                <div class="col-lg-3 section-links wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                    <div class="section-link-title">
                        <span><?php _e('住まい', 'bizen'); ?> <br>
                        <small><?php _e('HOUSE', 'bizen'); ?></small></span>
                        <img src="<?php echo img_url(); ?>icon_house.svg" class="title-icon i-house posa">
                    </div>
                    <?php 
                    	$links = get_field('house_links'); 
                    	include_template('content-link-item', ['links' => $links['links']]); 
                	?>
                </div>
                <div class="col-lg-3 section-links wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
                    <div class="section-link-title">
                        <span><?php _e('仕事', 'bizen'); ?> <br>
                        <small><?php _e('WORK', 'bizen'); ?></small></span>
                        <img src="<?php echo img_url(); ?>icon_work.svg" class="title-icon i-work posa">
                    </div>
					<?php 
                    	$links = get_field('work_links'); 
                    	include_template('content-link-item', ['links' => $links['links']]); 
                	?>
                </div>
                <div class="col-lg-3 section-links wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
                    <div class="section-link-title">
                        <span><?php _e('暮らし', 'bizen'); ?> <br>
                        <small><?php _e('LIFE', 'bizen'); ?></small></span>
                        <img src="<?php echo img_url(); ?>icon_life.svg" class="title-icon i-life posa">
                    </div>
                    <?php 
                    	$links = get_field('life_links'); 
                    	include_template('content-link-item', ['links' => $links['links']]); 
                	?>
                </div>
                <div class="col-lg-3 section-links wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">
                    <div class="section-link-title">
                        <span><?php _e('子育て', 'bizen'); ?> <br>
                        <small><?php _e('CHILD', 'bizen'); ?></small></span>
                        <img src="<?php echo img_url(); ?>icon_child.svg" class="title-icon i-child posa">
                    </div>
                    <?php 
                    	$links = get_field('childs_links'); 
                    	include_template('content-link-item', ['links' => $links['links']]); 
                	?>
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo css_url(); ?>slick.css">
<link rel="stylesheet" href="<?php echo css_url(); ?>slick-theme.css">
<script src="<?php echo js_inc_url(); ?>slick.min.js"></script>
<script>
    $(function(){
        $('.main-slider').slick({
            arrows: false,
            dots: true,
            fade: true,
            autoplay: true,
            autoplaySpeed: 4000,
            infinite: true,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 415,
                    settings: {
                        dots: false
                    }
                },
            ]
        });
        $('.life-slider').slick({
            arrows: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 6000,
            infinite: true,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
            ]
        });
    });
</script>
<?php 

get_footer();