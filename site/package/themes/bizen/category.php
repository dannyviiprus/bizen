<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bizen
 */

get_header();
?>


<?php
	is_category(3)  ? include_template('category-interview') : include_template('category-information');
?>

<?php
get_footer();
