var mediaQueries = window.matchMedia('(max-width: 768px)');
$(function(){

    /*INIT WOW AND SMOOTH SCROLL*/
    new WOW().init();

    /*DO SCROLLTOP*/
    $('#scrolltop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;	
    });

    /*MENU TOGGLE ON MOBILE*/
    var nav = $('#main-nav');
    $('#button-menu').on('click',function(){
        $(this).toggleClass('active');
        $(nav).stop().fadeToggle();
    });

    /*ONSCROLL EVENT*/
    var menu = $('header');
    var offset = menu.offset().top;
    $(window).scroll(function () {
        /*ADD CLASS TO FIXED HEADER*/
        doHeaderScroll(menu);
        /*SCROLLTOP*/
        if ($(this).scrollTop() > 500) {
            $('#scrolltop').fadeIn();
        } else {
            $('#scrolltop').fadeOut('500');
        }
    });

    /*ADD SMOOTH SCROLL TO SECTION*/
    $("a").on('click', function(event) {
        if (this.hash !== "" && this && $(this).data('toggle') == null && !$(this).closest('li').hasClass('no-anchor')) {
          event.preventDefault();
          var hash = this.hash;
          $('html, body').animate({
            scrollTop: $(hash).offset().top - (mediaQueries.matches ? 76 : 176)
          }, 800, function(){
            // window.location.hash = hash;
          });
        }
    });
    
    $('#menu-main-menu').on('click', 'a', function(){
        mediaQueries.matches && $('#button-menu').click();
    });
    
});

$('#scrolltop').hide();

/*LOADING BACKGROUND*/
$(window).on('load', function(){
    $('.loading-bg').fadeOut(650);
    scrollTarget(mediaQueries);
});

/*KEEP SCROLL POSITION*/
/*$(window).scroll(function() {
  sessionStorage.scrollTop = $(this).scrollTop();
});

$(document).ready(function() {
  if (sessionStorage.scrollTop != "undefined") {
    $(window).scrollTop(sessionStorage.scrollTop);
  }
});*/

function doHeaderScroll(el){
    var sticky = $(el),
        scroll = $(window).scrollTop();

    if (scroll >= sticky.height()) {
        sticky.addClass('scrolled');
        $('body').addClass('scrolled-header');
    }else {
        sticky.removeClass('scrolled');  
        $("body").removeClass("scrolled-header");
    } 
}

function scrollTarget(mediaQueries) {
  let hash = window.location.hash.substr(1);
  if (hash != '') {
    const target = $("#main-content").find("#" + hash);
    console.log("#" + hash);
    if (target.length) {
      $("html, body").animate(
        {
          scrollTop: $(target).offset().top - (mediaQueries.matches ? 76 : 176),
        },
        800,
        function () {
          // window.location.hash = hash;
        }
      );
    }
  }
}
