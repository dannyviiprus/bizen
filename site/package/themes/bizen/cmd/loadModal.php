<?php

$result = loadModal();

function loadModal(){
	$result = array();
	$content = '';
	
	$args = array(
	    'p'    => 398,
	    'post_type' => 'work'
	);
	$work = get_posts($args);

	foreach ( $work as $p ) : 
		$gallery = get_field('image_gallery', $p->ID);
		$size = 'full';
		$content .= '<div class="main-info">';
			$content .= '<div class="row">';
				$content .= '<div class="col-md-8">';
					if( $gallery ): 
					$content .= '<div class="detail-slide">';
					    foreach( $gallery as $image ):
					    	$img_src = wp_get_attachment_image_src( $image['ID'], $size );
				            $content .= '<div class="slide-item">';
				            	$content .= '<div class="main-img">';
				            		$content .= '<div class="bg bg-cover bg-50" style="background: url(' . $img_src[0] . ');"></div>';
				            	$content .= '</div>';
				            $content .= '</div>';
					    endforeach; 
					$content .= '</div>';
					endif;
				$content .= '</div>';
				$content .= '<div class="info col-md-4">';
					$content .= '<h3 class="customer-name">' . get_the_title($p->ID) . '</h3>';
					if(have_rows('attributes' , $p->ID)):
						while(have_rows('attributes' , $p->ID)): the_row();
							$content .= '<div class="detail-row row">';
								$content .= '<div class="col-xs-6 col-sm-3">';
									$content .= '<p>' . get_sub_field('attribute_name') . '</p>';
								$content .= '</div>';
								$content .= '<div class="col-xs-6 col-sm-3">';
									$content .= '<p>' . get_sub_field('attribute_value') . '</p>';
								$content .= '</div>';
							$content .= '</div>';
						endwhile;
					endif;
					$desc = $p->post_content;
					$desc = apply_filters('the_content', $desc);
					$desc = str_replace(']]>', ']]&gt;', $desc);
					$content .= '<p class="description">' . $desc . '</p>';
				$content .= '</div>';
			$content .= '</div>';
		$content .= '</div>';
		if( $gallery ): 
		$content .= '<div class="detail-slider-nav">';
		    foreach( $gallery as $image ):
		    	$img_src = wp_get_attachment_image_src( $image['ID'], $size );
	            $content .= '<div class="slide-item">';
            		$content .= '<div class="bg bg-cover bg-60" style="background: url(' . $img_src[0] . ');"></div>';
	            $content .= '</div>';
		    endforeach; 
		$content .= '</div>';
		endif;

	endforeach; 

	if(!empty($content)){
		$result['result'] = 1;
		$result['data'] = $content;
		
	}

	return json_encode($result);
}

