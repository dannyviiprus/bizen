<?php 
	$latest_news = get_wp_object_query('post', [
		'posts_per_page' => get_option('posts_per_page'),
		'cat' => 2,
	]);
?>
<?php if ($latest_news->have_posts()): ?>
<?php $delay = 0.1; ?>
<section id="information-list" class="content-section-40">
    <div class="container">
        <div class="section-header wow fadeInUp">
            <h3 class="section-title"><?php _e('Information', 'bizen'); ?></h3>
        </div>
        <div class="information-list row">
        	<?php while ($latest_news->have_posts()): $latest_news->the_post();?>
            	<?php include_template('content-news-item', ['delay' => $delay]); ?>
            	<?php $delay += 0.1; ?>
        	<?php endwhile; ?>
        	<?php wp_reset_postdata(); ?>
        </div>
        <div class="text-center">
            <div class="pagination ">
                <?php pagination($latest_news->max_num_pages); ?>
            </div>
        </div>
        <div class="text-center  mg-btm-50">
            <a href="<?php echo site_url(); ?>" class="btn-default"><?php _e('TOPに戻る', 'bizen'); ?></a>
        </div>
    </div>
</section>
<?php endif; ?>