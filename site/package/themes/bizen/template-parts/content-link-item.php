<?php $links = !empty($args['links']) ? $args['links'] : [];  ?>
<?php if (!empty($links)): ?>
<div class="links">
	<?php foreach($links as $link_item): ?>
		<?php if (!empty($link_item['link']['url'] && !empty($link_item['link']['title']))): ?>
			<a href="<?php echo $link_item['link']['url']; ?>" <?php echo $link_item['link']['target'] ? 'target="_blank"' : ''; ?>><?php echo $link_item['link']['title']; ?> <span class="arrow-icon">→</span></a>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
<?php endif; ?>