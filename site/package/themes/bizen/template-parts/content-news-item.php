<?php
	$delay = !empty($args['delay']) ? $args['delay'] : '0.1';
	$wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $term = get_term( $wpseo_primary_term );
    $first_cat = !is_wp_error( $term ) ? $term : '';
?>
<div class="col-6 col-sm-6 col-lg-3 information-item wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
    <a href="<?php the_permalink() ?>">
        <div class="item-img">
            <div class="bg bg-cover bg-70" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full' ); ?>) no-repeat center;"></div>
        </div>
        <div class="entry-meta">
            <span class="item-date"><?php echo get_the_date() ?></span> <span class="category-tag"><?php echo !empty($first_cat) ? $first_cat->name : 'NEWS'; ?></span>
        </div>
        <h4 class="item-title"><?php the_title(); ?></h4>
    </a>
</div>
