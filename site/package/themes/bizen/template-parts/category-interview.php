<?php 
	$interview = get_wp_object_query('post', [
        'cat' => 3
    ]);
?>
<?php if ($interview->have_posts()): ?>
    <section id="interview" class="content-section-80">
        <div class="container">
            <div class="border-header wow fadeInUp">
                <p><?php echo single_cat_title(); ?></p>
                <h4 class="section-title"><?php _e('備前市での生活ってどうですか？', 'bizen'); ?></h4>
            </div>
            <div class="interview-list">
                <?php while ($interview->have_posts()): $interview->the_post();?>
                <div class="interview-item wow fadeInUp">
                    <div class="row align-items-center">
                        <div class="col-12 col-lg-6 img-col">
                            <?php if (has_post_thumbnail()): ?>
                            <div class="featured-img">
                                <div class="bg bg-cover bg-60" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full')?>);"></div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-lg-6 info-col">
                            <div class="interview-header">
                                <?php if (get_field('area')): ?>
                                <div class="circle-stamp posa <?php the_field( 'area_color' ); ?>-color">
                                    <span>
                                        <?php the_field( 'area' ); ?>
                                        <small><?php _e('エリア', 'bizen'); ?></small>
                                    </span>
                                </div>
                                <?php endif; ?>
                                <span><?php echo get_the_excerpt(); ?> <br>
                                    <?php the_title(); ?> <?php the_field( 'more_information' ); ?></span>
                            </div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="text-center">
                <div class="pagination ">
                    <?php pagination($interview->max_num_pages); ?>
                </div>
            </div>
            <div class="text-center  mg-btm-50">
                <a href="<?php echo site_url(); ?>" class="btn-default"><?php _e('TOPに戻る', 'bizen'); ?></a>
            </div>
        </div>
    </section>
<?php endif; ?>