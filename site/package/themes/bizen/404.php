<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Bizen
 */

get_header();
?>

<section class="content-section-100">
	<div class="container">
		<div class="border-header wow fadeInUp">
            <h1 class="section-title"><?php _e( '404 Page not found.', 'bizen' ); ?></h1>
        </div>
        <div class="text-center">
        	<h4><?php esc_html_e( 'Nothing was found at this location, please try again', 'bizen' ); ?></h4>
        </div>
	</div>
</section>

<?php
get_footer();
