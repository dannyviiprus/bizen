<?php

/**
 * NEW BOOTSTRAP & FONTAWESOME
 * EMBED THESE STYLESHEET & SCRIPT TO HEADER
 */

/*<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>*/

/**
 * Enqueue styles
 */
function enqueue_styles() {
	wp_enqueue_style('style-anim', css_url() . 'animate.min.css', false, '1', 'all');
	wp_enqueue_style('style-theme', css_url() . 'style.css', false, '1', 'all');
	wp_enqueue_style('style-slick', css_url() . 'slick.css', true, '1', 'all');
	wp_enqueue_style('style-slick-theme', css_url() . 'slick-theme.css', true, '1', 'all');
	wp_enqueue_style('fancybox', css_url() . 'jquery.fancybox.css', true, '1', 'all');
	// wp_enqueue_style('style-theme-min', css_url() . 'style.min.css', false, '1', 'all');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

/**
 * Enqueue scripts
 */
function enqueue_scripts() {
	wp_enqueue_script('script-wow', js_inc_url() . 'wow.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-main', js_url() . 'app.js', array('jquery'), '1', true);
	wp_enqueue_script('script-wow', js_inc_url() . 'smart-resize.js', array('jquery'), '1', true);
	wp_enqueue_script('script-slick', js_inc_url() . 'slick.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-fancybox', js_inc_url() . 'jquery.fancybox.pack.js', array('jquery'), '1', true);
	/*wp_enqueue_script('script-tweenmax','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', array('jquery'), '1', true);
	wp_enqueue_script('script-scrollto','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js', array('jquery'), '1', true);*/
	// wp_enqueue_script('script-main-min', js_url() . 'app.min.js', array('jquery'), '1', false);
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');
