<?php

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.svg) !important;
    		width:192px;
    		height:40px;
    		background-size: 192px 40px;
    		background-repeat: no-repeat;
        	padding-bottom: 20px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function get_the_title_with_code($post_id){
    global $wpdb;
    $title = $wpdb->get_var( $wpdb->prepare( "SELECT post_title FROM $wpdb->posts WHERE ID = %d", $post_id));
    return $title;
}

function get_term_title_with_code($term_id){
    global $wpdb;
    $title = $wpdb->get_var( $wpdb->prepare( "SELECT name FROM $wpdb->terms WHERE term_id = %d", $term_id));
    return $title;
}

function get_the_slug( $id=null ){
  if( empty($id) ):
    global $post;
    if( empty($post) )
      return ''; // No global $post var available.
    $id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}

/**
 * Display the page or post slug
 *
 * Uses get_the_slug() and applies 'the_slug' filter.
 */
function the_slug( $id=null ){
  echo apply_filters( 'the_slug', get_the_slug($id) );
}

function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

/*CUSTOM LANGUAGES BAR STYLE*/
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #qtranxs-meta-box-lsb {
      position: fixed;
      z-index: 999999;
      top: 0;
      right: 92px;
      background: none;
      border: none;
    } 
    #qtranxs-meta-box-lsb .postbox .handlediv{
      display: none;
    }
  </style>';
}

/**
 * ADD CUSTOM POST TYPE TO SEARCH
 */
add_action('pre_get_posts', 'djg_includ_my_cpt_in_query', 99);
function djg_includ_my_cpt_in_query($query){
    if(is_search()) :              // Ensure you only alter your desired query
        $post_types = $query->get('post_type');             // Get the currnet post types in the query
        if(!is_array($post_types) && !empty($post_types))   // Check that the current posts types are stored as an array
            $post_types = explode(',', $post_types);
        if(empty($post_types))                              // If there are no post types defined, be sure to include posts so that they are not ignored
            $post_types[] = 'company';         
        $post_types[] = 'company';                         // Add your custom post type
        $post_types = array_map('trim', $post_types);       // Trim every element, just in case
        $post_types = array_filter($post_types);            // Remove any empty elements, just in case
        unset($post_types[0]);
        $query->set('post_type', $post_types);              // Add the updated list of post types to your query
    endif; 
    return $query;
}

/*SHOW EMAIL ERROR*/
function action_wp_mail_failed($wp_error) 
{
    pre_result($wp_error, true);
}
          
// add the action 
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);

/*CUSTOM FILE UPLOAD NAME*/
function dvpz_custom_filename($dir, $name, $ext){
    $key = wp_rand();
    return $key.'.'.$name.$ext;
}

/**
 * GET IMAGE URL
 */
function img_url($path = ''){
  if(empty($path))
    return THEME_URL . IMG_PATH;
  else
    return THEME_URL . $path;
}

/**
 * GET JS URL
 */
function js_url($path = ''){
  if(empty($path))
    return THEME_URL . JS_PATH;
  else
    return THEME_URL . $path;
}

/**
 * GET INC JS URL
 */
function js_inc_url($path = ''){
  if(empty($path))
    return THEME_URL . JS_INC_PATH;
  else
    return THEME_URL . $path;
}

/**
 * GET CSS URL
 */
function css_url($path = ''){
  if(empty($path))
    return THEME_URL . CSS_PATH;
  else
    return THEME_URL . $path;
}

/**
 * GET THUMB URL WITH SIZE
 */
function thumb_url($post_id, $size = 'full', $is_display = 1){
  if(!$is_display)
    return get_the_post_thumbnail_url($post_id, $size);
  else
    echo get_the_post_thumbnail_url($post_id, $size);
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_shortcode ('year', function() {
  return get_the_date('Y');
});

add_filter('apply_year_copyright', function ($content) {
    ob_start();
    $content = do_shortcode($content);
    ob_clean();

    return $content;
});

function get_wp_object_query($post_type = 'post', $custom_args = [])
{
    $default_args = [
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => !get_option('posts_per_page') ? get_option('posts_per_page') : -1,
        'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
        'orderby' => 'date',
        'order' => 'DESC'
    ];

    return new \WP_Query(!empty($custom_args) ? wp_parse_args($custom_args, $default_args) : $default_args);
}

function include_template($name, $data = []) {
    return get_template_part( 'template-parts/' . $name, '', $data );
}
function pagination($max_num_page, $args = '')
{
    if (empty($max_num_page)) {
        return;
    }

    $big = 999999999;

    $args = wp_parse_args($args, [
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $max_num_page,
        'end_size' => 1,
        'mid_size' => 2,
        'prev_next' => true,
        'prev_text' => __('&lt;'),
        'next_text' => __('&gt;'),
    ]);
    echo paginate_links($args);
}

function interview_pre_get_posts( $query ) {
    if ( is_category( 3 ) && is_main_query() )
        $query->set( 'posts_per_page', 4 );
}
add_action( 'pre_get_posts', 'interview_pre_get_posts' );

function is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}