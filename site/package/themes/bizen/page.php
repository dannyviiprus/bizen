<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bizen
 */

get_header();
?>

<section class="content-section-100">
	<div class="container">
		<div class="border-header wow fadeInUp">
            <h1 class="section-title"><?php the_title(); ?></h1>
        </div>
        <?php the_content(); ?>
	</div>
</section>

<?php
get_footer();
