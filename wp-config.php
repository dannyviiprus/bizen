<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bizen_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'artO6G] #NyN>*i3lP-HCKCo9(OHA/n7h%l)atjD<TUm%:D8&mI$@|/MvBe}D[wD' );
define( 'SECURE_AUTH_KEY',  '; }%z$wVO<)S}lse n6@9jZ:Sd -SdO;?fSz0a,~lhz8or_]5,3WGM V-mR/4J~I' );
define( 'LOGGED_IN_KEY',    '<;clp9 *eQiy?[9_FEdhfC:[j{_e&(1]*_mk*w4Si>H1O+0Z,Yv9 Q4$WN-d@K6`' );
define( 'NONCE_KEY',        ';7aks)xL_Is=3SbrY.aeS]4Ct^/R*sCDALi9J$k;ka?s[vuMq*L7`x!Jc{GOsKU8' );
define( 'AUTH_SALT',        '?a-KgN?}%M5js!op9O?qvL],:rxv,U@o;{y]-TC~!LM->HB;spy&e7(vA%dL4;HV' );
define( 'SECURE_AUTH_SALT', 'cbQ`TQ41:M`LJw(bu@jzK-kV#J[&P|~BL=ckHt==}bJ`#CTD5kFy_6JYv@sK^$eg' );
define( 'LOGGED_IN_SALT',   'ghq&JI#D@58FS]ol=fa`ZJ!%4Ru|}%VAFRvf4.alLXq^^2J43+XfqFicZDwI5cNH' );
define( 'NONCE_SALT',       'UQ@ y|)kF!8<^CxIZ>hk_.-B4-}*9lUv)7rIM0gjm%l8e+Rg&+{*S]cmL8I=;Y5k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bzn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-includes/const.php' );
require_once ABSPATH . 'wp-settings.php';
