-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2021 at 06:14 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bizen_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bzn_actionscheduler_actions`
--

CREATE TABLE `bzn_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_actionscheduler_actions`
--

INSERT INTO `bzn_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(10, 'action_scheduler/migration_hook', 'complete', '2021-02-03 17:42:56', '2021-02-03 17:42:56', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1612374176;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1612374176;}', 1, 1, '2021-02-03 17:43:04', '2021-02-03 17:43:04', 0, NULL),
(11, 'wp_mail_smtp_admin_notifications_update', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '{\"tasks_meta_id\":1}', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 2, 1, '2021-02-03 17:43:08', '2021-02-03 17:43:08', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_actionscheduler_claims`
--

CREATE TABLE `bzn_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_actionscheduler_groups`
--

CREATE TABLE `bzn_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_actionscheduler_groups`
--

INSERT INTO `bzn_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wp_mail_smtp');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_actionscheduler_logs`
--

CREATE TABLE `bzn_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_actionscheduler_logs`
--

INSERT INTO `bzn_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 10, 'action created', '2021-02-03 17:41:57', '2021-02-03 17:41:57'),
(2, 10, 'action started via WP Cron', '2021-02-03 17:43:04', '2021-02-03 17:43:04'),
(3, 10, 'action complete via WP Cron', '2021-02-03 17:43:04', '2021-02-03 17:43:04'),
(4, 11, 'action created', '2021-02-03 17:43:05', '2021-02-03 17:43:05'),
(5, 11, 'action started via Async Request', '2021-02-03 17:43:07', '2021-02-03 17:43:07'),
(6, 11, 'action complete via Async Request', '2021-02-03 17:43:08', '2021-02-03 17:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_commentmeta`
--

CREATE TABLE `bzn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_comments`
--

CREATE TABLE `bzn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_bans`
--

CREATE TABLE `bzn_itsec_bans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `host` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ip',
  `created_at` datetime NOT NULL,
  `actor_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actor_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_distributed_storage`
--

CREATE TABLE `bzn_itsec_distributed_storage` (
  `storage_id` bigint(20) UNSIGNED NOT NULL,
  `storage_group` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `storage_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `storage_chunk` int(11) NOT NULL DEFAULT '0',
  `storage_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `storage_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_fingerprints`
--

CREATE TABLE `bzn_itsec_fingerprints` (
  `fingerprint_id` bigint(20) UNSIGNED NOT NULL,
  `fingerprint_user` bigint(20) UNSIGNED NOT NULL,
  `fingerprint_hash` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fingerprint_created_at` datetime NOT NULL,
  `fingerprint_approved_at` datetime NOT NULL,
  `fingerprint_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fingerprint_snapshot` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fingerprint_last_seen` datetime NOT NULL,
  `fingerprint_uses` int(11) NOT NULL DEFAULT '0',
  `fingerprint_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fingerprint_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_geolocation_cache`
--

CREATE TABLE `bzn_itsec_geolocation_cache` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_host` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_lat` decimal(10,8) NOT NULL,
  `location_long` decimal(11,8) NOT NULL,
  `location_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_credit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_lockouts`
--

CREATE TABLE `bzn_itsec_lockouts` (
  `lockout_id` bigint(20) UNSIGNED NOT NULL,
  `lockout_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lockout_start` datetime NOT NULL,
  `lockout_start_gmt` datetime NOT NULL,
  `lockout_expire` datetime NOT NULL,
  `lockout_expire_gmt` datetime NOT NULL,
  `lockout_host` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lockout_user` bigint(20) UNSIGNED DEFAULT NULL,
  `lockout_username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lockout_active` int(1) NOT NULL DEFAULT '1',
  `lockout_context` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_logs`
--

CREATE TABLE `bzn_itsec_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'notice',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `init_timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memory_current` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `memory_peak` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `blog_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `remote_ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_itsec_logs`
--

INSERT INTO `bzn_itsec_logs` (`id`, `parent_id`, `module`, `code`, `data`, `type`, `timestamp`, `init_timestamp`, `memory_current`, `memory_peak`, `url`, `blog_id`, `user_id`, `remote_ip`) VALUES
(1, 0, 'notification_center', 'send::hide-backend', 'a:2:{s:10:\"recipients\";a:1:{i:0;s:26:\"dev.tranthanhduy@gmail.com\";}s:7:\"subject\";s:45:\"[bizen.local] WordPress Login Address Changed\";}', 'debug', '2021-02-03 17:46:13', '2021-02-03 17:46:13', 45082248, 45170320, 'http://bizen.local/wp-admin/admin-ajax.php', 1, 1, '127.0.0.1'),
(2, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_USER_AGENT\";s:33:\"WordPress/5.6; http://bizen.local\";s:11:\"HTTP_ACCEPT\";s:3:\"*/*\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"deflate, gzip\";s:12:\"HTTP_REFERER\";s:28:\"http://bizen.local/not_found\";s:18:\"HTTP_CACHE_CONTROL\";s:8:\"no-cache\";s:15:\"HTTP_CONNECTION\";s:5:\"close\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612976916.45;s:12:\"REQUEST_TIME\";i:1612976916;}}', 'notice', '2021-02-10 17:08:37', '2021-02-10 17:08:36', 38909224, 39066384, 'http://bizen.local/not_found', 1, 0, '127.0.0.1'),
(3, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977199.124;s:12:\"REQUEST_TIME\";i:1612977199;}}', 'notice', '2021-02-10 17:13:21', '2021-02-10 17:13:20', 38904304, 39013664, 'http://bizen.local/assets/images/illustration.svg', 1, 1, '127.0.0.1'),
(4, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:3:\"*/*\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977199.108;s:12:\"REQUEST_TIME\";i:1612977199;}}', 'notice', '2021-02-10 17:13:21', '2021-02-10 17:13:20', 38904208, 39013568, 'http://bizen.local/assets/js/inc/slick.min.js', 1, 1, '127.0.0.1'),
(5, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977199.125;s:12:\"REQUEST_TIME\";i:1612977199;}}', 'notice', '2021-02-10 17:13:21', '2021-02-10 17:13:20', 38881384, 38990752, 'http://bizen.local/assets/images/bird.svg', 1, 1, '127.0.0.1'),
(6, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977201.157;s:12:\"REQUEST_TIME\";i:1612977201;}}', 'notice', '2021-02-10 17:13:24', '2021-02-10 17:13:22', 38865600, 38974960, 'http://bizen.local/assets/images/guitar.svg', 1, 1, '127.0.0.1'),
(7, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977201.639;s:12:\"REQUEST_TIME\";i:1612977201;}}', 'notice', '2021-02-10 17:13:25', '2021-02-10 17:13:23', 38865552, 38974920, 'http://bizen.local/assets/images/hand.svg', 1, 1, '127.0.0.1'),
(8, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977202.034;s:12:\"REQUEST_TIME\";i:1612977202;}}', 'notice', '2021-02-10 17:13:25', '2021-02-10 17:13:23', 38901184, 39010552, 'http://bizen.local/assets/images/baby.svg', 1, 1, '127.0.0.1'),
(9, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977201.952;s:12:\"REQUEST_TIME\";i:1612977201;}}', 'notice', '2021-02-10 17:13:25', '2021-02-10 17:13:23', 38865552, 38974920, 'http://bizen.local/assets/images/man.svg', 1, 1, '127.0.0.1'),
(10, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:3:\"*/*\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.016;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:25', 38865504, 38974864, 'http://bizen.local/assets/js/inc/slick.min.js', 1, 1, '127.0.0.1'),
(11, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.497;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:26', 38901184, 39010552, 'http://bizen.local/assets/images/text.svg', 1, 1, '127.0.0.1'),
(12, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.642;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:26', 38920136, 39029496, 'http://bizen.local/assets/images/icon_house.svg', 1, 1, '127.0.0.1'),
(13, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.575;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:26', 38904288, 39013656, 'http://bizen.local/assets/images/house.svg', 1, 1, '127.0.0.1'),
(14, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.696;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:26', 38904304, 39013664, 'http://bizen.local/assets/images/icon_work.svg', 1, 1, '127.0.0.1'),
(15, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977205.576;s:12:\"REQUEST_TIME\";i:1612977205;}}', 'notice', '2021-02-10 17:13:27', '2021-02-10 17:13:26', 38862512, 38971880, 'http://bizen.local/assets/images/store.svg', 1, 1, '127.0.0.1'),
(16, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977207.494;s:12:\"REQUEST_TIME\";i:1612977207;}}', 'notice', '2021-02-10 17:13:29', '2021-02-10 17:13:28', 38917064, 39026424, 'http://bizen.local/assets/images/icon_life.svg', 1, 1, '127.0.0.1'),
(17, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:50:\"image/avif,image/webp,image/apng,image/*,*/*;q=0.8\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977207.887;s:12:\"REQUEST_TIME\";i:1612977207;}}', 'notice', '2021-02-10 17:13:29', '2021-02-10 17:13:28', 38878360, 38987720, 'http://bizen.local/assets/images/icon_child.svg', 1, 1, '127.0.0.1'),
(18, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:3:\"*/*\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977226.053;s:12:\"REQUEST_TIME\";i:1612977226;}}', 'notice', '2021-02-10 17:13:47', '2021-02-10 17:13:46', 38865504, 38974864, 'http://bizen.local/assets/js/inc/slick.min.js', 1, 1, '127.0.0.1'),
(19, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1612977686.924;s:12:\"REQUEST_TIME\";i:1612977686;}}', 'notice', '2021-02-10 17:21:27', '2021-02-10 17:21:27', 38863808, 38973384, 'http://bizen.local/information-list.html', 1, 1, '127.0.0.1'),
(20, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:16:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:18:\"HTTP_CACHE_CONTROL\";s:9:\"max-age=0\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:55:\"http://bizen.local/wp-admin/post.php?post=7&action=edit\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613150650.755;s:12:\"REQUEST_TIME\";i:1613150650;}}', 'notice', '2021-02-12 17:24:12', '2021-02-12 17:24:11', 38914896, 39072056, 'http://bizen.local/not_found', 1, 0, '127.0.0.1'),
(21, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:16:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:18:\"HTTP_CACHE_CONTROL\";s:9:\"max-age=0\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 OPR/72.0.3815.487\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:56:\"http://bizen.local/wp-admin/post.php?post=31&action=edit\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613150653.731;s:12:\"REQUEST_TIME\";i:1613150653;}}', 'notice', '2021-02-12 17:24:16', '2021-02-12 17:24:15', 38918000, 39075160, 'http://bizen.local/not_found', 1, 0, '127.0.0.1'),
(22, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:14:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_USER_AGENT\";s:33:\"WordPress/5.6; http://bizen.local\";s:11:\"HTTP_ACCEPT\";s:3:\"*/*\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"deflate, gzip\";s:12:\"HTTP_REFERER\";s:28:\"http://bizen.local/not_found\";s:18:\"HTTP_CACHE_CONTROL\";s:8:\"no-cache\";s:15:\"HTTP_CONNECTION\";s:5:\"close\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613150660.976;s:12:\"REQUEST_TIME\";i:1613150660;}}', 'notice', '2021-02-12 17:24:22', '2021-02-12 17:24:21', 38910472, 39067632, 'http://bizen.local/not_found', 1, 0, '127.0.0.1'),
(23, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:19:\"http://bizen.local/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613150944.978;s:12:\"REQUEST_TIME\";i:1613150944;}}', 'notice', '2021-02-12 17:29:06', '2021-02-12 17:29:06', 38866256, 38975784, 'http://bizen.local/information-detail.html', 1, 1, '127.0.0.1'),
(24, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:40:\"http://bizen.local/category/information/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613320435.701;s:12:\"REQUEST_TIME\";i:1613320435;}}', 'notice', '2021-02-14 16:33:57', '2021-02-14 16:33:56', 39182384, 39270472, 'http://bizen.local/category/information/page/2/', 1, 1, '127.0.0.1'),
(25, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:31:\"http://bizen.local/information/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613320785.972;s:12:\"REQUEST_TIME\";i:1613320785;}}', 'notice', '2021-02-14 16:39:46', '2021-02-14 16:39:46', 38926232, 39014752, 'http://bizen.local/information/page/2/', 1, 1, '127.0.0.1'),
(26, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:28:\"http://bizen.local/sample-4/\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613322366.445;s:12:\"REQUEST_TIME\";i:1613322366;}}', 'notice', '2021-02-14 17:06:07', '2021-02-14 17:06:06', 38908912, 39016360, 'http://bizen.local/sample-4/index.html', 1, 1, '127.0.0.1'),
(27, 0, 'four_oh_four', 'found_404', 'a:1:{s:6:\"SERVER\";a:15:{s:18:\"HTTP_AUTHORIZATION\";s:0:\"\";s:9:\"HTTP_HOST\";s:11:\"bizen.local\";s:15:\"HTTP_CONNECTION\";s:10:\"keep-alive\";s:30:\"HTTP_UPGRADE_INSECURE_REQUESTS\";s:1:\"1\";s:15:\"HTTP_USER_AGENT\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:11:\"HTTP_ACCEPT\";s:135:\"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\";s:12:\"HTTP_REFERER\";s:42:\"http://bizen.local/?page_id=3&preview=true\";s:20:\"HTTP_ACCEPT_ENCODING\";s:13:\"gzip, deflate\";s:20:\"HTTP_ACCEPT_LANGUAGE\";s:14:\"en-US,en;q=0.9\";s:14:\"REQUEST_SCHEME\";s:4:\"http\";s:15:\"SCRIPT_FILENAME\";s:37:\"D:/Tools/xampp/htdocs/bizen/index.php\";s:15:\"SERVER_PROTOCOL\";s:8:\"HTTP/1.1\";s:14:\"REQUEST_METHOD\";s:3:\"GET\";s:18:\"REQUEST_TIME_FLOAT\";d:1613322680.904;s:12:\"REQUEST_TIME\";i:1613322680;}}', 'notice', '2021-02-14 17:11:21', '2021-02-14 17:11:21', 38908872, 39016368, 'http://bizen.local/index.html', 1, 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_mutexes`
--

CREATE TABLE `bzn_itsec_mutexes` (
  `mutex_id` bigint(20) UNSIGNED NOT NULL,
  `mutex_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutex_expires` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_opaque_tokens`
--

CREATE TABLE `bzn_itsec_opaque_tokens` (
  `token_id` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_hashed` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_temp`
--

CREATE TABLE `bzn_itsec_temp` (
  `temp_id` bigint(20) UNSIGNED NOT NULL,
  `temp_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_date` datetime NOT NULL,
  `temp_date_gmt` datetime NOT NULL,
  `temp_host` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temp_user` bigint(20) UNSIGNED DEFAULT NULL,
  `temp_username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_itsec_temp`
--

INSERT INTO `bzn_itsec_temp` (`temp_id`, `temp_type`, `temp_date`, `temp_date_gmt`, `temp_host`, `temp_user`, `temp_username`) VALUES
(1, 'four_oh_four', '2021-02-10 17:08:36', '2021-02-10 17:08:36', '127.0.0.1', NULL, NULL),
(2, 'four_oh_four', '2021-02-10 17:13:20', '2021-02-10 17:13:20', '127.0.0.1', NULL, NULL),
(3, 'four_oh_four', '2021-02-10 17:13:20', '2021-02-10 17:13:20', '127.0.0.1', NULL, NULL),
(4, 'four_oh_four', '2021-02-10 17:13:20', '2021-02-10 17:13:20', '127.0.0.1', NULL, NULL),
(5, 'four_oh_four', '2021-02-10 17:13:22', '2021-02-10 17:13:22', '127.0.0.1', NULL, NULL),
(6, 'four_oh_four', '2021-02-10 17:13:23', '2021-02-10 17:13:23', '127.0.0.1', NULL, NULL),
(7, 'four_oh_four', '2021-02-10 17:13:23', '2021-02-10 17:13:23', '127.0.0.1', NULL, NULL),
(8, 'four_oh_four', '2021-02-10 17:13:23', '2021-02-10 17:13:23', '127.0.0.1', NULL, NULL),
(9, 'four_oh_four', '2021-02-10 17:13:25', '2021-02-10 17:13:25', '127.0.0.1', NULL, NULL),
(10, 'four_oh_four', '2021-02-10 17:13:26', '2021-02-10 17:13:26', '127.0.0.1', NULL, NULL),
(11, 'four_oh_four', '2021-02-10 17:13:26', '2021-02-10 17:13:26', '127.0.0.1', NULL, NULL),
(12, 'four_oh_four', '2021-02-10 17:13:26', '2021-02-10 17:13:26', '127.0.0.1', NULL, NULL),
(13, 'four_oh_four', '2021-02-10 17:13:26', '2021-02-10 17:13:26', '127.0.0.1', NULL, NULL),
(14, 'four_oh_four', '2021-02-10 17:13:26', '2021-02-10 17:13:26', '127.0.0.1', NULL, NULL),
(15, 'four_oh_four', '2021-02-10 17:13:28', '2021-02-10 17:13:28', '127.0.0.1', NULL, NULL),
(16, 'four_oh_four', '2021-02-10 17:13:28', '2021-02-10 17:13:28', '127.0.0.1', NULL, NULL),
(17, 'four_oh_four', '2021-02-10 17:13:46', '2021-02-10 17:13:46', '127.0.0.1', NULL, NULL),
(18, 'four_oh_four', '2021-02-10 17:21:27', '2021-02-10 17:21:27', '127.0.0.1', NULL, NULL),
(19, 'four_oh_four', '2021-02-12 17:24:11', '2021-02-12 17:24:11', '127.0.0.1', NULL, NULL),
(20, 'four_oh_four', '2021-02-12 17:24:15', '2021-02-12 17:24:15', '127.0.0.1', NULL, NULL),
(21, 'four_oh_four', '2021-02-12 17:24:21', '2021-02-12 17:24:21', '127.0.0.1', NULL, NULL),
(22, 'four_oh_four', '2021-02-12 17:29:06', '2021-02-12 17:29:06', '127.0.0.1', NULL, NULL),
(23, 'four_oh_four', '2021-02-14 16:33:56', '2021-02-14 16:33:56', '127.0.0.1', NULL, NULL),
(24, 'four_oh_four', '2021-02-14 16:39:46', '2021-02-14 16:39:46', '127.0.0.1', NULL, NULL),
(25, 'four_oh_four', '2021-02-14 17:06:06', '2021-02-14 17:06:06', '127.0.0.1', NULL, NULL),
(26, 'four_oh_four', '2021-02-14 17:11:21', '2021-02-14 17:11:21', '127.0.0.1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_itsec_user_groups`
--

CREATE TABLE `bzn_itsec_user_groups` (
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_roles` text COLLATE utf8mb4_unicode_ci,
  `group_canonical` text COLLATE utf8mb4_unicode_ci,
  `group_users` text COLLATE utf8mb4_unicode_ci,
  `group_min_role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_itsec_user_groups`
--

INSERT INTO `bzn_itsec_user_groups` (`group_id`, `group_label`, `group_roles`, `group_canonical`, `group_users`, `group_min_role`, `group_created_at`) VALUES
('0d48cc63-1c71-4dba-95b8-bf0f47230fe1', 'Author Users', '', 'author', '', '', '2021-02-03 17:41:52'),
('15724da0-3df5-405e-9085-189dbe936334', 'Editor Users', '', 'editor', '', '', '2021-02-03 17:41:52'),
('35780d7b-de17-4778-bd5d-5049ac615d2d', 'Subscriber Users', '', 'subscriber', '', '', '2021-02-03 17:41:56'),
('38b2b553-840c-48d0-aee4-55c5643cc1fe', 'Contributor Users', '', 'contributor', '', '', '2021-02-03 17:41:53'),
('7b75724c-64a1-493f-901d-667d1384d1d4', 'Editor Users', '', 'editor', '', '', '2021-02-03 17:41:56'),
('8fc0ea70-9dd3-49e8-a7dc-f85712662c3d', 'Administrator Users', '', 'administrator', '', '', '2021-02-03 17:41:52'),
('b500e0ff-1766-46c0-96b8-8a203e975550', 'Author Users', '', 'author', '', '', '2021-02-03 17:41:56'),
('c58a1f79-7751-422b-9e25-7f27a31736bc', 'Administrator Users', '', 'administrator', '', '', '2021-02-03 17:41:56'),
('cf2bbd74-1369-4e81-86f9-deb0e2b03b7b', 'Contributor Users', '', 'contributor', '', '', '2021-02-03 17:41:56'),
('e67d52e6-e9dc-42a0-bc4f-4622532c6642', 'Subscriber Users', '', 'subscriber', '', '', '2021-02-03 17:41:53');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_links`
--

CREATE TABLE `bzn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_options`
--

CREATE TABLE `bzn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_options`
--

INSERT INTO `bzn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://bizen.local', 'yes'),
(2, 'home', 'http://bizen.local', 'yes'),
(3, 'blogname', 'Bizen', 'yes'),
(4, 'blogdescription', '備前市移住ガイド', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'dev.tranthanhduy@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '2', 'yes'),
(19, 'default_comment_status', '', 'yes'),
(20, 'default_ping_status', '', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '20', 'yes'),
(23, 'date_format', 'Y.m.d', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:41:\"better-wp-security/better-wp-security.php\";i:2;s:33:\"duplicate-post/duplicate-post.php\";i:3;s:24:\"wordpress-seo/wp-seo.php\";i:4;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'bizen', 'yes'),
(41, 'stylesheet', 'bizen', 'yes'),
(42, 'comment_registration', '', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '0', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', '', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:1:{s:41:\"better-wp-security/better-wp-security.php\";a:2:{i:0;s:10:\"ITSEC_Core\";i:1;s:16:\"handle_uninstall\";}}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '7', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1627925725', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'initial_db_version', '49752', 'yes'),
(99, 'bzn_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:63:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:39:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;s:10:\"copy_posts\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:10:\"copy_posts\";b:1;}}}', 'yes'),
(100, 'fresh_site', '0', 'yes'),
(101, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"header-widget\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(107, 'cron', 'a:10:{i:1613322835;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1613324131;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1613324132;a:4:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1613324174;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1613324176;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1613324511;a:2:{s:13:\"wpseo-reindex\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:31:\"wpseo_permalink_structure_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1613345013;a:1:{s:15:\"itsec_cron_test\";a:1:{s:32:\"48a1cf9920c459f77d7ed9df72afbecf\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:1613345013;}}}}i:1613583711;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1613669731;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(108, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:0:{}', 'yes'),
(119, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.6.1-partial-0.zip\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.1\";s:7:\"version\";s:5:\"5.6.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:3:\"5.6\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.1-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.6.1-partial-0.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.1-rollback-0.zip\";}s:7:\"current\";s:5:\"5.6.1\";s:7:\"version\";s:5:\"5.6.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:3:\"5.6\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1613319996;s:15:\"version_checked\";s:3:\"5.6\";s:12:\"translations\";a:0:{}}', 'no'),
(120, 'theme_mods_twentytwentyone', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1612373806;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(126, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1613319997;s:7:\"checked\";a:1:{s:5:\"bizen\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(135, 'can_compress_scripts', '1', 'no'),
(146, 'current_theme', 'Bizen', 'yes'),
(147, 'theme_mods_bizen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:9:\"main-menu\";i:4;s:17:\"main-menu-subpage\";i:7;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(151, 'finished_updating_comment_type', '1', 'yes'),
(154, 'WPLANG', '', 'yes'),
(155, 'new_admin_email', 'dev.tranthanhduy@gmail.com', 'yes'),
(166, 'recently_activated', 'a:0:{}', 'yes'),
(186, 'itsec-storage', 'a:9:{s:6:\"global\";a:35:{s:15:\"lockout_message\";s:5:\"error\";s:20:\"user_lockout_message\";s:64:\"You have been locked out due to too many invalid login attempts.\";s:25:\"community_lockout_message\";s:77:\"Your IP address has been flagged as a threat by the iThemes Security network.\";s:9:\"blacklist\";b:1;s:15:\"blacklist_count\";i:3;s:16:\"blacklist_period\";i:7;s:14:\"lockout_period\";i:15;s:18:\"lockout_white_list\";a:0:{}s:12:\"log_rotation\";i:60;s:17:\"file_log_rotation\";i:180;s:8:\"log_type\";s:8:\"database\";s:12:\"log_location\";s:71:\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/ithemes-security/logs\";s:8:\"log_info\";s:0:\"\";s:14:\"allow_tracking\";b:0;s:11:\"write_files\";b:1;s:10:\"nginx_file\";s:38:\"D:\\Tools\\xampp\\htdocs\\bizen/nginx.conf\";s:24:\"infinitewp_compatibility\";b:0;s:11:\"did_upgrade\";b:0;s:9:\"lock_file\";b:0;s:5:\"proxy\";s:9:\"automatic\";s:12:\"proxy_header\";s:20:\"HTTP_X_FORWARDED_FOR\";s:14:\"hide_admin_bar\";b:0;s:16:\"show_error_codes\";b:0;s:19:\"show_security_check\";b:0;s:5:\"build\";i:4121;s:13:\"initial_build\";i:4121;s:20:\"activation_timestamp\";i:1612374072;s:11:\"cron_status\";i:1;s:8:\"use_cron\";b:1;s:14:\"cron_test_time\";i:1613345013;s:19:\"enable_grade_report\";b:0;s:10:\"server_ips\";a:1:{i:0;s:9:\"127.0.0.1\";}s:13:\"feature_flags\";a:0:{}s:12:\"manage_group\";a:0:{}s:24:\"licensed_hostname_prompt\";b:0;}s:11:\"user-groups\";a:1:{s:14:\"default_groups\";a:5:{s:13:\"administrator\";s:36:\"8fc0ea70-9dd3-49e8-a7dc-f85712662c3d\";s:6:\"editor\";s:36:\"15724da0-3df5-405e-9085-189dbe936334\";s:6:\"author\";s:36:\"0d48cc63-1c71-4dba-95b8-bf0f47230fe1\";s:11:\"contributor\";s:36:\"38b2b553-840c-48d0-aee4-55c5643cc1fe\";s:10:\"subscriber\";s:36:\"e67d52e6-e9dc-42a0-bc4f-4622532c6642\";}}s:18:\"security-check-pro\";a:6:{s:19:\"last_scan_timestamp\";N;s:15:\"remote_ip_index\";N;s:13:\"ssl_supported\";N;s:20:\"remote_ips_timestamp\";N;s:10:\"remote_ips\";a:0:{}s:8:\"key_salt\";s:60:\"m>;!:oV~4SFT^C3&376[BI71A=IkKvu&%Tv+=j9(dJgWEwNB(DWq[BA4T Tw\";}s:21:\"password-requirements\";a:2:{s:20:\"enabled_requirements\";a:1:{s:8:\"strength\";b:1;}s:20:\"requirement_settings\";a:1:{s:8:\"strength\";a:1:{s:5:\"group\";a:6:{i:0;s:36:\"8fc0ea70-9dd3-49e8-a7dc-f85712662c3d\";i:1;s:36:\"15724da0-3df5-405e-9085-189dbe936334\";i:2;s:36:\"0d48cc63-1c71-4dba-95b8-bf0f47230fe1\";i:3;s:36:\"38b2b553-840c-48d0-aee4-55c5643cc1fe\";i:4;s:36:\"e67d52e6-e9dc-42a0-bc4f-4622532c6642\";i:5;s:14:\"everybody-else\";}}}}s:16:\"wordpress-tweaks\";a:13:{s:18:\"wlwmanifest_header\";b:1;s:14:\"edituri_header\";b:1;s:12:\"comment_spam\";b:1;s:11:\"file_editor\";b:1;s:14:\"disable_xmlrpc\";i:2;s:22:\"allow_xmlrpc_multiauth\";b:0;s:8:\"rest_api\";s:15:\"restrict-access\";s:21:\"valid_user_login_type\";s:4:\"both\";s:26:\"patch_thumb_file_traversal\";b:1;s:12:\"login_errors\";b:0;s:21:\"force_unique_nicename\";b:0;s:27:\"disable_unused_author_pages\";b:0;s:16:\"block_tabnapping\";b:0;}s:19:\"notification-center\";a:8:{s:10:\"from_email\";s:0:\"\";s:18:\"default_recipients\";a:1:{s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}}s:13:\"notifications\";a:4:{s:6:\"backup\";a:2:{s:7:\"subject\";N;s:10:\"email_list\";a:1:{i:0;s:26:\"dev.tranthanhduy@gmail.com\";}}s:11:\"file-change\";a:4:{s:7:\"enabled\";b:1;s:7:\"subject\";N;s:14:\"recipient_type\";s:7:\"default\";s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}}s:6:\"digest\";a:5:{s:7:\"subject\";N;s:8:\"schedule\";s:5:\"daily\";s:14:\"recipient_type\";s:7:\"default\";s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}s:7:\"enabled\";b:0;}s:7:\"lockout\";a:4:{s:7:\"enabled\";b:1;s:7:\"subject\";N;s:14:\"recipient_type\";s:7:\"default\";s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}}}s:9:\"last_sent\";a:0:{}s:4:\"data\";a:0:{}s:9:\"resend_at\";a:0:{}s:12:\"admin_emails\";a:0:{}s:15:\"last_mail_error\";s:0:\"\";}s:11:\"file-change\";a:4:{s:9:\"file_list\";a:0:{}s:5:\"types\";a:39:{i:0;s:4:\".log\";i:1;s:3:\".mo\";i:2;s:3:\".po\";i:3;s:4:\".bmp\";i:4;s:4:\".gif\";i:5;s:4:\".ico\";i:6;s:4:\".jpe\";i:7;s:5:\".jpeg\";i:8;s:4:\".jpg\";i:9;s:4:\".png\";i:10;s:4:\".psd\";i:11;s:4:\".raw\";i:12;s:4:\".svg\";i:13;s:4:\".tif\";i:14;s:5:\".tiff\";i:15;s:4:\".aif\";i:16;s:5:\".flac\";i:17;s:4:\".m4a\";i:18;s:4:\".mp3\";i:19;s:4:\".oga\";i:20;s:4:\".ogg\";i:22;s:3:\".ra\";i:23;s:4:\".wav\";i:24;s:4:\".wma\";i:25;s:4:\".asf\";i:26;s:4:\".avi\";i:27;s:4:\".mkv\";i:28;s:4:\".mov\";i:29;s:4:\".mp4\";i:30;s:4:\".mpe\";i:31;s:5:\".mpeg\";i:32;s:4:\".mpg\";i:33;s:4:\".ogv\";i:34;s:3:\".qt\";i:35;s:3:\".rm\";i:36;s:4:\".vob\";i:37;s:5:\".webm\";i:38;s:3:\".wm\";i:39;s:4:\".wmv\";}s:15:\"expected_hashes\";a:1:{s:37:\"D:/Tools/xampp/htdocs/bizen/.htaccess\";s:32:\"8bcfcf678968b0832b9298acc1a407b7\";}s:9:\"last_scan\";i:0;}s:13:\"system-tweaks\";a:10:{s:13:\"protect_files\";b:1;s:18:\"directory_browsing\";b:1;s:24:\"suspicious_query_strings\";b:1;s:17:\"write_permissions\";b:1;s:11:\"uploads_php\";b:1;s:11:\"plugins_php\";b:1;s:10:\"themes_php\";b:1;s:15:\"request_methods\";b:0;s:22:\"non_english_characters\";b:0;s:16:\"long_url_strings\";b:0;}s:12:\"hide-backend\";a:6:{s:7:\"enabled\";b:1;s:4:\"slug\";s:10:\"bizenlogin\";s:12:\"theme_compat\";b:1;s:17:\"theme_compat_slug\";s:9:\"not_found\";s:16:\"post_logout_slug\";s:0:\"\";s:8:\"register\";s:13:\"wp-signup.php\";}}', 'yes'),
(187, 'wp_mail_smtp_initial_version', '2.5.1', 'no'),
(188, 'wp_mail_smtp_version', '2.5.1', 'no'),
(189, 'wp_mail_smtp', 'a:2:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:26:\"dev.tranthanhduy@gmail.com\";s:9:\"from_name\";s:5:\"Bizen\";s:6:\"mailer\";s:4:\"mail\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:2:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;}}', 'no'),
(190, 'wp_mail_smtp_activated_time', '1612374078', 'no'),
(191, 'wp_mail_smtp_activated', 'a:1:{s:4:\"lite\";i:1612374078;}', 'yes'),
(192, 'wpseo', 'a:42:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";b:0;s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:1;s:19:\"indexing_first_time\";b:1;s:16:\"indexing_started\";b:0;s:15:\"indexing_reason\";s:21:\"category_base_changed\";s:29:\"indexables_indexing_completed\";b:1;s:7:\"version\";s:4:\"15.7\";s:16:\"previous_version\";s:6:\"15.6.2\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1612374112;s:13:\"myyoast-oauth\";b:0;s:26:\"semrush_integration_active\";b:1;s:14:\"semrush_tokens\";a:0:{}s:20:\"semrush_country_code\";s:2:\"us\";s:19:\"permalink_structure\";s:12:\"/%postname%/\";s:8:\"home_url\";s:18:\"http://bizen.local\";s:18:\"dynamic_permalinks\";b:0;s:17:\"category_base_url\";s:0:\"\";s:12:\"tag_base_url\";s:0:\"\";s:21:\"custom_taxonomy_slugs\";a:0:{}s:29:\"enable_enhanced_slack_sharing\";b:1;s:25:\"zapier_integration_active\";b:0;s:19:\"zapier_subscription\";a:0:{}s:14:\"zapier_api_key\";s:0:\"\";}', 'yes'),
(193, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:4:\"15.7\";}', 'yes'),
(194, 'wpseo_titles', 'a:76:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-pipe\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:1;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:2:\"»\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:5:\"Bizen\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:1;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";s:1:\"0\";s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:18:\"title-tax-category\";s:44:\"%%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}', 'yes'),
(195, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";}', 'yes'),
(197, 'itsec_temp_whitelist_ip', 'a:1:{s:9:\"127.0.0.1\";i:1613406391;}', 'no'),
(198, 'action_scheduler_hybrid_store_demarkation', '9', 'yes'),
(199, 'schema-ActionScheduler_StoreSchema', '3.0.1612374115', 'yes'),
(200, 'schema-ActionScheduler_LoggerSchema', '2.0.1612374115', 'yes'),
(204, 'wp_mail_smtp_migration_version', '2', 'yes'),
(205, 'acf_version', '5.8.11', 'yes'),
(206, 'duplicate_post_copytitle', '1', 'yes'),
(207, 'duplicate_post_copydate', '0', 'yes'),
(208, 'duplicate_post_copystatus', '0', 'yes'),
(209, 'duplicate_post_copyslug', '0', 'yes'),
(210, 'duplicate_post_copyexcerpt', '1', 'yes'),
(211, 'duplicate_post_copycontent', '1', 'yes'),
(212, 'duplicate_post_copythumbnail', '1', 'yes'),
(213, 'duplicate_post_copytemplate', '1', 'yes'),
(214, 'duplicate_post_copyformat', '1', 'yes'),
(215, 'duplicate_post_copyauthor', '0', 'yes'),
(216, 'duplicate_post_copypassword', '0', 'yes'),
(217, 'duplicate_post_copyattachments', '0', 'yes'),
(218, 'duplicate_post_copychildren', '0', 'yes'),
(219, 'duplicate_post_copycomments', '0', 'yes'),
(220, 'duplicate_post_copymenuorder', '1', 'yes'),
(221, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(222, 'duplicate_post_blacklist', '', 'yes'),
(223, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(224, 'duplicate_post_show_original_column', '0', 'yes'),
(225, 'duplicate_post_show_original_in_post_states', '0', 'yes'),
(226, 'duplicate_post_show_original_meta_box', '0', 'yes'),
(227, 'duplicate_post_show_link', 'a:3:{s:9:\"new_draft\";s:1:\"1\";s:5:\"clone\";s:1:\"1\";s:17:\"rewrite_republish\";s:1:\"1\";}', 'yes'),
(228, 'duplicate_post_show_link_in', 'a:4:{s:3:\"row\";s:1:\"1\";s:8:\"adminbar\";s:1:\"1\";s:9:\"submitbox\";s:1:\"1\";s:11:\"bulkactions\";s:1:\"1\";}', 'yes'),
(231, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:-1;s:10:\"last_fetch\";i:1612978929;}', 'yes'),
(242, 'wp_mail_smtp_review_notice', 'a:2:{s:4:\"time\";i:1612374120;s:9:\"dismissed\";b:0;}', 'yes'),
(243, 'action_scheduler_lock_async-request-runner', '1613322857', 'yes'),
(259, 'duplicate_post_show_notice', '0', 'no'),
(260, 'duplicate_post_version', '4.1.1', 'yes'),
(263, 'action_scheduler_migration_status', 'complete', 'yes'),
(265, 'wp_mail_smtp_notifications', 'a:4:{s:6:\"update\";i:1612374188;s:4:\"feed\";a:0:{}s:6:\"events\";a:0:{}s:9:\"dismissed\";a:0:{}}', 'yes'),
(276, 'itsec_active_modules', 'a:9:{s:9:\"ban-users\";b:1;s:6:\"backup\";b:1;s:11:\"brute-force\";b:1;s:19:\"network-brute-force\";b:1;s:16:\"wordpress-tweaks\";b:1;s:18:\"security-check-pro\";b:1;s:13:\"404-detection\";b:1;s:11:\"file-change\";b:1;s:13:\"system-tweaks\";b:1;}', 'yes'),
(283, 'wp_mail_smtp_debug', 'a:1:{i:0;s:110:\"Mailer: Default (none)\r\nPHPMailer was able to connect to SMTP server but failed while trying to send an email.\";}', 'no'),
(308, '_transient_health-check-site-status-result', '{\"good\":\"10\",\"recommended\":\"9\",\"critical\":\"1\"}', 'yes'),
(354, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(386, 'options_copyright', '©[year]　Bizen City. all rights reserved.', 'no'),
(387, '_options_copyright', 'field_602419de10681', 'no'),
(388, 'options_pdf_download', '29', 'no'),
(389, '_options_pdf_download', 'field_602419eb10682', 'no'),
(390, 'options_contact_information_title', 'お問い合わせはこちらから', 'no'),
(391, '_options_contact_information_title', 'field_60241a2510684', 'no'),
(392, 'options_contact_information_content', '備前市産業部都市住宅課移住定住推進係\r\n受付時間：8時30分〜17時\r\n〒705-8602　岡山県備前市東片上126番地\r\nTEL：0869-64-2225', 'no'),
(393, '_options_contact_information_content', 'field_60241a6610685', 'no'),
(394, 'options_contact_information', '', 'no'),
(395, '_options_contact_information', 'field_60241a1410683', 'no'),
(402, 'options_reference_link', 'https://www.city.bizen.okayama.jp/site/bizen/9806.html', 'no'),
(403, '_options_reference_link', 'field_60241c0e33419', 'no'),
(410, '_transient_timeout_acf_plugin_updates', '1613323445', 'no'),
(411, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.6\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.8.11\";}}', 'no'),
(415, '_site_transient_timeout_php_check_fce1f096719779a3888ef81259141e1f', '1613755450', 'no'),
(416, '_site_transient_php_check_fce1f096719779a3888ef81259141e1f', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(435, '_site_transient_timeout_browser_28c545c98305b4896bacfc3f6da37924', '1613755699', 'no'),
(436, '_site_transient_browser_28c545c98305b4896bacfc3f6da37924', 'a:10:{s:4:\"name\";s:5:\"Opera\";s:7:\"version\";s:13:\"73.0.3856.396\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:22:\"https://www.opera.com/\";s:7:\"img_src\";s:42:\"http://s.w.org/images/browsers/opera.png?1\";s:11:\"img_src_ssl\";s:43:\"https://s.w.org/images/browsers/opera.png?1\";s:15:\"current_version\";s:5:\"12.18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(446, 'category_children', 'a:1:{i:2;a:3:{i:0;i:5;i:1;i:6;i:2;i:8;}}', 'yes'),
(455, '_site_transient_itsec_wp_upload_dir', 'a:6:{s:4:\"path\";s:49:\"D:/Tools/xampp/htdocs/bizen//site/package/uploads\";s:3:\"url\";s:39:\"http://bizen.local/site/package/uploads\";s:6:\"subdir\";s:0:\"\";s:7:\"basedir\";s:49:\"D:/Tools/xampp/htdocs/bizen//site/package/uploads\";s:7:\"baseurl\";s:39:\"http://bizen.local/site/package/uploads\";s:5:\"error\";b:0;}', 'no'),
(456, '_site_transient_timeout_itsec_wp_upload_dir', '1613323456', 'no'),
(494, '_site_transient_timeout_theme_roots', '1613321796', 'no'),
(495, '_site_transient_theme_roots', 'a:1:{s:5:\"bizen\";s:7:\"/themes\";}', 'no'),
(496, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1613319998;s:7:\"checked\";a:7:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.8.11\";s:19:\"akismet/akismet.php\";s:5:\"4.1.7\";s:9:\"hello.php\";s:5:\"1.7.2\";s:41:\"better-wp-security/better-wp-security.php\";s:5:\"7.9.0\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"2.5.1\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"4.1.1\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"15.7\";}s:8:\"response\";a:4:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.6.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"2.6.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.2.6.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2468655\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2468655\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.6.1\";s:12:\"requires_php\";s:3:\"5.5\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"15.8\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.15.8.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.6.1\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.6\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"better-wp-security/better-wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/better-wp-security\";s:4:\"slug\";s:18:\"better-wp-security\";s:6:\"plugin\";s:41:\"better-wp-security/better-wp-security.php\";s:11:\"new_version\";s:5:\"7.9.0\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/better-wp-security/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/better-wp-security.7.9.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:70:\"https://ps.w.org/better-wp-security/assets/icon-256x256.jpg?rev=969999\";s:2:\"1x\";s:62:\"https://ps.w.org/better-wp-security/assets/icon.svg?rev=970042\";s:3:\"svg\";s:62:\"https://ps.w.org/better-wp-security/assets/icon.svg?rev=970042\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/better-wp-security/assets/banner-772x250.png?rev=881897\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(516, '_transient_timeout_wpseo_total_unindexed_posts', '1613407056', 'no'),
(517, '_transient_wpseo_total_unindexed_posts', '0', 'no'),
(518, '_transient_timeout_wpseo_total_unindexed_terms', '1613407056', 'no'),
(519, '_transient_wpseo_total_unindexed_terms', '0', 'no'),
(520, '_transient_timeout_wpseo_total_unindexed_post_type_archives', '1613407056', 'no'),
(521, '_transient_wpseo_total_unindexed_post_type_archives', '0', 'no'),
(522, '_transient_timeout_wpseo_unindexed_post_link_count', '1613407056', 'no'),
(523, '_transient_wpseo_unindexed_post_link_count', '0', 'no'),
(524, '_transient_timeout_wpseo_unindexed_term_link_count', '1613407056', 'no'),
(525, '_transient_wpseo_unindexed_term_link_count', '0', 'no');
INSERT INTO `bzn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(528, 'rewrite_rules', 'a:108:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:57:\"(information/event)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:40:\"(information/event)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:22:\"(information/event)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:51:\"(information)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:34:\"(information)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:16:\"(information)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:56:\"(information/news)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:39:\"(information/news)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:21:\"(information/news)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:57:\"(information/other)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:40:\"(information/other)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:22:\"(information/other)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:49:\"(interview)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:32:\"(interview)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:14:\"(interview)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:14:\"category/(.+)$\";s:45:\"index.php?wpseo_category_redirect=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(535, '_site_transient_timeout_available_translations', '1613331724', 'no');
INSERT INTO `bzn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(536, '_site_transient_available_translations', 'a:125:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.5.3\";s:7:\"updated\";s:19:\"2020-10-30 09:12:13\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.5.3/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 23:50:25\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-09 19:08:56\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.15/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-01-22 10:57:09\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:22:\"Продължение\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.8.14/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.5.3\";s:7:\"updated\";s:19:\"2020-10-30 03:24:38\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.5.3/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.4.4\";s:7:\"updated\";s:19:\"2021-02-03 21:38:34\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.4/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-30 07:52:03\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 21:32:58\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-02 14:47:38\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-21 11:29:16\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-08 15:56:22\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-07 12:39:24\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.6/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:10:56\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:11:01\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.6/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-07 12:36:13\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:3:\"dsb\";a:8:{s:8:\"language\";s:3:\"dsb\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:08:08\";s:12:\"english_name\";s:13:\"Lower Sorbian\";s:11:\"native_name\";s:16:\"Dolnoserbšćina\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.6/dsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"dsb\";i:3;s:3:\"dsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Dalej\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 21:05:55\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:18:55\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-09 01:25:09\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:06:07\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-09 01:24:14\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-13 04:37:35\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-01 09:21:22\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 06:18:55\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 06:18:30\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 21:52:43\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-27 04:11:56\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-23 02:47:04\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_EC\";a:8:{s:8:\"language\";s:5:\"es_EC\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 06:19:16\";s:12:\"english_name\";s:17:\"Spanish (Ecuador)\";s:11:\"native_name\";s:19:\"Español de Ecuador\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_EC.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-23 23:51:44\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-06-23 16:46:04\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.2/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PR\";a:8:{s:8:\"language\";s:5:\"es_PR\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2020-04-29 15:36:59\";s:12:\"english_name\";s:21:\"Spanish (Puerto Rico)\";s:11:\"native_name\";s:23:\"Español de Puerto Rico\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.1/es_PR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_UY\";a:8:{s:8:\"language\";s:5:\"es_UY\";s:7:\"version\";s:5:\"5.3.2\";s:7:\"updated\";s:19:\"2019-11-12 04:43:11\";s:12:\"english_name\";s:17:\"Spanish (Uruguay)\";s:11:\"native_name\";s:19:\"Español de Uruguay\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.2/es_UY.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"5.1.6\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.1.6/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-05 04:02:03\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:3:\"5.5\";s:7:\"updated\";s:19:\"2020-08-12 08:38:59\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.5/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-03 11:54:08\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-13 16:13:36\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"fa_AF\";a:8:{s:8:\"language\";s:5:\"fa_AF\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-03 19:17:59\";s:12:\"english_name\";s:21:\"Persian (Afghanistan)\";s:11:\"native_name\";s:31:\"(فارسی (افغانستان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/fa_AF.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 05:20:40\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-20 14:58:20\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-02 13:12:29\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-02 16:39:19\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-30 23:42:25\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-13 14:17:34\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"להמשיך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 21:01:09\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:3:\"hsb\";a:8:{s:8:\"language\";s:3:\"hsb\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:08:42\";s:12:\"english_name\";s:13:\"Upper Sorbian\";s:11:\"native_name\";s:17:\"Hornjoserbšćina\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.6/hsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"hsb\";i:3;s:3:\"hsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:4:\"Dale\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 22:13:42\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Tovább\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-03 06:49:47\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-27 14:41:16\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-31 01:17:57\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nutugne\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"5.4.4\";s:7:\"updated\";s:19:\"2020-11-04 09:27:56\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.4/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-10 23:41:00\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.6/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2019-01-09 07:34:10\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.0.3/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.16\";s:7:\"updated\";s:19:\"2020-09-30 14:08:59\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.16/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-01 01:51:34\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:8:\"5.6-beta\";s:7:\"updated\";s:19:\"2020-11-20 14:49:35\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.6-beta/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"ຕໍ່\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-06 15:17:10\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"5.4.2\";s:7:\"updated\";s:19:\"2020-07-14 08:34:14\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.2/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"5.2.3\";s:7:\"updated\";s:19:\"2019-09-08 12:57:25\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.2.3/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.14/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:6:\"4.9.15\";s:7:\"updated\";s:19:\"2018-08-31 11:57:07\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.15/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ေဆာင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-01 14:40:58\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"जारीराख्नु \";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-30 18:26:14\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:45:10\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:8:\"5.6-beta\";s:7:\"updated\";s:19:\"2020-11-25 10:09:04\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/translation/core/5.6-beta/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-01-01 08:53:00\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-04 07:24:53\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"دوام\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-08 14:21:04\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-25 17:26:41\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-07 22:41:07\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/translation/core/5.6/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-07 22:39:42\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-30 08:38:07\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:09:26\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:3:\"snd\";a:8:{s:8:\"language\";s:3:\"snd\";s:7:\"version\";s:3:\"5.3\";s:7:\"updated\";s:19:\"2019-11-12 04:37:38\";s:12:\"english_name\";s:6:\"Sindhi\";s:11:\"native_name\";s:8:\"سنڌي\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/translation/core/5.3/snd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"sd\";i:2;s:3:\"snd\";i:3;s:3:\"snd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"اڳتي هلو\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2020-12-09 09:21:17\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.5.1\";s:7:\"updated\";s:19:\"2020-09-13 06:50:55\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.5.1/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Nadaljujte\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-31 10:15:23\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:3:\"5.5\";s:7:\"updated\";s:19:\"2020-08-12 14:55:13\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.5/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-28 20:24:32\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"sw\";a:8:{s:8:\"language\";s:2:\"sw\";s:7:\"version\";s:5:\"5.2.6\";s:7:\"updated\";s:19:\"2019-10-22 00:19:41\";s:12:\"english_name\";s:7:\"Swahili\";s:11:\"native_name\";s:9:\"Kiswahili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.2.6/sw.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sw\";i:2;s:3:\"swa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Endelea\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.4.4\";s:7:\"updated\";s:19:\"2021-01-30 18:23:21\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.4/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-17 20:47:32\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-01-29 19:19:37\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/translation/core/5.6/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.1.6\";s:7:\"updated\";s:19:\"2020-04-09 10:48:08\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.1.6/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:6:\"5.0.10\";s:7:\"updated\";s:19:\"2019-01-23 12:32:40\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.0.10/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.4.4\";s:7:\"updated\";s:19:\"2021-01-06 14:54:27\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.4/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-01 12:49:30\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:3:\"5.6\";s:7:\"updated\";s:19:\"2021-02-03 07:19:13\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.6/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"5.3.3\";s:7:\"updated\";s:19:\"2020-03-08 12:12:22\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.3.3/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_postmeta`
--

CREATE TABLE `bzn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_postmeta`
--

INSERT INTO `bzn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(10, 7, '_edit_lock', '1613241852:1'),
(11, 7, '_wp_page_template', 'front.php'),
(12, 10, '_menu_item_type', 'post_type'),
(13, 10, '_menu_item_menu_item_parent', '0'),
(14, 10, '_menu_item_object_id', '7'),
(15, 10, '_menu_item_object', 'page'),
(16, 10, '_menu_item_target', ''),
(17, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(18, 10, '_menu_item_xfn', ''),
(19, 10, '_menu_item_url', ''),
(21, 11, '_menu_item_type', 'taxonomy'),
(22, 11, '_menu_item_menu_item_parent', '0'),
(23, 11, '_menu_item_object_id', '3'),
(24, 11, '_menu_item_object', 'category'),
(25, 11, '_menu_item_target', ''),
(26, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(27, 11, '_menu_item_xfn', ''),
(28, 11, '_menu_item_url', ''),
(30, 12, '_menu_item_type', 'custom'),
(31, 12, '_menu_item_menu_item_parent', '0'),
(32, 12, '_menu_item_object_id', '12'),
(33, 12, '_menu_item_object', 'custom'),
(34, 12, '_menu_item_target', ''),
(35, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(36, 12, '_menu_item_xfn', ''),
(37, 12, '_menu_item_url', '#footer'),
(39, 7, '_edit_last', '1'),
(40, 14, '_menu_item_type', 'custom'),
(41, 14, '_menu_item_menu_item_parent', '0'),
(42, 14, '_menu_item_object_id', '14'),
(43, 14, '_menu_item_object', 'custom'),
(44, 14, '_menu_item_target', ''),
(45, 14, '_menu_item_classes', 'a:1:{i:0;s:11:\"anchor-link\";}'),
(46, 14, '_menu_item_xfn', ''),
(47, 14, '_menu_item_url', 'http://bizen.local/#bizen-city'),
(49, 15, '_menu_item_type', 'custom'),
(50, 15, '_menu_item_menu_item_parent', '0'),
(51, 15, '_menu_item_object_id', '15'),
(52, 15, '_menu_item_object', 'custom'),
(53, 15, '_menu_item_target', ''),
(54, 15, '_menu_item_classes', 'a:1:{i:0;s:11:\"anchor-link\";}'),
(55, 15, '_menu_item_xfn', ''),
(56, 15, '_menu_item_url', 'http://bizen.local/#how-to'),
(58, 16, '_menu_item_type', 'custom'),
(59, 16, '_menu_item_menu_item_parent', '0'),
(60, 16, '_menu_item_object_id', '16'),
(61, 16, '_menu_item_object', 'custom'),
(62, 16, '_menu_item_target', ''),
(63, 16, '_menu_item_classes', 'a:1:{i:0;s:11:\"anchor-link\";}'),
(64, 16, '_menu_item_xfn', ''),
(65, 16, '_menu_item_url', 'http://bizen.local/#support'),
(67, 17, '_menu_item_type', 'post_type'),
(68, 17, '_menu_item_menu_item_parent', '0'),
(69, 17, '_menu_item_object_id', '7'),
(70, 17, '_menu_item_object', 'page'),
(71, 17, '_menu_item_target', ''),
(72, 17, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(73, 17, '_menu_item_xfn', ''),
(74, 17, '_menu_item_url', ''),
(76, 18, '_menu_item_type', 'custom'),
(77, 18, '_menu_item_menu_item_parent', '0'),
(78, 18, '_menu_item_object_id', '18'),
(79, 18, '_menu_item_object', 'custom'),
(80, 18, '_menu_item_target', ''),
(81, 18, '_menu_item_classes', 'a:1:{i:0;s:9:\"no-anchor\";}'),
(82, 18, '_menu_item_xfn', ''),
(83, 18, '_menu_item_url', 'http://bizen.local/#bizen-city'),
(85, 19, '_menu_item_type', 'taxonomy'),
(86, 19, '_menu_item_menu_item_parent', '0'),
(87, 19, '_menu_item_object_id', '3'),
(88, 19, '_menu_item_object', 'category'),
(89, 19, '_menu_item_target', ''),
(90, 19, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(91, 19, '_menu_item_xfn', ''),
(92, 19, '_menu_item_url', ''),
(94, 20, '_menu_item_type', 'custom'),
(95, 20, '_menu_item_menu_item_parent', '0'),
(96, 20, '_menu_item_object_id', '20'),
(97, 20, '_menu_item_object', 'custom'),
(98, 20, '_menu_item_target', ''),
(99, 20, '_menu_item_classes', 'a:1:{i:0;s:9:\"no-anchor\";}'),
(100, 20, '_menu_item_xfn', ''),
(101, 20, '_menu_item_url', 'http://bizen.local/#how-to'),
(103, 21, '_menu_item_type', 'custom'),
(104, 21, '_menu_item_menu_item_parent', '0'),
(105, 21, '_menu_item_object_id', '21'),
(106, 21, '_menu_item_object', 'custom'),
(107, 21, '_menu_item_target', ''),
(108, 21, '_menu_item_classes', 'a:1:{i:0;s:9:\"no-anchor\";}'),
(109, 21, '_menu_item_xfn', ''),
(110, 21, '_menu_item_url', 'http://bizen.local/#support'),
(112, 22, '_menu_item_type', 'custom'),
(113, 22, '_menu_item_menu_item_parent', '0'),
(114, 22, '_menu_item_object_id', '22'),
(115, 22, '_menu_item_object', 'custom'),
(116, 22, '_menu_item_target', ''),
(117, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(118, 22, '_menu_item_xfn', ''),
(119, 22, '_menu_item_url', '#footer'),
(121, 23, '_edit_last', '1'),
(122, 23, '_edit_lock', '1612979100:1'),
(123, 29, '_wp_attached_file', 'bizen_specification.pdf'),
(124, 31, '_edit_last', '1'),
(125, 31, '_edit_lock', '1613320493:1'),
(126, 37, '_wp_attached_file', 'banner_asset.png'),
(127, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:348;s:6:\"height\";i:222;s:4:\"file\";s:16:\"banner_asset.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"banner_asset-300x191.png\";s:5:\"width\";i:300;s:6:\"height\";i:191;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"banner_asset-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:24:\"banner_asset-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:24:\"banner_asset-200x200.png\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:24:\"banner_asset-300x222.png\";s:5:\"width\";i:300;s:6:\"height\";i:222;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(128, 38, '_wp_attached_file', 'main.jpg'),
(129, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:466;s:4:\"file\";s:8:\"main.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"main-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"main-1024x349.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:349;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"main-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"main-768x262.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:262;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:16:\"main-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:16:\"main-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:16:\"main-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:16:\"main-500x466.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:466;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(130, 7, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(131, 7, 'poster_image', '37'),
(132, 7, '_poster_image', 'field_60241cfb84d6e'),
(133, 7, 'main_slider_0_image', '38'),
(134, 7, '_main_slider_0_image', 'field_60241d2de1e18'),
(135, 7, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(136, 7, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(137, 7, 'main_slider_1_image', '38'),
(138, 7, '_main_slider_1_image', 'field_60241d2de1e18'),
(139, 7, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(140, 7, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(141, 7, 'main_slider_2_image', '38'),
(142, 7, '_main_slider_2_image', 'field_60241d2de1e18'),
(143, 7, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(144, 7, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(145, 7, 'main_slider', '3'),
(146, 7, '_main_slider', 'field_60241d14e1e17'),
(147, 39, 'poster_image', '37'),
(148, 39, '_poster_image', 'field_60241cfb84d6e'),
(149, 39, 'main_slider_0_image', '38'),
(150, 39, '_main_slider_0_image', 'field_60241d2de1e18'),
(151, 39, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(152, 39, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(153, 39, 'main_slider_1_image', '38'),
(154, 39, '_main_slider_1_image', 'field_60241d2de1e18'),
(155, 39, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(156, 39, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(157, 39, 'main_slider_2_image', '38'),
(158, 39, '_main_slider_2_image', 'field_60241d2de1e18'),
(159, 39, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(160, 39, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(161, 39, 'main_slider', '3'),
(162, 39, '_main_slider', 'field_60241d14e1e17'),
(163, 41, '_wp_attached_file', 'responsive_main.png'),
(164, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:851;s:6:\"height\";i:362;s:4:\"file\";s:19:\"responsive_main.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"responsive_main-300x128.png\";s:5:\"width\";i:300;s:6:\"height\";i:128;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"responsive_main-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"responsive_main-768x327.png\";s:5:\"width\";i:768;s:6:\"height\";i:327;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:27:\"responsive_main-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:27:\"responsive_main-200x200.png\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:27:\"responsive_main-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:27:\"responsive_main-500x362.png\";s:5:\"width\";i:500;s:6:\"height\";i:362;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 7, 'main_slider_0_image_mobile', '41'),
(166, 7, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(167, 7, 'main_slider_1_image_mobile', '41'),
(168, 7, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(169, 7, 'main_slider_2_image_mobile', '41'),
(170, 7, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(171, 42, 'poster_image', '37'),
(172, 42, '_poster_image', 'field_60241cfb84d6e'),
(173, 42, 'main_slider_0_image', '38'),
(174, 42, '_main_slider_0_image', 'field_60241d2de1e18'),
(175, 42, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(176, 42, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(177, 42, 'main_slider_1_image', '38'),
(178, 42, '_main_slider_1_image', 'field_60241d2de1e18'),
(179, 42, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(180, 42, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(181, 42, 'main_slider_2_image', '38'),
(182, 42, '_main_slider_2_image', 'field_60241d2de1e18'),
(183, 42, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(184, 42, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(185, 42, 'main_slider', '3'),
(186, 42, '_main_slider', 'field_60241d14e1e17'),
(187, 42, 'main_slider_0_image_mobile', '41'),
(188, 42, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(189, 42, 'main_slider_1_image_mobile', '41'),
(190, 42, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(191, 42, 'main_slider_2_image_mobile', '41'),
(192, 42, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(193, 7, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(194, 7, '_pickup_text', 'field_60241f7d2a361'),
(195, 7, 'video_1_title', '「かたちづくる街」'),
(196, 7, '_video_1_title', 'field_60241fdc2a363'),
(197, 7, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(198, 7, '_video_1_embed_url', 'field_60241fe12a364'),
(199, 7, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(200, 7, '_video_1_description', 'field_6024200c2a365'),
(201, 7, 'video_1', ''),
(202, 7, '_video_1', 'field_60241fd22a362'),
(203, 7, 'video_2_title', '「かたちづくる街」'),
(204, 7, '_video_2_title', 'field_602420392a367'),
(205, 7, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(206, 7, '_video_2_embed_url', 'field_602420392a368'),
(207, 7, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(208, 7, '_video_2_description', 'field_602420392a369'),
(209, 7, 'video_2', ''),
(210, 7, '_video_2', 'field_602420392a366'),
(211, 53, 'poster_image', '37'),
(212, 53, '_poster_image', 'field_60241cfb84d6e'),
(213, 53, 'main_slider_0_image', '38'),
(214, 53, '_main_slider_0_image', 'field_60241d2de1e18'),
(215, 53, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(216, 53, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(217, 53, 'main_slider_1_image', '38'),
(218, 53, '_main_slider_1_image', 'field_60241d2de1e18'),
(219, 53, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(220, 53, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(221, 53, 'main_slider_2_image', '38'),
(222, 53, '_main_slider_2_image', 'field_60241d2de1e18'),
(223, 53, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(224, 53, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(225, 53, 'main_slider', '3'),
(226, 53, '_main_slider', 'field_60241d14e1e17'),
(227, 53, 'main_slider_0_image_mobile', '41'),
(228, 53, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(229, 53, 'main_slider_1_image_mobile', '41'),
(230, 53, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(231, 53, 'main_slider_2_image_mobile', '41'),
(232, 53, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(233, 53, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(234, 53, '_pickup_text', 'field_60241f7d2a361'),
(235, 53, 'video_1_title', '「かたちづくる街」'),
(236, 53, '_video_1_title', 'field_60241fdc2a363'),
(237, 53, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(238, 53, '_video_1_embed_url', 'field_60241fe12a364'),
(239, 53, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(240, 53, '_video_1_description', 'field_6024200c2a365'),
(241, 53, 'video_1', ''),
(242, 53, '_video_1', 'field_60241fd22a362'),
(243, 53, 'video_2_title', '「かたちづくる街」'),
(244, 53, '_video_2_title', 'field_602420392a367'),
(245, 53, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(246, 53, '_video_2_embed_url', 'field_602420392a368'),
(247, 53, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(248, 53, '_video_2_description', 'field_602420392a369'),
(249, 53, 'video_2', ''),
(250, 53, '_video_2', 'field_602420392a366'),
(251, 54, '_edit_lock', '1613151839:1'),
(252, 55, '_wp_attached_file', 'sample_lg.jpg'),
(253, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:866;s:6:\"height\";i:406;s:4:\"file\";s:13:\"sample_lg.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"sample_lg-300x141.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"sample_lg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"sample_lg-768x360.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:360;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:21:\"sample_lg-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:21:\"sample_lg-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:21:\"sample_lg-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:21:\"sample_lg-500x406.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:406;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(255, 54, '_edit_last', '1'),
(257, 54, '_yoast_wpseo_content_score', '90'),
(258, 54, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(259, 57, '_wp_attached_file', 'sample.jpg'),
(260, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:488;s:6:\"height\";i:320;s:4:\"file\";s:10:\"sample.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"sample-300x197.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"sample-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:18:\"sample-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:18:\"sample-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:18:\"sample-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(262, 54, '_thumbnail_id', '57'),
(264, 54, '_yoast_wpseo_primary_category', '5'),
(265, 58, '_yoast_wpseo_content_score', '90'),
(266, 58, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(267, 58, '_thumbnail_id', '57'),
(269, 58, '_yoast_wpseo_primary_category', '8'),
(270, 58, '_dp_original', '54'),
(271, 58, '_edit_lock', '1613151842:1'),
(272, 58, '_edit_last', '1'),
(274, 60, '_yoast_wpseo_content_score', '90'),
(275, 60, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(276, 60, '_thumbnail_id', '57'),
(277, 60, '_yoast_wpseo_primary_category', '6'),
(280, 60, '_dp_original', '58'),
(281, 60, '_edit_lock', '1613151847:1'),
(282, 60, '_edit_last', '1'),
(284, 62, '_yoast_wpseo_content_score', '30'),
(285, 62, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(286, 62, '_thumbnail_id', '57'),
(287, 62, '_yoast_wpseo_primary_category', '5'),
(290, 62, '_dp_original', '60'),
(291, 62, '_edit_lock', '1613151848:1'),
(292, 62, '_edit_last', '1'),
(294, 62, '_wp_old_date', '2021-02-12'),
(297, 58, '_wp_old_date', '2021-02-12'),
(298, 64, '_yoast_wpseo_content_score', '90'),
(299, 64, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(300, 64, '_thumbnail_id', '57'),
(301, 64, '_yoast_wpseo_primary_category', '8'),
(303, 64, '_dp_original', '60'),
(304, 64, '_edit_lock', '1613151857:1'),
(306, 64, '_edit_last', '1'),
(310, 54, '_wp_old_slug', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5'),
(313, 58, '_wp_old_slug', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5-2'),
(315, 60, '_wp_old_slug', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5-3'),
(317, 62, '_wp_old_slug', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5-4'),
(321, 64, '_wp_old_slug', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b%e3%81%8a%e7%9f%a5-5'),
(323, 78, '_wp_attached_file', 'hand.svg'),
(324, 79, '_wp_attached_file', 'map.png'),
(325, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:545;s:6:\"height\";i:423;s:4:\"file\";s:7:\"map.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"map-300x233.png\";s:5:\"width\";i:300;s:6:\"height\";i:233;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"map-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:15:\"map-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:15:\"map-200x200.png\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:15:\"map-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:15:\"map-500x423.png\";s:5:\"width\";i:500;s:6:\"height\";i:423;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(326, 7, 'small_title', '備前市を知ろう'),
(327, 7, '_small_title', 'field_6026c19cc456b'),
(328, 7, 'title_with_icon_left_text', 'いいね'),
(329, 7, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(330, 7, 'title_with_icon_icon', '78'),
(331, 7, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(332, 7, 'title_with_icon_right_text', '備前市'),
(333, 7, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(334, 7, 'title_with_icon', ''),
(335, 7, '_title_with_icon', 'field_6026c1a5c456c'),
(336, 7, 'section_title', '備前市ってこんなところ'),
(337, 7, '_section_title', 'field_6026c212c4571'),
(338, 7, 'map', '79'),
(339, 7, '_map', 'field_6026c220c4572'),
(340, 7, '_', 'field_6026c234c4573'),
(341, 7, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(342, 7, '_map_info_box_1', 'field_6026c2bcc82ea'),
(343, 7, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(344, 7, '_map_info_box_2', 'field_6026c2fbc82eb'),
(345, 80, 'poster_image', '37'),
(346, 80, '_poster_image', 'field_60241cfb84d6e'),
(347, 80, 'main_slider_0_image', '38'),
(348, 80, '_main_slider_0_image', 'field_60241d2de1e18'),
(349, 80, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(350, 80, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(351, 80, 'main_slider_1_image', '38'),
(352, 80, '_main_slider_1_image', 'field_60241d2de1e18'),
(353, 80, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(354, 80, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(355, 80, 'main_slider_2_image', '38'),
(356, 80, '_main_slider_2_image', 'field_60241d2de1e18'),
(357, 80, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(358, 80, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(359, 80, 'main_slider', '3'),
(360, 80, '_main_slider', 'field_60241d14e1e17'),
(361, 80, 'main_slider_0_image_mobile', '41'),
(362, 80, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(363, 80, 'main_slider_1_image_mobile', '41'),
(364, 80, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(365, 80, 'main_slider_2_image_mobile', '41'),
(366, 80, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(367, 80, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(368, 80, '_pickup_text', 'field_60241f7d2a361'),
(369, 80, 'video_1_title', '「かたちづくる街」'),
(370, 80, '_video_1_title', 'field_60241fdc2a363'),
(371, 80, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(372, 80, '_video_1_embed_url', 'field_60241fe12a364'),
(373, 80, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(374, 80, '_video_1_description', 'field_6024200c2a365'),
(375, 80, 'video_1', ''),
(376, 80, '_video_1', 'field_60241fd22a362'),
(377, 80, 'video_2_title', '「かたちづくる街」'),
(378, 80, '_video_2_title', 'field_602420392a367'),
(379, 80, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(380, 80, '_video_2_embed_url', 'field_602420392a368'),
(381, 80, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(382, 80, '_video_2_description', 'field_602420392a369'),
(383, 80, 'video_2', ''),
(384, 80, '_video_2', 'field_602420392a366'),
(385, 80, 'small_title', '備前市を知ろう'),
(386, 80, '_small_title', 'field_6026c19cc456b'),
(387, 80, 'title_with_icon_left_text', 'いいね　'),
(388, 80, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(389, 80, 'title_with_icon_icon', '78'),
(390, 80, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(391, 80, 'title_with_icon_right_text', '備前市'),
(392, 80, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(393, 80, 'title_with_icon', ''),
(394, 80, '_title_with_icon', 'field_6026c1a5c456c'),
(395, 80, 'section_title', '備前市ってこんなところ'),
(396, 80, '_section_title', 'field_6026c212c4571'),
(397, 80, 'map', '79'),
(398, 80, '_map', 'field_6026c220c4572'),
(399, 80, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(400, 80, '_map_info_box_1', 'field_6026c2bcc82ea'),
(401, 80, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(402, 80, '_map_info_box_2', 'field_6026c2fbc82eb'),
(403, 7, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(404, 7, '_map_infomation', 'field_6026c234c4573'),
(405, 81, 'poster_image', '37'),
(406, 81, '_poster_image', 'field_60241cfb84d6e'),
(407, 81, 'main_slider_0_image', '38'),
(408, 81, '_main_slider_0_image', 'field_60241d2de1e18'),
(409, 81, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(410, 81, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(411, 81, 'main_slider_1_image', '38'),
(412, 81, '_main_slider_1_image', 'field_60241d2de1e18'),
(413, 81, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(414, 81, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(415, 81, 'main_slider_2_image', '38'),
(416, 81, '_main_slider_2_image', 'field_60241d2de1e18'),
(417, 81, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(418, 81, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(419, 81, 'main_slider', '3'),
(420, 81, '_main_slider', 'field_60241d14e1e17'),
(421, 81, 'main_slider_0_image_mobile', '41'),
(422, 81, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(423, 81, 'main_slider_1_image_mobile', '41'),
(424, 81, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(425, 81, 'main_slider_2_image_mobile', '41'),
(426, 81, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(427, 81, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(428, 81, '_pickup_text', 'field_60241f7d2a361'),
(429, 81, 'video_1_title', '「かたちづくる街」'),
(430, 81, '_video_1_title', 'field_60241fdc2a363'),
(431, 81, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(432, 81, '_video_1_embed_url', 'field_60241fe12a364'),
(433, 81, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(434, 81, '_video_1_description', 'field_6024200c2a365'),
(435, 81, 'video_1', ''),
(436, 81, '_video_1', 'field_60241fd22a362'),
(437, 81, 'video_2_title', '「かたちづくる街」'),
(438, 81, '_video_2_title', 'field_602420392a367'),
(439, 81, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(440, 81, '_video_2_embed_url', 'field_602420392a368'),
(441, 81, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(442, 81, '_video_2_description', 'field_602420392a369'),
(443, 81, 'video_2', ''),
(444, 81, '_video_2', 'field_602420392a366'),
(445, 81, 'small_title', '備前市を知ろう'),
(446, 81, '_small_title', 'field_6026c19cc456b'),
(447, 81, 'title_with_icon_left_text', 'いいね　'),
(448, 81, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(449, 81, 'title_with_icon_icon', '78'),
(450, 81, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(451, 81, 'title_with_icon_right_text', '備前市'),
(452, 81, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(453, 81, 'title_with_icon', ''),
(454, 81, '_title_with_icon', 'field_6026c1a5c456c'),
(455, 81, 'section_title', '備前市ってこんなところ'),
(456, 81, '_section_title', 'field_6026c212c4571'),
(457, 81, 'map', '79'),
(458, 81, '_map', 'field_6026c220c4572'),
(459, 81, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(460, 81, '_map_info_box_1', 'field_6026c2bcc82ea'),
(461, 81, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(462, 81, '_map_info_box_2', 'field_6026c2fbc82eb'),
(463, 81, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(464, 81, '_map_infomation', 'field_6026c234c4573'),
(465, 88, '_wp_attached_file', 'baby.svg'),
(466, 89, '_wp_attached_file', 'house.svg'),
(467, 90, '_wp_attached_file', 'man.svg'),
(468, 91, '_wp_attached_file', 'text.svg'),
(469, 7, 'information_boxes_title', '備前市の魅力'),
(470, 7, '_information_boxes_title', 'field_6026c5cb5e01d'),
(471, 7, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(472, 7, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(473, 7, 'information_boxes_0_box_background', '1'),
(474, 7, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(475, 7, 'information_boxes_0_icon', '90'),
(476, 7, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(477, 7, 'information_boxes_0_icon_class', 'icon-man'),
(478, 7, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(479, 7, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(480, 7, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(481, 7, 'information_boxes_1_box_background', '0'),
(482, 7, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(483, 7, 'information_boxes_1_icon', ''),
(484, 7, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(485, 7, 'information_boxes_1_icon_class', ''),
(486, 7, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(487, 7, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(488, 7, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(489, 7, 'information_boxes_2_box_background', '1'),
(490, 7, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(491, 7, 'information_boxes_2_icon', '88'),
(492, 7, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(493, 7, 'information_boxes_2_icon_class', 'icon-baby'),
(494, 7, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(495, 7, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(496, 7, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(497, 7, 'information_boxes_3_box_background', '0'),
(498, 7, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(499, 7, 'information_boxes_3_icon', ''),
(500, 7, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(501, 7, 'information_boxes_3_icon_class', ''),
(502, 7, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(503, 7, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(504, 7, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(505, 7, 'information_boxes_4_box_background', '0'),
(506, 7, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(507, 7, 'information_boxes_4_icon', ''),
(508, 7, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(509, 7, 'information_boxes_4_icon_class', ''),
(510, 7, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(511, 7, 'information_boxes', '12'),
(512, 7, '_information_boxes', 'field_6026c4af5e017'),
(513, 92, 'poster_image', '37'),
(514, 92, '_poster_image', 'field_60241cfb84d6e'),
(515, 92, 'main_slider_0_image', '38'),
(516, 92, '_main_slider_0_image', 'field_60241d2de1e18'),
(517, 92, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(518, 92, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(519, 92, 'main_slider_1_image', '38'),
(520, 92, '_main_slider_1_image', 'field_60241d2de1e18'),
(521, 92, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(522, 92, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(523, 92, 'main_slider_2_image', '38'),
(524, 92, '_main_slider_2_image', 'field_60241d2de1e18'),
(525, 92, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(526, 92, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(527, 92, 'main_slider', '3'),
(528, 92, '_main_slider', 'field_60241d14e1e17'),
(529, 92, 'main_slider_0_image_mobile', '41'),
(530, 92, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(531, 92, 'main_slider_1_image_mobile', '41'),
(532, 92, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(533, 92, 'main_slider_2_image_mobile', '41'),
(534, 92, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(535, 92, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(536, 92, '_pickup_text', 'field_60241f7d2a361'),
(537, 92, 'video_1_title', '「かたちづくる街」'),
(538, 92, '_video_1_title', 'field_60241fdc2a363'),
(539, 92, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(540, 92, '_video_1_embed_url', 'field_60241fe12a364'),
(541, 92, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(542, 92, '_video_1_description', 'field_6024200c2a365'),
(543, 92, 'video_1', ''),
(544, 92, '_video_1', 'field_60241fd22a362'),
(545, 92, 'video_2_title', '「かたちづくる街」'),
(546, 92, '_video_2_title', 'field_602420392a367'),
(547, 92, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(548, 92, '_video_2_embed_url', 'field_602420392a368'),
(549, 92, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(550, 92, '_video_2_description', 'field_602420392a369'),
(551, 92, 'video_2', ''),
(552, 92, '_video_2', 'field_602420392a366'),
(553, 92, 'small_title', '備前市を知ろう'),
(554, 92, '_small_title', 'field_6026c19cc456b'),
(555, 92, 'title_with_icon_left_text', 'いいね　'),
(556, 92, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(557, 92, 'title_with_icon_icon', '78'),
(558, 92, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(559, 92, 'title_with_icon_right_text', '備前市'),
(560, 92, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(561, 92, 'title_with_icon', ''),
(562, 92, '_title_with_icon', 'field_6026c1a5c456c'),
(563, 92, 'section_title', '備前市ってこんなところ'),
(564, 92, '_section_title', 'field_6026c212c4571'),
(565, 92, 'map', '79'),
(566, 92, '_map', 'field_6026c220c4572'),
(567, 92, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(568, 92, '_map_info_box_1', 'field_6026c2bcc82ea'),
(569, 92, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(570, 92, '_map_info_box_2', 'field_6026c2fbc82eb'),
(571, 92, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(572, 92, '_map_infomation', 'field_6026c234c4573'),
(573, 92, 'information_boxes_title', '備前市の魅力'),
(574, 92, '_information_boxes_title', 'field_6026c5cb5e01d'),
(575, 92, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(576, 92, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(577, 92, 'information_boxes_0_box_background', '1'),
(578, 92, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(579, 92, 'information_boxes_0_icon', '90'),
(580, 92, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(581, 92, 'information_boxes_0_icon_class', 'icon-man'),
(582, 92, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(583, 92, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(584, 92, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(585, 92, 'information_boxes_1_box_background', '0'),
(586, 92, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(587, 92, 'information_boxes_1_icon', ''),
(588, 92, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(589, 92, 'information_boxes_1_icon_class', ''),
(590, 92, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(591, 92, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(592, 92, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(593, 92, 'information_boxes_2_box_background', '1'),
(594, 92, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(595, 92, 'information_boxes_2_icon', '88'),
(596, 92, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(597, 92, 'information_boxes_2_icon_class', 'icon-baby'),
(598, 92, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(599, 92, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(600, 92, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(601, 92, 'information_boxes_3_box_background', '0'),
(602, 92, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(603, 92, 'information_boxes_3_icon', ''),
(604, 92, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(605, 92, 'information_boxes_3_icon_class', ''),
(606, 92, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(607, 92, 'information_boxes_4_info_text', ''),
(608, 92, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(609, 92, 'information_boxes_4_box_background', '0'),
(610, 92, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(611, 92, 'information_boxes_4_icon', ''),
(612, 92, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(613, 92, 'information_boxes_4_icon_class', ''),
(614, 92, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(615, 92, 'information_boxes', '5'),
(616, 92, '_information_boxes', 'field_6026c4af5e017'),
(617, 7, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(618, 7, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(619, 7, 'information_boxes_5_box_background', '1'),
(620, 7, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(621, 7, 'information_boxes_5_icon', ''),
(622, 7, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(623, 7, 'information_boxes_5_icon_class', ''),
(624, 7, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(625, 7, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(626, 7, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(627, 7, 'information_boxes_6_box_background', '0'),
(628, 7, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(629, 7, 'information_boxes_6_icon', ''),
(630, 7, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(631, 7, 'information_boxes_6_icon_class', ''),
(632, 7, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(633, 7, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(634, 7, '_information_boxes_7_info_text', 'field_6026c4f15e018'),
(635, 7, 'information_boxes_7_box_background', '1'),
(636, 7, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(637, 7, 'information_boxes_7_icon', '91'),
(638, 7, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(639, 7, 'information_boxes_7_icon_class', 'icon-text'),
(640, 7, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(641, 7, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(642, 7, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(643, 7, 'information_boxes_8_box_background', '1'),
(644, 7, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(645, 7, 'information_boxes_8_icon', ''),
(646, 7, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(647, 7, 'information_boxes_8_icon_class', ''),
(648, 7, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(649, 7, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(650, 7, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(651, 7, 'information_boxes_9_box_background', '0'),
(652, 7, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(653, 7, 'information_boxes_9_icon', '89'),
(654, 7, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(655, 7, 'information_boxes_9_icon_class', 'icon-house'),
(656, 7, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(657, 7, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(658, 7, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(659, 7, 'information_boxes_10_box_background', '1'),
(660, 7, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(661, 7, 'information_boxes_10_icon', ''),
(662, 7, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(663, 7, 'information_boxes_10_icon_class', ''),
(664, 7, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(665, 7, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(666, 7, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(667, 7, 'information_boxes_11_box_background', '0'),
(668, 7, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(669, 7, 'information_boxes_11_icon', ''),
(670, 7, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(671, 7, 'information_boxes_11_icon_class', ''),
(672, 7, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(673, 93, 'poster_image', '37'),
(674, 93, '_poster_image', 'field_60241cfb84d6e'),
(675, 93, 'main_slider_0_image', '38'),
(676, 93, '_main_slider_0_image', 'field_60241d2de1e18'),
(677, 93, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(678, 93, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(679, 93, 'main_slider_1_image', '38'),
(680, 93, '_main_slider_1_image', 'field_60241d2de1e18'),
(681, 93, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(682, 93, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(683, 93, 'main_slider_2_image', '38'),
(684, 93, '_main_slider_2_image', 'field_60241d2de1e18'),
(685, 93, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(686, 93, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(687, 93, 'main_slider', '3'),
(688, 93, '_main_slider', 'field_60241d14e1e17'),
(689, 93, 'main_slider_0_image_mobile', '41'),
(690, 93, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(691, 93, 'main_slider_1_image_mobile', '41'),
(692, 93, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(693, 93, 'main_slider_2_image_mobile', '41'),
(694, 93, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(695, 93, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(696, 93, '_pickup_text', 'field_60241f7d2a361'),
(697, 93, 'video_1_title', '「かたちづくる街」'),
(698, 93, '_video_1_title', 'field_60241fdc2a363'),
(699, 93, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(700, 93, '_video_1_embed_url', 'field_60241fe12a364'),
(701, 93, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(702, 93, '_video_1_description', 'field_6024200c2a365'),
(703, 93, 'video_1', ''),
(704, 93, '_video_1', 'field_60241fd22a362'),
(705, 93, 'video_2_title', '「かたちづくる街」'),
(706, 93, '_video_2_title', 'field_602420392a367'),
(707, 93, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(708, 93, '_video_2_embed_url', 'field_602420392a368'),
(709, 93, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(710, 93, '_video_2_description', 'field_602420392a369'),
(711, 93, 'video_2', ''),
(712, 93, '_video_2', 'field_602420392a366'),
(713, 93, 'small_title', '備前市を知ろう'),
(714, 93, '_small_title', 'field_6026c19cc456b'),
(715, 93, 'title_with_icon_left_text', 'いいね　'),
(716, 93, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(717, 93, 'title_with_icon_icon', '78'),
(718, 93, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(719, 93, 'title_with_icon_right_text', '備前市'),
(720, 93, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(721, 93, 'title_with_icon', ''),
(722, 93, '_title_with_icon', 'field_6026c1a5c456c'),
(723, 93, 'section_title', '備前市ってこんなところ'),
(724, 93, '_section_title', 'field_6026c212c4571'),
(725, 93, 'map', '79'),
(726, 93, '_map', 'field_6026c220c4572'),
(727, 93, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(728, 93, '_map_info_box_1', 'field_6026c2bcc82ea'),
(729, 93, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(730, 93, '_map_info_box_2', 'field_6026c2fbc82eb'),
(731, 93, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(732, 93, '_map_infomation', 'field_6026c234c4573'),
(733, 93, 'information_boxes_title', '備前市の魅力'),
(734, 93, '_information_boxes_title', 'field_6026c5cb5e01d'),
(735, 93, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(736, 93, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(737, 93, 'information_boxes_0_box_background', '1'),
(738, 93, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(739, 93, 'information_boxes_0_icon', '90'),
(740, 93, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(741, 93, 'information_boxes_0_icon_class', 'icon-man'),
(742, 93, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(743, 93, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(744, 93, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(745, 93, 'information_boxes_1_box_background', '0'),
(746, 93, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(747, 93, 'information_boxes_1_icon', ''),
(748, 93, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(749, 93, 'information_boxes_1_icon_class', ''),
(750, 93, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(751, 93, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(752, 93, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(753, 93, 'information_boxes_2_box_background', '1'),
(754, 93, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(755, 93, 'information_boxes_2_icon', '88'),
(756, 93, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(757, 93, 'information_boxes_2_icon_class', 'icon-baby'),
(758, 93, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(759, 93, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(760, 93, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(761, 93, 'information_boxes_3_box_background', '0'),
(762, 93, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(763, 93, 'information_boxes_3_icon', ''),
(764, 93, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(765, 93, 'information_boxes_3_icon_class', ''),
(766, 93, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(767, 93, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(768, 93, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(769, 93, 'information_boxes_4_box_background', '0'),
(770, 93, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(771, 93, 'information_boxes_4_icon', ''),
(772, 93, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(773, 93, 'information_boxes_4_icon_class', ''),
(774, 93, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(775, 93, 'information_boxes', '12'),
(776, 93, '_information_boxes', 'field_6026c4af5e017'),
(777, 93, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(778, 93, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(779, 93, 'information_boxes_5_box_background', '1'),
(780, 93, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(781, 93, 'information_boxes_5_icon', ''),
(782, 93, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(783, 93, 'information_boxes_5_icon_class', ''),
(784, 93, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(785, 93, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(786, 93, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(787, 93, 'information_boxes_6_box_background', '0'),
(788, 93, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(789, 93, 'information_boxes_6_icon', ''),
(790, 93, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(791, 93, 'information_boxes_6_icon_class', ''),
(792, 93, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(793, 93, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(794, 93, '_information_boxes_7_info_text', 'field_6026c4f15e018');
INSERT INTO `bzn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(795, 93, 'information_boxes_7_box_background', '1'),
(796, 93, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(797, 93, 'information_boxes_7_icon', '91'),
(798, 93, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(799, 93, 'information_boxes_7_icon_class', 'icon-text'),
(800, 93, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(801, 93, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(802, 93, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(803, 93, 'information_boxes_8_box_background', '1'),
(804, 93, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(805, 93, 'information_boxes_8_icon', ''),
(806, 93, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(807, 93, 'information_boxes_8_icon_class', ''),
(808, 93, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(809, 93, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(810, 93, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(811, 93, 'information_boxes_9_box_background', '0'),
(812, 93, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(813, 93, 'information_boxes_9_icon', '89'),
(814, 93, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(815, 93, 'information_boxes_9_icon_class', 'icon-house'),
(816, 93, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(817, 93, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(818, 93, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(819, 93, 'information_boxes_10_box_background', '1'),
(820, 93, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(821, 93, 'information_boxes_10_icon', ''),
(822, 93, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(823, 93, 'information_boxes_10_icon_class', ''),
(824, 93, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(825, 93, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(826, 93, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(827, 93, 'information_boxes_11_box_background', '0'),
(828, 93, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(829, 93, 'information_boxes_11_icon', ''),
(830, 93, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(831, 93, 'information_boxes_11_icon_class', ''),
(832, 93, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(833, 94, 'poster_image', '37'),
(834, 94, '_poster_image', 'field_60241cfb84d6e'),
(835, 94, 'main_slider_0_image', '38'),
(836, 94, '_main_slider_0_image', 'field_60241d2de1e18'),
(837, 94, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(838, 94, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(839, 94, 'main_slider_1_image', '38'),
(840, 94, '_main_slider_1_image', 'field_60241d2de1e18'),
(841, 94, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(842, 94, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(843, 94, 'main_slider_2_image', '38'),
(844, 94, '_main_slider_2_image', 'field_60241d2de1e18'),
(845, 94, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(846, 94, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(847, 94, 'main_slider', '3'),
(848, 94, '_main_slider', 'field_60241d14e1e17'),
(849, 94, 'main_slider_0_image_mobile', '41'),
(850, 94, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(851, 94, 'main_slider_1_image_mobile', '41'),
(852, 94, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(853, 94, 'main_slider_2_image_mobile', '41'),
(854, 94, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(855, 94, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(856, 94, '_pickup_text', 'field_60241f7d2a361'),
(857, 94, 'video_1_title', '「かたちづくる街」'),
(858, 94, '_video_1_title', 'field_60241fdc2a363'),
(859, 94, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(860, 94, '_video_1_embed_url', 'field_60241fe12a364'),
(861, 94, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(862, 94, '_video_1_description', 'field_6024200c2a365'),
(863, 94, 'video_1', ''),
(864, 94, '_video_1', 'field_60241fd22a362'),
(865, 94, 'video_2_title', '「かたちづくる街」'),
(866, 94, '_video_2_title', 'field_602420392a367'),
(867, 94, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(868, 94, '_video_2_embed_url', 'field_602420392a368'),
(869, 94, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(870, 94, '_video_2_description', 'field_602420392a369'),
(871, 94, 'video_2', ''),
(872, 94, '_video_2', 'field_602420392a366'),
(873, 94, 'small_title', '備前市を知ろう'),
(874, 94, '_small_title', 'field_6026c19cc456b'),
(875, 94, 'title_with_icon_left_text', 'いいね　'),
(876, 94, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(877, 94, 'title_with_icon_icon', '78'),
(878, 94, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(879, 94, 'title_with_icon_right_text', '備前市'),
(880, 94, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(881, 94, 'title_with_icon', ''),
(882, 94, '_title_with_icon', 'field_6026c1a5c456c'),
(883, 94, 'section_title', '備前市ってこんなところ'),
(884, 94, '_section_title', 'field_6026c212c4571'),
(885, 94, 'map', '79'),
(886, 94, '_map', 'field_6026c220c4572'),
(887, 94, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(888, 94, '_map_info_box_1', 'field_6026c2bcc82ea'),
(889, 94, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(890, 94, '_map_info_box_2', 'field_6026c2fbc82eb'),
(891, 94, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(892, 94, '_map_infomation', 'field_6026c234c4573'),
(893, 94, 'information_boxes_title', '備前市の魅力'),
(894, 94, '_information_boxes_title', 'field_6026c5cb5e01d'),
(895, 94, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(896, 94, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(897, 94, 'information_boxes_0_box_background', '1'),
(898, 94, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(899, 94, 'information_boxes_0_icon', '90'),
(900, 94, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(901, 94, 'information_boxes_0_icon_class', 'icon-man'),
(902, 94, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(903, 94, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(904, 94, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(905, 94, 'information_boxes_1_box_background', '0'),
(906, 94, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(907, 94, 'information_boxes_1_icon', ''),
(908, 94, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(909, 94, 'information_boxes_1_icon_class', ''),
(910, 94, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(911, 94, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(912, 94, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(913, 94, 'information_boxes_2_box_background', '1'),
(914, 94, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(915, 94, 'information_boxes_2_icon', '88'),
(916, 94, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(917, 94, 'information_boxes_2_icon_class', 'icon-baby'),
(918, 94, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(919, 94, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(920, 94, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(921, 94, 'information_boxes_3_box_background', '0'),
(922, 94, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(923, 94, 'information_boxes_3_icon', ''),
(924, 94, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(925, 94, 'information_boxes_3_icon_class', ''),
(926, 94, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(927, 94, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(928, 94, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(929, 94, 'information_boxes_4_box_background', '0'),
(930, 94, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(931, 94, 'information_boxes_4_icon', ''),
(932, 94, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(933, 94, 'information_boxes_4_icon_class', ''),
(934, 94, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(935, 94, 'information_boxes', '12'),
(936, 94, '_information_boxes', 'field_6026c4af5e017'),
(937, 94, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(938, 94, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(939, 94, 'information_boxes_5_box_background', '1'),
(940, 94, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(941, 94, 'information_boxes_5_icon', ''),
(942, 94, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(943, 94, 'information_boxes_5_icon_class', ''),
(944, 94, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(945, 94, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(946, 94, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(947, 94, 'information_boxes_6_box_background', '0'),
(948, 94, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(949, 94, 'information_boxes_6_icon', ''),
(950, 94, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(951, 94, 'information_boxes_6_icon_class', ''),
(952, 94, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(953, 94, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(954, 94, '_information_boxes_7_info_text', 'field_6026c4f15e018'),
(955, 94, 'information_boxes_7_box_background', '1'),
(956, 94, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(957, 94, 'information_boxes_7_icon', '91'),
(958, 94, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(959, 94, 'information_boxes_7_icon_class', 'icon-text'),
(960, 94, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(961, 94, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(962, 94, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(963, 94, 'information_boxes_8_box_background', '1'),
(964, 94, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(965, 94, 'information_boxes_8_icon', ''),
(966, 94, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(967, 94, 'information_boxes_8_icon_class', ''),
(968, 94, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(969, 94, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(970, 94, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(971, 94, 'information_boxes_9_box_background', '0'),
(972, 94, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(973, 94, 'information_boxes_9_icon', '89'),
(974, 94, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(975, 94, 'information_boxes_9_icon_class', 'icon-house'),
(976, 94, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(977, 94, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(978, 94, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(979, 94, 'information_boxes_10_box_background', '1'),
(980, 94, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(981, 94, 'information_boxes_10_icon', ''),
(982, 94, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(983, 94, 'information_boxes_10_icon_class', ''),
(984, 94, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(985, 94, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(986, 94, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(987, 94, 'information_boxes_11_box_background', '0'),
(988, 94, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(989, 94, 'information_boxes_11_icon', ''),
(990, 94, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(991, 94, 'information_boxes_11_icon_class', ''),
(992, 94, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(993, 95, 'poster_image', '37'),
(994, 95, '_poster_image', 'field_60241cfb84d6e'),
(995, 95, 'main_slider_0_image', '38'),
(996, 95, '_main_slider_0_image', 'field_60241d2de1e18'),
(997, 95, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(998, 95, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(999, 95, 'main_slider_1_image', '38'),
(1000, 95, '_main_slider_1_image', 'field_60241d2de1e18'),
(1001, 95, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1002, 95, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(1003, 95, 'main_slider_2_image', '38'),
(1004, 95, '_main_slider_2_image', 'field_60241d2de1e18'),
(1005, 95, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1006, 95, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(1007, 95, 'main_slider', '3'),
(1008, 95, '_main_slider', 'field_60241d14e1e17'),
(1009, 95, 'main_slider_0_image_mobile', '41'),
(1010, 95, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(1011, 95, 'main_slider_1_image_mobile', '41'),
(1012, 95, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(1013, 95, 'main_slider_2_image_mobile', '41'),
(1014, 95, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(1015, 95, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(1016, 95, '_pickup_text', 'field_60241f7d2a361'),
(1017, 95, 'video_1_title', '「かたちづくる街」'),
(1018, 95, '_video_1_title', 'field_60241fdc2a363'),
(1019, 95, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1020, 95, '_video_1_embed_url', 'field_60241fe12a364'),
(1021, 95, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(1022, 95, '_video_1_description', 'field_6024200c2a365'),
(1023, 95, 'video_1', ''),
(1024, 95, '_video_1', 'field_60241fd22a362'),
(1025, 95, 'video_2_title', '「かたちづくる街」'),
(1026, 95, '_video_2_title', 'field_602420392a367'),
(1027, 95, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1028, 95, '_video_2_embed_url', 'field_602420392a368'),
(1029, 95, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(1030, 95, '_video_2_description', 'field_602420392a369'),
(1031, 95, 'video_2', ''),
(1032, 95, '_video_2', 'field_602420392a366'),
(1033, 95, 'small_title', '備前市を知ろう'),
(1034, 95, '_small_title', 'field_6026c19cc456b'),
(1035, 95, 'title_with_icon_left_text', 'いいね'),
(1036, 95, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(1037, 95, 'title_with_icon_icon', '78'),
(1038, 95, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(1039, 95, 'title_with_icon_right_text', '備前市'),
(1040, 95, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(1041, 95, 'title_with_icon', ''),
(1042, 95, '_title_with_icon', 'field_6026c1a5c456c'),
(1043, 95, 'section_title', '備前市ってこんなところ'),
(1044, 95, '_section_title', 'field_6026c212c4571'),
(1045, 95, 'map', '79'),
(1046, 95, '_map', 'field_6026c220c4572'),
(1047, 95, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(1048, 95, '_map_info_box_1', 'field_6026c2bcc82ea'),
(1049, 95, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(1050, 95, '_map_info_box_2', 'field_6026c2fbc82eb'),
(1051, 95, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(1052, 95, '_map_infomation', 'field_6026c234c4573'),
(1053, 95, 'information_boxes_title', '備前市の魅力'),
(1054, 95, '_information_boxes_title', 'field_6026c5cb5e01d'),
(1055, 95, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(1056, 95, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(1057, 95, 'information_boxes_0_box_background', '1'),
(1058, 95, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(1059, 95, 'information_boxes_0_icon', '90'),
(1060, 95, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(1061, 95, 'information_boxes_0_icon_class', 'icon-man'),
(1062, 95, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(1063, 95, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(1064, 95, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(1065, 95, 'information_boxes_1_box_background', '0'),
(1066, 95, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(1067, 95, 'information_boxes_1_icon', ''),
(1068, 95, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(1069, 95, 'information_boxes_1_icon_class', ''),
(1070, 95, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(1071, 95, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(1072, 95, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(1073, 95, 'information_boxes_2_box_background', '1'),
(1074, 95, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(1075, 95, 'information_boxes_2_icon', '88'),
(1076, 95, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(1077, 95, 'information_boxes_2_icon_class', 'icon-baby'),
(1078, 95, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(1079, 95, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(1080, 95, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(1081, 95, 'information_boxes_3_box_background', '0'),
(1082, 95, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(1083, 95, 'information_boxes_3_icon', ''),
(1084, 95, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(1085, 95, 'information_boxes_3_icon_class', ''),
(1086, 95, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(1087, 95, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(1088, 95, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(1089, 95, 'information_boxes_4_box_background', '0'),
(1090, 95, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(1091, 95, 'information_boxes_4_icon', ''),
(1092, 95, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(1093, 95, 'information_boxes_4_icon_class', ''),
(1094, 95, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(1095, 95, 'information_boxes', '12'),
(1096, 95, '_information_boxes', 'field_6026c4af5e017'),
(1097, 95, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(1098, 95, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(1099, 95, 'information_boxes_5_box_background', '1'),
(1100, 95, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(1101, 95, 'information_boxes_5_icon', ''),
(1102, 95, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(1103, 95, 'information_boxes_5_icon_class', ''),
(1104, 95, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(1105, 95, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(1106, 95, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(1107, 95, 'information_boxes_6_box_background', '0'),
(1108, 95, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(1109, 95, 'information_boxes_6_icon', ''),
(1110, 95, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(1111, 95, 'information_boxes_6_icon_class', ''),
(1112, 95, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(1113, 95, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(1114, 95, '_information_boxes_7_info_text', 'field_6026c4f15e018'),
(1115, 95, 'information_boxes_7_box_background', '1'),
(1116, 95, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(1117, 95, 'information_boxes_7_icon', '91'),
(1118, 95, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(1119, 95, 'information_boxes_7_icon_class', 'icon-text'),
(1120, 95, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(1121, 95, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(1122, 95, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(1123, 95, 'information_boxes_8_box_background', '1'),
(1124, 95, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(1125, 95, 'information_boxes_8_icon', ''),
(1126, 95, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(1127, 95, 'information_boxes_8_icon_class', ''),
(1128, 95, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(1129, 95, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(1130, 95, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(1131, 95, 'information_boxes_9_box_background', '0'),
(1132, 95, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(1133, 95, 'information_boxes_9_icon', '89'),
(1134, 95, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(1135, 95, 'information_boxes_9_icon_class', 'icon-house'),
(1136, 95, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(1137, 95, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(1138, 95, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(1139, 95, 'information_boxes_10_box_background', '1'),
(1140, 95, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(1141, 95, 'information_boxes_10_icon', ''),
(1142, 95, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(1143, 95, 'information_boxes_10_icon_class', ''),
(1144, 95, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(1145, 95, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(1146, 95, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(1147, 95, 'information_boxes_11_box_background', '0'),
(1148, 95, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(1149, 95, 'information_boxes_11_icon', ''),
(1150, 95, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(1151, 95, 'information_boxes_11_icon_class', ''),
(1152, 95, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(1153, 96, '_edit_lock', '1613238814:1'),
(1154, 97, '_wp_attached_file', 'interview1.jpg'),
(1155, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:535;s:6:\"height\";i:318;s:4:\"file\";s:14:\"interview1.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"interview1-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"interview1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:22:\"interview1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:22:\"interview1-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:22:\"interview1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:22:\"interview1-500x318.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1156, 98, '_wp_attached_file', 'interview2.jpg'),
(1157, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:535;s:6:\"height\";i:318;s:4:\"file\";s:14:\"interview2.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"interview2-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"interview2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:22:\"interview2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:22:\"interview2-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:22:\"interview2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:22:\"interview2-500x318.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1158, 99, '_wp_attached_file', 'interview3.jpg'),
(1159, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1070;s:6:\"height\";i:636;s:4:\"file\";s:14:\"interview3.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"interview3-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"interview3-1024x609.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:609;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"interview3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"interview3-768x456.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:456;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:22:\"interview3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:22:\"interview3-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:22:\"interview3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:22:\"interview3-500x500.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1160, 100, '_wp_attached_file', 'interview4.jpg'),
(1161, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:535;s:6:\"height\";i:318;s:4:\"file\";s:14:\"interview4.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"interview4-300x178.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:178;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"interview4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tn_thumb\";a:4:{s:4:\"file\";s:22:\"interview4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"xs_thumb\";a:4:{s:4:\"file\";s:22:\"interview4-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"sm_thumb\";a:4:{s:4:\"file\";s:22:\"interview4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"md_thumb\";a:4:{s:4:\"file\";s:22:\"interview4-500x318.jpg\";s:5:\"width\";i:500;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1163, 96, '_thumbnail_id', '97'),
(1164, 96, '_edit_last', '1'),
(1166, 96, '_yoast_wpseo_primary_category', '3'),
(1167, 96, '_yoast_wpseo_content_score', '90'),
(1168, 96, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(1171, 103, '_edit_last', '1'),
(1172, 103, '_edit_lock', '1613238694:1'),
(1174, 96, 'area', '備前'),
(1175, 96, '_area', 'field_60280db729309'),
(1176, 96, 'more_information', '（東京都から移住）'),
(1177, 96, '_more_information', 'field_60280dc32930a'),
(1178, 106, 'area', '備前'),
(1179, 106, '_area', 'field_60280db729309'),
(1180, 106, 'more_information', '（東京都から移住）'),
(1181, 106, '_more_information', 'field_60280dc32930a'),
(1183, 96, 'area_color', 'orange'),
(1184, 96, '_area_color', 'field_60280e35226f3'),
(1185, 108, 'area', '備前'),
(1186, 108, '_area', 'field_60280db729309'),
(1187, 108, 'more_information', '（東京都から移住）'),
(1188, 108, '_more_information', 'field_60280dc32930a'),
(1189, 108, 'area_color', 'yellow'),
(1190, 108, '_area_color', 'field_60280e35226f3'),
(1191, 109, '_edit_lock', '1613239154:1'),
(1193, 109, '_thumbnail_id', '100'),
(1194, 109, '_edit_last', '1'),
(1196, 109, '_yoast_wpseo_content_score', '90'),
(1197, 109, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(1198, 109, 'area', '備前'),
(1199, 109, '_area', 'field_60280db729309'),
(1200, 109, 'area_color', 'orange'),
(1201, 109, '_area_color', 'field_60280e35226f3'),
(1202, 109, 'more_information', '（大阪府から移住）'),
(1203, 109, '_more_information', 'field_60280dc32930a'),
(1204, 111, 'area', '備前'),
(1205, 111, '_area', 'field_60280db729309'),
(1206, 111, 'area_color', 'blue'),
(1207, 111, '_area_color', 'field_60280e35226f3'),
(1208, 111, 'more_information', '（大阪府から移住）'),
(1209, 111, '_more_information', 'field_60280dc32930a'),
(1210, 109, '_yoast_wpseo_primary_category', '3'),
(1211, 113, '_edit_lock', '1613238736:1'),
(1213, 113, '_edit_last', '1'),
(1215, 113, '_yoast_wpseo_content_score', '90'),
(1216, 113, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(1217, 113, 'area', '吉永'),
(1218, 113, '_area', 'field_60280db729309'),
(1219, 113, 'area_color', 'green'),
(1220, 113, '_area_color', 'field_60280e35226f3'),
(1221, 113, 'more_information', '（岡山市から移住）'),
(1222, 113, '_more_information', 'field_60280dc32930a'),
(1223, 115, 'area', '吉永'),
(1224, 115, '_area', 'field_60280db729309'),
(1225, 115, 'area_color', 'green'),
(1226, 115, '_area_color', 'field_60280e35226f3'),
(1227, 115, 'more_information', '（岡山市から移住）'),
(1228, 115, '_more_information', 'field_60280dc32930a'),
(1229, 113, '_yoast_wpseo_primary_category', '3'),
(1230, 113, '_thumbnail_id', '99'),
(1232, 116, '_edit_lock', '1613238623:1'),
(1234, 116, '_thumbnail_id', '98'),
(1235, 116, '_edit_last', '1'),
(1237, 116, '_yoast_wpseo_content_score', '90'),
(1238, 116, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(1239, 116, 'area', '日生'),
(1240, 116, '_area', 'field_60280db729309'),
(1241, 116, 'area_color', 'blue'),
(1242, 116, '_area_color', 'field_60280e35226f3'),
(1243, 116, 'more_information', '（京都府から移住）'),
(1244, 116, '_more_information', 'field_60280dc32930a'),
(1245, 118, 'area', '日生'),
(1246, 118, '_area', 'field_60280db729309'),
(1247, 118, 'area_color', 'blue'),
(1248, 118, '_area_color', 'field_60280e35226f3'),
(1249, 118, 'more_information', '（京都府から移住）'),
(1250, 118, '_more_information', 'field_60280dc32930a'),
(1251, 116, '_yoast_wpseo_primary_category', '3'),
(1255, 119, 'area', '備前'),
(1256, 119, '_area', 'field_60280db729309'),
(1257, 119, 'more_information', '（東京都から移住）'),
(1258, 119, '_more_information', 'field_60280dc32930a'),
(1259, 119, 'area_color', 'orange'),
(1260, 119, '_area_color', 'field_60280e35226f3'),
(1262, 109, '_encloseme', '1'),
(1263, 120, 'area', '備前'),
(1264, 120, '_area', 'field_60280db729309'),
(1265, 120, 'area_color', 'orange'),
(1266, 120, '_area_color', 'field_60280e35226f3'),
(1267, 120, 'more_information', '（大阪府から移住）'),
(1268, 120, '_more_information', 'field_60280dc32930a'),
(1269, 7, 'small_title_1', '移住を考える'),
(1270, 7, '_small_title_1', 'field_602813e399f18'),
(1271, 7, 'section_title_1', '備前市で暮らしてみようかな、と思ったら'),
(1272, 7, '_section_title_1', 'field_6028141099f19'),
(1273, 7, 'section_title_en', 'HOW TO'),
(1274, 7, '_section_title_en', 'field_6028142599f1a'),
(1275, 7, 'section_title_2', '移住の流れ'),
(1276, 7, '_section_title_2', 'field_602818030c02d'),
(1277, 7, 'step_1_title', '田舎暮らしを <br>イメージしよう'),
(1278, 7, '_step_1_title', 'field_6028143b99f1b'),
(1279, 7, 'step_2_title', '情報を集めて <br> 相談してみよう'),
(1280, 7, '_step_2_title', 'field_6028145499f1c'),
(1281, 7, 'step_3_title', '備前市に <br> 行ってみよう'),
(1282, 7, '_step_3_title', 'field_6028146b99f1d'),
(1283, 7, 'step_4_title', '住まい・仕事を <br> 探してみよう'),
(1284, 7, '_step_4_title', 'field_6028147a99f1e'),
(1285, 7, 'step_5_title', '備前市での新生活を <br>  楽しもう'),
(1286, 7, '_step_5_title', 'field_6028148299f1f'),
(1287, 7, 'description_1', 'あなたの家族、パートナーが移住先でやりたい事や実現したい夢を具体的にイメージすると、住む地域や仕事なども決めやすくなります！'),
(1288, 7, '_description_1', 'field_6028148c99f20'),
(1289, 7, 'description_2', '移住相談窓口で相談したり、移住セミナーに参加して現地の情報を収集しよう！自身に必要な条件を細かくリストアップすることも大切。'),
(1290, 7, '_description_2', 'field_602814ae99f21'),
(1291, 7, 'description_3', '現地に行き、地元の人と触れ合うことでリアルな生活情報を入手できます。移住体験住宅や移住調査宿泊費補助もあるので、ご活用ください。'),
(1292, 7, '_description_3', 'field_602814b099f22'),
(1293, 7, 'description_4', '仕事は、就職・起業・就農などの選択肢があり、就職フェアもあります。住まい選びは空家バンクで検索するのもオススメです。'),
(1294, 7, '_description_4', 'field_602814b299f23'),
(1295, 7, 'description_5', '引っ越しが終わったら近隣の方々に挨拶しましょう。地域に合ったお付き合いを心がけることで、早く地域に溶け込むことができます。'),
(1296, 7, '_description_5', 'field_602814b399f24'),
(1297, 7, 'house_links_links_0_link', 'a:3:{s:5:\"title\";s:15:\"空き家情報\";s:3:\"url\";s:23:\"https://www.ok-smile.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1298, 7, '_house_links_links_0_link', 'field_60281560980c2'),
(1299, 7, 'house_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"移住体験\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-74\";s:6:\"target\";s:6:\"_blank\";}'),
(1300, 7, '_house_links_links_1_link', 'field_60281560980c2'),
(1301, 7, 'house_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"住宅関連補助\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-77\";s:6:\"target\";s:6:\"_blank\";}'),
(1302, 7, '_house_links_links_2_link', 'field_60281560980c2'),
(1303, 7, 'house_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"分譲地情報\";s:3:\"url\";s:1:\"#\";s:6:\"target\";s:0:\"\";}'),
(1304, 7, '_house_links_links_3_link', 'field_60281560980c2'),
(1305, 7, 'house_links_links', '4'),
(1306, 7, '_house_links_links', 'field_60281550980c1'),
(1307, 7, 'house_links', ''),
(1308, 7, '_house_links', 'field_602814d8980bf'),
(1309, 7, 'work_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"ハローワーク\";s:3:\"url\";s:79:\"https://www.hellowork.careers/岡山県+備前市でのハローワーク求人\";s:6:\"target\";s:6:\"_blank\";}'),
(1310, 7, '_work_links_links_0_link', 'field_60281574980c5'),
(1311, 7, 'work_links_links_1_link', 'a:3:{s:5:\"title\";s:21:\"備前商工会議所\";s:3:\"url\";s:21:\"http://bizencci.or.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1312, 7, '_work_links_links_1_link', 'field_60281574980c5'),
(1313, 7, 'work_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"備前東商工会\";s:3:\"url\";s:37:\"http://www.okasci.or.jp/bizenhigashi/\";s:6:\"target\";s:6:\"_blank\";}'),
(1314, 7, '_work_links_links_2_link', 'field_60281574980c5'),
(1315, 7, 'work_links_links_3_link', 'a:3:{s:5:\"title\";s:6:\"起業\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/19/672.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1316, 7, '_work_links_links_3_link', 'field_60281574980c5'),
(1317, 7, 'work_links_links_4_link', 'a:3:{s:5:\"title\";s:15:\"移住支援金\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/site/bizen/987.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1318, 7, '_work_links_links_4_link', 'field_60281574980c5'),
(1319, 7, 'work_links_links', '5'),
(1320, 7, '_work_links_links', 'field_60281574980c4'),
(1321, 7, 'work_links', ''),
(1322, 7, '_work_links', 'field_60281574980c3'),
(1323, 7, 'life_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"くらしの情報\";s:3:\"url\";s:42:\"https://www.city.bizen.okayama.jp/life/1/#\";s:6:\"target\";s:6:\"_blank\";}'),
(1324, 7, '_life_links_links_0_link', 'field_6028157f980c8'),
(1325, 7, 'life_links_links_1_link', 'a:3:{s:5:\"title\";s:6:\"ごみ\";s:3:\"url\";s:46:\"https://www.city.bizen.okayama.jp/life/1/4/18/\";s:6:\"target\";s:6:\"_blank\";}'),
(1326, 7, '_life_links_links_1_link', 'field_6028157f980c8'),
(1327, 7, 'life_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"水道・下水道\";s:3:\"url\";s:43:\"https://www.city.bizen.okayama.jp/life/1/6/\";s:6:\"target\";s:6:\"_blank\";}'),
(1328, 7, '_life_links_links_2_link', 'field_6028157f980c8'),
(1329, 7, 'life_links_links_3_link', 'a:3:{s:5:\"title\";s:12:\"公共交通\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/11/769.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1330, 7, '_life_links_links_3_link', 'field_6028157f980c8'),
(1331, 7, 'life_links_links', '4'),
(1332, 7, '_life_links_links', 'field_6028157f980c7'),
(1333, 7, 'life_links', ''),
(1334, 7, '_life_links', 'field_6028157f980c6'),
(1335, 7, 'childs_links_links_0_link', 'a:3:{s:5:\"title\";s:12:\"幼児教育\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/soshiki/30/\";s:6:\"target\";s:6:\"_blank\";}'),
(1336, 7, '_childs_links_links_0_link', 'field_602816880579c'),
(1337, 7, 'childs_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"小中学校\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/29/200.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1338, 7, '_childs_links_links_1_link', 'field_602816880579c'),
(1339, 7, 'childs_links_links_2_link', 'a:3:{s:5:\"title\";s:15:\"子育て支援\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/life/1/1/2/\";s:6:\"target\";s:6:\"_blank\";}'),
(1340, 7, '_childs_links_links_2_link', 'field_602816880579c'),
(1341, 7, 'childs_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"子育て情報\";s:3:\"url\";s:33:\"https://touch-bizen.jimdofree.com\";s:6:\"target\";s:6:\"_blank\";}'),
(1342, 7, '_childs_links_links_3_link', 'field_602816880579c'),
(1343, 7, 'childs_links_links', '4'),
(1344, 7, '_childs_links_links', 'field_602816880579b'),
(1345, 7, 'childs_links', ''),
(1346, 7, '_childs_links', 'field_602816880579a'),
(1347, 157, 'poster_image', '37'),
(1348, 157, '_poster_image', 'field_60241cfb84d6e'),
(1349, 157, 'main_slider_0_image', '38'),
(1350, 157, '_main_slider_0_image', 'field_60241d2de1e18'),
(1351, 157, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1352, 157, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(1353, 157, 'main_slider_1_image', '38'),
(1354, 157, '_main_slider_1_image', 'field_60241d2de1e18'),
(1355, 157, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1356, 157, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(1357, 157, 'main_slider_2_image', '38'),
(1358, 157, '_main_slider_2_image', 'field_60241d2de1e18'),
(1359, 157, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1360, 157, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(1361, 157, 'main_slider', '3'),
(1362, 157, '_main_slider', 'field_60241d14e1e17'),
(1363, 157, 'main_slider_0_image_mobile', '41'),
(1364, 157, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(1365, 157, 'main_slider_1_image_mobile', '41'),
(1366, 157, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(1367, 157, 'main_slider_2_image_mobile', '41'),
(1368, 157, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(1369, 157, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(1370, 157, '_pickup_text', 'field_60241f7d2a361'),
(1371, 157, 'video_1_title', '「かたちづくる街」'),
(1372, 157, '_video_1_title', 'field_60241fdc2a363'),
(1373, 157, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1374, 157, '_video_1_embed_url', 'field_60241fe12a364'),
(1375, 157, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(1376, 157, '_video_1_description', 'field_6024200c2a365'),
(1377, 157, 'video_1', ''),
(1378, 157, '_video_1', 'field_60241fd22a362'),
(1379, 157, 'video_2_title', '「かたちづくる街」'),
(1380, 157, '_video_2_title', 'field_602420392a367'),
(1381, 157, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1382, 157, '_video_2_embed_url', 'field_602420392a368'),
(1383, 157, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(1384, 157, '_video_2_description', 'field_602420392a369'),
(1385, 157, 'video_2', ''),
(1386, 157, '_video_2', 'field_602420392a366'),
(1387, 157, 'small_title', '備前市を知ろう'),
(1388, 157, '_small_title', 'field_6026c19cc456b'),
(1389, 157, 'title_with_icon_left_text', 'いいね'),
(1390, 157, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(1391, 157, 'title_with_icon_icon', '78'),
(1392, 157, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(1393, 157, 'title_with_icon_right_text', '備前市'),
(1394, 157, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(1395, 157, 'title_with_icon', ''),
(1396, 157, '_title_with_icon', 'field_6026c1a5c456c'),
(1397, 157, 'section_title', '備前市ってこんなところ'),
(1398, 157, '_section_title', 'field_6026c212c4571'),
(1399, 157, 'map', '79'),
(1400, 157, '_map', 'field_6026c220c4572'),
(1401, 157, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(1402, 157, '_map_info_box_1', 'field_6026c2bcc82ea'),
(1403, 157, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(1404, 157, '_map_info_box_2', 'field_6026c2fbc82eb'),
(1405, 157, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(1406, 157, '_map_infomation', 'field_6026c234c4573'),
(1407, 157, 'information_boxes_title', '備前市の魅力'),
(1408, 157, '_information_boxes_title', 'field_6026c5cb5e01d'),
(1409, 157, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(1410, 157, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(1411, 157, 'information_boxes_0_box_background', '1'),
(1412, 157, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(1413, 157, 'information_boxes_0_icon', '90'),
(1414, 157, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(1415, 157, 'information_boxes_0_icon_class', 'icon-man'),
(1416, 157, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(1417, 157, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(1418, 157, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(1419, 157, 'information_boxes_1_box_background', '0'),
(1420, 157, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(1421, 157, 'information_boxes_1_icon', ''),
(1422, 157, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(1423, 157, 'information_boxes_1_icon_class', ''),
(1424, 157, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(1425, 157, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(1426, 157, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(1427, 157, 'information_boxes_2_box_background', '1'),
(1428, 157, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(1429, 157, 'information_boxes_2_icon', '88'),
(1430, 157, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(1431, 157, 'information_boxes_2_icon_class', 'icon-baby'),
(1432, 157, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(1433, 157, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(1434, 157, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(1435, 157, 'information_boxes_3_box_background', '0'),
(1436, 157, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(1437, 157, 'information_boxes_3_icon', ''),
(1438, 157, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(1439, 157, 'information_boxes_3_icon_class', ''),
(1440, 157, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(1441, 157, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(1442, 157, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(1443, 157, 'information_boxes_4_box_background', '0'),
(1444, 157, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(1445, 157, 'information_boxes_4_icon', ''),
(1446, 157, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(1447, 157, 'information_boxes_4_icon_class', ''),
(1448, 157, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(1449, 157, 'information_boxes', '12'),
(1450, 157, '_information_boxes', 'field_6026c4af5e017'),
(1451, 157, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(1452, 157, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(1453, 157, 'information_boxes_5_box_background', '1'),
(1454, 157, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(1455, 157, 'information_boxes_5_icon', ''),
(1456, 157, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(1457, 157, 'information_boxes_5_icon_class', ''),
(1458, 157, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(1459, 157, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(1460, 157, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(1461, 157, 'information_boxes_6_box_background', '0'),
(1462, 157, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(1463, 157, 'information_boxes_6_icon', ''),
(1464, 157, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(1465, 157, 'information_boxes_6_icon_class', ''),
(1466, 157, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(1467, 157, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(1468, 157, '_information_boxes_7_info_text', 'field_6026c4f15e018'),
(1469, 157, 'information_boxes_7_box_background', '1'),
(1470, 157, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(1471, 157, 'information_boxes_7_icon', '91'),
(1472, 157, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(1473, 157, 'information_boxes_7_icon_class', 'icon-text'),
(1474, 157, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(1475, 157, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(1476, 157, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(1477, 157, 'information_boxes_8_box_background', '1'),
(1478, 157, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(1479, 157, 'information_boxes_8_icon', ''),
(1480, 157, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(1481, 157, 'information_boxes_8_icon_class', ''),
(1482, 157, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(1483, 157, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(1484, 157, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(1485, 157, 'information_boxes_9_box_background', '0'),
(1486, 157, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(1487, 157, 'information_boxes_9_icon', '89'),
(1488, 157, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(1489, 157, 'information_boxes_9_icon_class', 'icon-house'),
(1490, 157, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(1491, 157, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(1492, 157, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(1493, 157, 'information_boxes_10_box_background', '1'),
(1494, 157, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(1495, 157, 'information_boxes_10_icon', ''),
(1496, 157, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(1497, 157, 'information_boxes_10_icon_class', ''),
(1498, 157, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(1499, 157, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(1500, 157, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(1501, 157, 'information_boxes_11_box_background', '0'),
(1502, 157, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(1503, 157, 'information_boxes_11_icon', ''),
(1504, 157, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(1505, 157, 'information_boxes_11_icon_class', ''),
(1506, 157, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(1507, 157, 'small_title_1', '移住を考える'),
(1508, 157, '_small_title_1', 'field_602813e399f18'),
(1509, 157, 'section_title_1', '備前市で暮らしてみようかな、と思ったら'),
(1510, 157, '_section_title_1', 'field_6028141099f19'),
(1511, 157, 'section_title_en', 'HOW TO'),
(1512, 157, '_section_title_en', 'field_6028142599f1a'),
(1513, 157, 'section_title_2', '移住の流れ'),
(1514, 157, '_section_title_2', 'field_602818030c02d'),
(1515, 157, 'step_1_title', '田舎暮らしを <br>イメージしよう'),
(1516, 157, '_step_1_title', 'field_6028143b99f1b'),
(1517, 157, 'step_2_title', '情報を集めて <br> 相談してみよう'),
(1518, 157, '_step_2_title', 'field_6028145499f1c'),
(1519, 157, 'step_3_title', '備前市に <br> 行ってみよう'),
(1520, 157, '_step_3_title', 'field_6028146b99f1d'),
(1521, 157, 'step_4_title', '住まい・仕事を <br> 探してみよう'),
(1522, 157, '_step_4_title', 'field_6028147a99f1e'),
(1523, 157, 'step_5_title', '備前市での新生活を <br>  楽しもう'),
(1524, 157, '_step_5_title', 'field_6028148299f1f'),
(1525, 157, 'description_1', 'あなたの家族、パートナーが移住先でやりたい事や実現したい夢を具体的にイメージすると、住む地域や仕事なども決めやすくなります！'),
(1526, 157, '_description_1', 'field_6028148c99f20'),
(1527, 157, 'description_2', '移住相談窓口で相談したり、移住セミナーに参加して現地の情報を収集しよう！自身に必要な条件を細かくリストアップすることも大切。'),
(1528, 157, '_description_2', 'field_602814ae99f21'),
(1529, 157, 'description_3', '現地に行き、地元の人と触れ合うことでリアルな生活情報を入手できます。移住体験住宅や移住調査宿泊費補助もあるので、ご活用ください。'),
(1530, 157, '_description_3', 'field_602814b099f22'),
(1531, 157, 'description_4', '仕事は、就職・起業・就農などの選択肢があり、就職フェアもあります。住まい選びは空家バンクで検索するのもオススメです。'),
(1532, 157, '_description_4', 'field_602814b299f23'),
(1533, 157, 'description_5', '引っ越しが終わったら近隣の方々に挨拶しましょう。地域に合ったお付き合いを心がけることで、早く地域に溶け込むことができます。'),
(1534, 157, '_description_5', 'field_602814b399f24'),
(1535, 157, 'house_links_links_0_link', 'a:3:{s:5:\"title\";s:15:\"空き家情報\";s:3:\"url\";s:23:\"https://www.ok-smile.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1536, 157, '_house_links_links_0_link', 'field_60281560980c2'),
(1537, 157, 'house_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"移住体験\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-74\";s:6:\"target\";s:6:\"_blank\";}'),
(1538, 157, '_house_links_links_1_link', 'field_60281560980c2'),
(1539, 157, 'house_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"住宅関連補助\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-77\";s:6:\"target\";s:6:\"_blank\";}'),
(1540, 157, '_house_links_links_2_link', 'field_60281560980c2');
INSERT INTO `bzn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1541, 157, 'house_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"分譲地情報\";s:3:\"url\";s:1:\"#\";s:6:\"target\";s:0:\"\";}'),
(1542, 157, '_house_links_links_3_link', 'field_60281560980c2'),
(1543, 157, 'house_links_links', '4'),
(1544, 157, '_house_links_links', 'field_60281550980c1'),
(1545, 157, 'house_links', ''),
(1546, 157, '_house_links', 'field_602814d8980bf'),
(1547, 157, 'work_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"ハローワーク\";s:3:\"url\";s:79:\"https://www.hellowork.careers/岡山県+備前市でのハローワーク求人\";s:6:\"target\";s:6:\"_blank\";}'),
(1548, 157, '_work_links_links_0_link', 'field_60281574980c5'),
(1549, 157, 'work_links_links_1_link', 'a:3:{s:5:\"title\";s:21:\"備前商工会議所\";s:3:\"url\";s:21:\"http://bizencci.or.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1550, 157, '_work_links_links_1_link', 'field_60281574980c5'),
(1551, 157, 'work_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"備前東商工会\";s:3:\"url\";s:37:\"http://www.okasci.or.jp/bizenhigashi/\";s:6:\"target\";s:6:\"_blank\";}'),
(1552, 157, '_work_links_links_2_link', 'field_60281574980c5'),
(1553, 157, 'work_links_links_3_link', 'a:3:{s:5:\"title\";s:6:\"起業\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/19/672.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1554, 157, '_work_links_links_3_link', 'field_60281574980c5'),
(1555, 157, 'work_links_links_4_link', 'a:3:{s:5:\"title\";s:15:\"移住支援金\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/site/bizen/987.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1556, 157, '_work_links_links_4_link', 'field_60281574980c5'),
(1557, 157, 'work_links_links', '5'),
(1558, 157, '_work_links_links', 'field_60281574980c4'),
(1559, 157, 'work_links', ''),
(1560, 157, '_work_links', 'field_60281574980c3'),
(1561, 157, 'life_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"くらしの情報\";s:3:\"url\";s:42:\"https://www.city.bizen.okayama.jp/life/1/#\";s:6:\"target\";s:6:\"_blank\";}'),
(1562, 157, '_life_links_links_0_link', 'field_6028157f980c8'),
(1563, 157, 'life_links_links_1_link', 'a:3:{s:5:\"title\";s:6:\"ごみ\";s:3:\"url\";s:46:\"https://www.city.bizen.okayama.jp/life/1/4/18/\";s:6:\"target\";s:6:\"_blank\";}'),
(1564, 157, '_life_links_links_1_link', 'field_6028157f980c8'),
(1565, 157, 'life_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"水道・下水道\";s:3:\"url\";s:43:\"https://www.city.bizen.okayama.jp/life/1/6/\";s:6:\"target\";s:6:\"_blank\";}'),
(1566, 157, '_life_links_links_2_link', 'field_6028157f980c8'),
(1567, 157, 'life_links_links_3_link', 'a:3:{s:5:\"title\";s:12:\"公共交通\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/11/769.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1568, 157, '_life_links_links_3_link', 'field_6028157f980c8'),
(1569, 157, 'life_links_links', '4'),
(1570, 157, '_life_links_links', 'field_6028157f980c7'),
(1571, 157, 'life_links', ''),
(1572, 157, '_life_links', 'field_6028157f980c6'),
(1573, 157, 'childs_links_links_0_link', 'a:3:{s:5:\"title\";s:12:\"幼児教育\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/soshiki/30/\";s:6:\"target\";s:6:\"_blank\";}'),
(1574, 157, '_childs_links_links_0_link', 'field_602816880579c'),
(1575, 157, 'childs_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"小中学校\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/29/200.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1576, 157, '_childs_links_links_1_link', 'field_602816880579c'),
(1577, 157, 'childs_links_links_2_link', 'a:3:{s:5:\"title\";s:15:\"子育て支援\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/life/1/1/2/\";s:6:\"target\";s:6:\"_blank\";}'),
(1578, 157, '_childs_links_links_2_link', 'field_602816880579c'),
(1579, 157, 'childs_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"子育て情報\";s:3:\"url\";s:33:\"https://touch-bizen.jimdofree.com\";s:6:\"target\";s:6:\"_blank\";}'),
(1580, 157, '_childs_links_links_3_link', 'field_602816880579c'),
(1581, 157, 'childs_links_links', '4'),
(1582, 157, '_childs_links_links', 'field_602816880579b'),
(1583, 157, 'childs_links', ''),
(1584, 157, '_childs_links', 'field_602816880579a'),
(1585, 7, 'section_title_3', '支援・補助'),
(1586, 7, '_section_title_3', 'field_60281a96de1ab'),
(1587, 159, 'poster_image', '37'),
(1588, 159, '_poster_image', 'field_60241cfb84d6e'),
(1589, 159, 'main_slider_0_image', '38'),
(1590, 159, '_main_slider_0_image', 'field_60241d2de1e18'),
(1591, 159, 'main_slider_0_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1592, 159, '_main_slider_0_slider_text', 'field_60241d3ee1e19'),
(1593, 159, 'main_slider_1_image', '38'),
(1594, 159, '_main_slider_1_image', 'field_60241d2de1e18'),
(1595, 159, 'main_slider_1_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1596, 159, '_main_slider_1_slider_text', 'field_60241d3ee1e19'),
(1597, 159, 'main_slider_2_image', '38'),
(1598, 159, '_main_slider_2_image', 'field_60241d2de1e18'),
(1599, 159, 'main_slider_2_slider_text', 'それぞれの未来を \r\nかたちにする'),
(1600, 159, '_main_slider_2_slider_text', 'field_60241d3ee1e19'),
(1601, 159, 'main_slider', '3'),
(1602, 159, '_main_slider', 'field_60241d14e1e17'),
(1603, 159, 'main_slider_0_image_mobile', '41'),
(1604, 159, '_main_slider_0_image_mobile', 'field_60241e6b1320e'),
(1605, 159, 'main_slider_1_image_mobile', '41'),
(1606, 159, '_main_slider_1_image_mobile', 'field_60241e6b1320e'),
(1607, 159, 'main_slider_2_image_mobile', '41'),
(1608, 159, '_main_slider_2_image_mobile', 'field_60241e6b1320e'),
(1609, 159, 'pickup_text', '「ハルカの陶」主題歌のサボテン高水さんが、備前市を題材に作詞・作曲をした「かたちづくる街」。\r\n自らを主人公にこの街を舞台に歩き出して欲しい、という想いが込められています。'),
(1610, 159, '_pickup_text', 'field_60241f7d2a361'),
(1611, 159, 'video_1_title', '「かたちづくる街」'),
(1612, 159, '_video_1_title', 'field_60241fdc2a363'),
(1613, 159, 'video_1_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1614, 159, '_video_1_embed_url', 'field_60241fe12a364'),
(1615, 159, 'video_1_description', '就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2 権やぼぐッ省港訃ラ。'),
(1616, 159, '_video_1_description', 'field_6024200c2a365'),
(1617, 159, 'video_1', ''),
(1618, 159, '_video_1', 'field_60241fd22a362'),
(1619, 159, 'video_2_title', '「かたちづくる街」'),
(1620, 159, '_video_2_title', 'field_602420392a367'),
(1621, 159, 'video_2_embed_url', 'https://www.youtube.com/embed/iL5LrGtKPmM'),
(1622, 159, '_video_2_embed_url', 'field_602420392a368'),
(1623, 159, 'video_2_description', '価ナ渡姿東会説超た松王じねトレ社豊テシ演意ムコナ 原偽ヲ天件むぜのご連舎でん。'),
(1624, 159, '_video_2_description', 'field_602420392a369'),
(1625, 159, 'video_2', ''),
(1626, 159, '_video_2', 'field_602420392a366'),
(1627, 159, 'small_title', '備前市を知ろう'),
(1628, 159, '_small_title', 'field_6026c19cc456b'),
(1629, 159, 'title_with_icon_left_text', 'いいね'),
(1630, 159, '_title_with_icon_left_text', 'field_6026c1c2c456d'),
(1631, 159, 'title_with_icon_icon', '78'),
(1632, 159, '_title_with_icon_icon', 'field_6026c1e1c456f'),
(1633, 159, 'title_with_icon_right_text', '備前市'),
(1634, 159, '_title_with_icon_right_text', 'field_6026c1cac456e'),
(1635, 159, 'title_with_icon', ''),
(1636, 159, '_title_with_icon', 'field_6026c1a5c456c'),
(1637, 159, 'section_title', '備前市ってこんなところ'),
(1638, 159, '_section_title', 'field_6026c212c4571'),
(1639, 159, 'map', '79'),
(1640, 159, '_map', 'field_6026c220c4572'),
(1641, 159, 'map_info_box_1', '<span>人口 <br>\r\n<strong>33,558人</strong></span>\r\n<span>世帯数 <br>\r\n<strong>15,666世帯</strong></span>\r\n<small>(2020年10月末日現在)</small>'),
(1642, 159, '_map_info_box_1', 'field_6026c2bcc82ea'),
(1643, 159, 'map_info_box_2', '<div class=\"text-center\">\r\n  <span>アクセス</span>\r\n</div>\r\n<p>電車：大阪から約2時間 <br> 「岡山駅」からJR赤穂線で40分 <br> 自動車：山陽自動車備前ICから15分</p>'),
(1644, 159, '_map_info_box_2', 'field_6026c2fbc82eb'),
(1645, 159, 'map_infomation', '備前市は3つのエリアに分かれており、それぞれに特徴の違いが感じられます。</p>\r\n<span class=\"orange-color\">備前エリア</span>は、一千年の伝統を誇る陶芸の里、備前焼の産地として知られ、日本遺産に認定された旧閑谷学校と共に伝統を感じる街です。\r\n<span class=\"green-color\">吉永エリア</span>は、山に囲まれており、自然の春夏秋冬を肌で感じていただけるのどかな風景が一面に広がっています。八塔寺ふるさと村は今も、かや葺き民家が点在しています。\r\n<span class=\"blue-color\">日生エリア</span>は、瀬戸内海に面しており、大小13の島があります。美味しい魚と牡蠣が多く取れる、活気ある漁師まちです。五味の市では季節の旬な魚が勢ぞろいします。'),
(1646, 159, '_map_infomation', 'field_6026c234c4573'),
(1647, 159, 'information_boxes_title', '備前市の魅力'),
(1648, 159, '_information_boxes_title', 'field_6026c5cb5e01d'),
(1649, 159, 'information_boxes_0_info_text', '18歳まで医療費が無料！'),
(1650, 159, '_information_boxes_0_info_text', 'field_6026c4f15e018'),
(1651, 159, 'information_boxes_0_box_background', '1'),
(1652, 159, '_information_boxes_0_box_background', 'field_6026c5035e019'),
(1653, 159, 'information_boxes_0_icon', '90'),
(1654, 159, '_information_boxes_0_icon', 'field_6026c5445e01b'),
(1655, 159, 'information_boxes_0_icon_class', 'icon-man'),
(1656, 159, '_information_boxes_0_icon_class', 'field_6026c5a75e01c'),
(1657, 159, 'information_boxes_1_info_text', '市内に市立病院が３つ！\r\n（備前、日生、吉永）'),
(1658, 159, '_information_boxes_1_info_text', 'field_6026c4f15e018'),
(1659, 159, 'information_boxes_1_box_background', '0'),
(1660, 159, '_information_boxes_1_box_background', 'field_6026c5035e019'),
(1661, 159, 'information_boxes_1_icon', ''),
(1662, 159, '_information_boxes_1_icon', 'field_6026c5445e01b'),
(1663, 159, 'information_boxes_1_icon_class', ''),
(1664, 159, '_information_boxes_1_icon_class', 'field_6026c5a75e01c'),
(1665, 159, 'information_boxes_2_info_text', '0歳時〜保育料が無料！'),
(1666, 159, '_information_boxes_2_info_text', 'field_6026c4f15e018'),
(1667, 159, 'information_boxes_2_box_background', '1'),
(1668, 159, '_information_boxes_2_box_background', 'field_6026c5035e019'),
(1669, 159, 'information_boxes_2_icon', '88'),
(1670, 159, '_information_boxes_2_icon', 'field_6026c5445e01b'),
(1671, 159, 'information_boxes_2_icon_class', 'icon-baby'),
(1672, 159, '_information_boxes_2_icon_class', 'field_6026c5a75e01c'),
(1673, 159, 'information_boxes_3_info_text', '給食費が第2子半額、\r\n 第3子以降無料！'),
(1674, 159, '_information_boxes_3_info_text', 'field_6026c4f15e018'),
(1675, 159, 'information_boxes_3_box_background', '0'),
(1676, 159, '_information_boxes_3_box_background', 'field_6026c5035e019'),
(1677, 159, 'information_boxes_3_icon', ''),
(1678, 159, '_information_boxes_3_icon', 'field_6026c5445e01b'),
(1679, 159, 'information_boxes_3_icon_class', ''),
(1680, 159, '_information_boxes_3_icon_class', 'field_6026c5a75e01c'),
(1681, 159, 'information_boxes_4_info_text', '子育て拠点施設が多い！'),
(1682, 159, '_information_boxes_4_info_text', 'field_6026c4f15e018'),
(1683, 159, 'information_boxes_4_box_background', '0'),
(1684, 159, '_information_boxes_4_box_background', 'field_6026c5035e019'),
(1685, 159, 'information_boxes_4_icon', ''),
(1686, 159, '_information_boxes_4_icon', 'field_6026c5445e01b'),
(1687, 159, 'information_boxes_4_icon_class', ''),
(1688, 159, '_information_boxes_4_icon_class', 'field_6026c5a75e01c'),
(1689, 159, 'information_boxes', '12'),
(1690, 159, '_information_boxes', 'field_6026c4af5e017'),
(1691, 159, 'information_boxes_5_info_text', 'フルーツ、魚介類が美味しく \r\nご当地グルメ「日生カキオコ」\r\n は大人気！'),
(1692, 159, '_information_boxes_5_info_text', 'field_6026c4f15e018'),
(1693, 159, 'information_boxes_5_box_background', '1'),
(1694, 159, '_information_boxes_5_box_background', 'field_6026c5035e019'),
(1695, 159, 'information_boxes_5_icon', ''),
(1696, 159, '_information_boxes_5_icon', 'field_6026c5445e01b'),
(1697, 159, 'information_boxes_5_icon_class', ''),
(1698, 159, '_information_boxes_5_icon_class', 'field_6026c5a75e01c'),
(1699, 159, 'information_boxes_6_info_text', '日本遺産が２つ！ \r\n（旧閑谷学校、備前焼）'),
(1700, 159, '_information_boxes_6_info_text', 'field_6026c4f15e018'),
(1701, 159, 'information_boxes_6_box_background', '0'),
(1702, 159, '_information_boxes_6_box_background', 'field_6026c5035e019'),
(1703, 159, 'information_boxes_6_icon', ''),
(1704, 159, '_information_boxes_6_icon', 'field_6026c5445e01b'),
(1705, 159, 'information_boxes_6_icon_class', ''),
(1706, 159, '_information_boxes_6_icon_class', 'field_6026c5a75e01c'),
(1707, 159, 'information_boxes_7_info_text', '市出身のプロ野球選手が\r\n続々と輩出！'),
(1708, 159, '_information_boxes_7_info_text', 'field_6026c4f15e018'),
(1709, 159, 'information_boxes_7_box_background', '1'),
(1710, 159, '_information_boxes_7_box_background', 'field_6026c5035e019'),
(1711, 159, 'information_boxes_7_icon', '91'),
(1712, 159, '_information_boxes_7_icon', 'field_6026c5445e01b'),
(1713, 159, 'information_boxes_7_icon_class', 'icon-text'),
(1714, 159, '_information_boxes_7_icon_class', 'field_6026c5a75e01c'),
(1715, 159, 'information_boxes_8_info_text', '住宅リフォーム補助、\r\n家賃補助、\r\n空き家購入補助もあり！'),
(1716, 159, '_information_boxes_8_info_text', 'field_6026c4f15e018'),
(1717, 159, 'information_boxes_8_box_background', '1'),
(1718, 159, '_information_boxes_8_box_background', 'field_6026c5035e019'),
(1719, 159, 'information_boxes_8_icon', ''),
(1720, 159, '_information_boxes_8_icon', 'field_6026c5445e01b'),
(1721, 159, 'information_boxes_8_icon_class', ''),
(1722, 159, '_information_boxes_8_icon_class', 'field_6026c5a75e01c'),
(1723, 159, 'information_boxes_9_info_text', '新築住宅補助金100万円！'),
(1724, 159, '_information_boxes_9_info_text', 'field_6026c4f15e018'),
(1725, 159, 'information_boxes_9_box_background', '0'),
(1726, 159, '_information_boxes_9_box_background', 'field_6026c5035e019'),
(1727, 159, 'information_boxes_9_icon', '89'),
(1728, 159, '_information_boxes_9_icon', 'field_6026c5445e01b'),
(1729, 159, 'information_boxes_9_icon_class', 'icon-house'),
(1730, 159, '_information_boxes_9_icon_class', 'field_6026c5a75e01c'),
(1731, 159, 'information_boxes_10_info_text', '市内ほぼ全域に \r\n光回線が開通！'),
(1732, 159, '_information_boxes_10_info_text', 'field_6026c4f15e018'),
(1733, 159, 'information_boxes_10_box_background', '1'),
(1734, 159, '_information_boxes_10_box_background', 'field_6026c5035e019'),
(1735, 159, 'information_boxes_10_icon', ''),
(1736, 159, '_information_boxes_10_icon', 'field_6026c5445e01b'),
(1737, 159, 'information_boxes_10_icon_class', ''),
(1738, 159, '_information_boxes_10_icon_class', 'field_6026c5a75e01c'),
(1739, 159, 'information_boxes_11_info_text', '耐火煉瓦、ベアリング、\r\n活性炭などトップシェアの \r\n製造企業が立地！'),
(1740, 159, '_information_boxes_11_info_text', 'field_6026c4f15e018'),
(1741, 159, 'information_boxes_11_box_background', '0'),
(1742, 159, '_information_boxes_11_box_background', 'field_6026c5035e019'),
(1743, 159, 'information_boxes_11_icon', ''),
(1744, 159, '_information_boxes_11_icon', 'field_6026c5445e01b'),
(1745, 159, 'information_boxes_11_icon_class', ''),
(1746, 159, '_information_boxes_11_icon_class', 'field_6026c5a75e01c'),
(1747, 159, 'small_title_1', '移住を考える'),
(1748, 159, '_small_title_1', 'field_602813e399f18'),
(1749, 159, 'section_title_1', '備前市で暮らしてみようかな、と思ったら'),
(1750, 159, '_section_title_1', 'field_6028141099f19'),
(1751, 159, 'section_title_en', 'HOW TO'),
(1752, 159, '_section_title_en', 'field_6028142599f1a'),
(1753, 159, 'section_title_2', '移住の流れ'),
(1754, 159, '_section_title_2', 'field_602818030c02d'),
(1755, 159, 'step_1_title', '田舎暮らしを <br>イメージしよう'),
(1756, 159, '_step_1_title', 'field_6028143b99f1b'),
(1757, 159, 'step_2_title', '情報を集めて <br> 相談してみよう'),
(1758, 159, '_step_2_title', 'field_6028145499f1c'),
(1759, 159, 'step_3_title', '備前市に <br> 行ってみよう'),
(1760, 159, '_step_3_title', 'field_6028146b99f1d'),
(1761, 159, 'step_4_title', '住まい・仕事を <br> 探してみよう'),
(1762, 159, '_step_4_title', 'field_6028147a99f1e'),
(1763, 159, 'step_5_title', '備前市での新生活を <br>  楽しもう'),
(1764, 159, '_step_5_title', 'field_6028148299f1f'),
(1765, 159, 'description_1', 'あなたの家族、パートナーが移住先でやりたい事や実現したい夢を具体的にイメージすると、住む地域や仕事なども決めやすくなります！'),
(1766, 159, '_description_1', 'field_6028148c99f20'),
(1767, 159, 'description_2', '移住相談窓口で相談したり、移住セミナーに参加して現地の情報を収集しよう！自身に必要な条件を細かくリストアップすることも大切。'),
(1768, 159, '_description_2', 'field_602814ae99f21'),
(1769, 159, 'description_3', '現地に行き、地元の人と触れ合うことでリアルな生活情報を入手できます。移住体験住宅や移住調査宿泊費補助もあるので、ご活用ください。'),
(1770, 159, '_description_3', 'field_602814b099f22'),
(1771, 159, 'description_4', '仕事は、就職・起業・就農などの選択肢があり、就職フェアもあります。住まい選びは空家バンクで検索するのもオススメです。'),
(1772, 159, '_description_4', 'field_602814b299f23'),
(1773, 159, 'description_5', '引っ越しが終わったら近隣の方々に挨拶しましょう。地域に合ったお付き合いを心がけることで、早く地域に溶け込むことができます。'),
(1774, 159, '_description_5', 'field_602814b399f24'),
(1775, 159, 'house_links_links_0_link', 'a:3:{s:5:\"title\";s:15:\"空き家情報\";s:3:\"url\";s:23:\"https://www.ok-smile.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1776, 159, '_house_links_links_0_link', 'field_60281560980c2'),
(1777, 159, 'house_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"移住体験\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-74\";s:6:\"target\";s:6:\"_blank\";}'),
(1778, 159, '_house_links_links_1_link', 'field_60281560980c2'),
(1779, 159, 'house_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"住宅関連補助\";s:3:\"url\";s:54:\"https://www.city.bizen.okayama.jp/site/bizen/list16-77\";s:6:\"target\";s:6:\"_blank\";}'),
(1780, 159, '_house_links_links_2_link', 'field_60281560980c2'),
(1781, 159, 'house_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"分譲地情報\";s:3:\"url\";s:1:\"#\";s:6:\"target\";s:0:\"\";}'),
(1782, 159, '_house_links_links_3_link', 'field_60281560980c2'),
(1783, 159, 'house_links_links', '4'),
(1784, 159, '_house_links_links', 'field_60281550980c1'),
(1785, 159, 'house_links', ''),
(1786, 159, '_house_links', 'field_602814d8980bf'),
(1787, 159, 'work_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"ハローワーク\";s:3:\"url\";s:79:\"https://www.hellowork.careers/岡山県+備前市でのハローワーク求人\";s:6:\"target\";s:6:\"_blank\";}'),
(1788, 159, '_work_links_links_0_link', 'field_60281574980c5'),
(1789, 159, 'work_links_links_1_link', 'a:3:{s:5:\"title\";s:21:\"備前商工会議所\";s:3:\"url\";s:21:\"http://bizencci.or.jp\";s:6:\"target\";s:6:\"_blank\";}'),
(1790, 159, '_work_links_links_1_link', 'field_60281574980c5'),
(1791, 159, 'work_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"備前東商工会\";s:3:\"url\";s:37:\"http://www.okasci.or.jp/bizenhigashi/\";s:6:\"target\";s:6:\"_blank\";}'),
(1792, 159, '_work_links_links_2_link', 'field_60281574980c5'),
(1793, 159, 'work_links_links_3_link', 'a:3:{s:5:\"title\";s:6:\"起業\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/19/672.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1794, 159, '_work_links_links_3_link', 'field_60281574980c5'),
(1795, 159, 'work_links_links_4_link', 'a:3:{s:5:\"title\";s:15:\"移住支援金\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/site/bizen/987.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1796, 159, '_work_links_links_4_link', 'field_60281574980c5'),
(1797, 159, 'work_links_links', '5'),
(1798, 159, '_work_links_links', 'field_60281574980c4'),
(1799, 159, 'work_links', ''),
(1800, 159, '_work_links', 'field_60281574980c3'),
(1801, 159, 'life_links_links_0_link', 'a:3:{s:5:\"title\";s:18:\"くらしの情報\";s:3:\"url\";s:42:\"https://www.city.bizen.okayama.jp/life/1/#\";s:6:\"target\";s:6:\"_blank\";}'),
(1802, 159, '_life_links_links_0_link', 'field_6028157f980c8'),
(1803, 159, 'life_links_links_1_link', 'a:3:{s:5:\"title\";s:6:\"ごみ\";s:3:\"url\";s:46:\"https://www.city.bizen.okayama.jp/life/1/4/18/\";s:6:\"target\";s:6:\"_blank\";}'),
(1804, 159, '_life_links_links_1_link', 'field_6028157f980c8'),
(1805, 159, 'life_links_links_2_link', 'a:3:{s:5:\"title\";s:18:\"水道・下水道\";s:3:\"url\";s:43:\"https://www.city.bizen.okayama.jp/life/1/6/\";s:6:\"target\";s:6:\"_blank\";}'),
(1806, 159, '_life_links_links_2_link', 'field_6028157f980c8'),
(1807, 159, 'life_links_links_3_link', 'a:3:{s:5:\"title\";s:12:\"公共交通\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/11/769.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1808, 159, '_life_links_links_3_link', 'field_6028157f980c8'),
(1809, 159, 'life_links_links', '4'),
(1810, 159, '_life_links_links', 'field_6028157f980c7'),
(1811, 159, 'life_links', ''),
(1812, 159, '_life_links', 'field_6028157f980c6'),
(1813, 159, 'childs_links_links_0_link', 'a:3:{s:5:\"title\";s:12:\"幼児教育\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/soshiki/30/\";s:6:\"target\";s:6:\"_blank\";}'),
(1814, 159, '_childs_links_links_0_link', 'field_602816880579c'),
(1815, 159, 'childs_links_links_1_link', 'a:3:{s:5:\"title\";s:12:\"小中学校\";s:3:\"url\";s:53:\"https://www.city.bizen.okayama.jp/soshiki/29/200.html\";s:6:\"target\";s:6:\"_blank\";}'),
(1816, 159, '_childs_links_links_1_link', 'field_602816880579c'),
(1817, 159, 'childs_links_links_2_link', 'a:3:{s:5:\"title\";s:15:\"子育て支援\";s:3:\"url\";s:45:\"https://www.city.bizen.okayama.jp/life/1/1/2/\";s:6:\"target\";s:6:\"_blank\";}'),
(1818, 159, '_childs_links_links_2_link', 'field_602816880579c'),
(1819, 159, 'childs_links_links_3_link', 'a:3:{s:5:\"title\";s:15:\"子育て情報\";s:3:\"url\";s:33:\"https://touch-bizen.jimdofree.com\";s:6:\"target\";s:6:\"_blank\";}'),
(1820, 159, '_childs_links_links_3_link', 'field_602816880579c'),
(1821, 159, 'childs_links_links', '4'),
(1822, 159, '_childs_links_links', 'field_602816880579b'),
(1823, 159, 'childs_links', ''),
(1824, 159, '_childs_links', 'field_602816880579a'),
(1825, 159, 'section_title_3', '支援・補助'),
(1826, 159, '_section_title_3', 'field_60281a96de1ab');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_posts`
--

CREATE TABLE `bzn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_posts`
--

INSERT INTO `bzn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3, 1, '2021-02-03 17:35:30', '2021-02-03 17:35:30', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://bizen.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-02-03 17:35:30', '2021-02-03 17:35:30', '', 0, 'http://bizen.local/?page_id=3', 0, 'page', '', 0),
(7, 1, '2021-02-03 17:37:32', '2021-02-03 17:37:32', '', 'ホーム', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2021-02-13 18:30:17', '2021-02-13 18:30:17', '', 0, 'http://bizen.local/?page_id=7', 0, 'page', '', 0),
(8, 1, '2021-02-03 17:37:32', '2021-02-03 17:37:32', '', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-03 17:37:32', '2021-02-03 17:37:32', '', 7, 'http://bizen.local/2021/02/03/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2021-02-10 17:09:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', '', '', '', '', '', '', '2021-02-10 17:09:25', '0000-00-00 00:00:00', '', 0, 'http://bizen.local/?p=9', 0, 'post', '', 0),
(10, 1, '2021-02-10 17:21:08', '2021-02-10 17:21:08', ' ', '', '', 'publish', 'closed', 'closed', '', '10', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2021-02-10 17:21:09', '2021-02-10 17:21:09', ' ', '', '', 'publish', 'closed', 'closed', '', '11', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=11', 3, 'nav_menu_item', '', 0),
(12, 1, '2021-02-10 17:21:09', '2021-02-10 17:21:09', '', 'お問い合わせ', '', 'publish', 'closed', 'closed', '', '%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=12', 6, 'nav_menu_item', '', 0),
(13, 1, '2021-02-10 17:22:31', '2021-02-10 17:22:31', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-10 17:22:31', '2021-02-10 17:22:31', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2021-02-10 17:23:57', '2021-02-10 17:23:57', '', '備前市を知ろう', '', 'publish', 'closed', 'closed', '', '%e5%82%99%e5%89%8d%e5%b8%82%e3%82%92%e7%9f%a5%e3%82%8d%e3%81%86', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=14', 2, 'nav_menu_item', '', 0),
(15, 1, '2021-02-10 17:23:57', '2021-02-10 17:23:57', '', '移住を考える', '', 'publish', 'closed', 'closed', '', '%e7%a7%bb%e4%bd%8f%e3%82%92%e8%80%83%e3%81%88%e3%82%8b', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=15', 4, 'nav_menu_item', '', 0),
(16, 1, '2021-02-10 17:23:57', '2021-02-10 17:23:57', '', '支援・補助', '', 'publish', 'closed', 'closed', '', '%e6%94%af%e6%8f%b4%e3%83%bb%e8%a3%9c%e5%8a%a9', '', '', '2021-02-10 17:27:35', '2021-02-10 17:27:35', '', 0, 'http://bizen.local/?p=16', 5, 'nav_menu_item', '', 0),
(17, 1, '2021-02-10 17:31:06', '2021-02-10 17:31:06', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2021-02-10 17:32:03', '2021-02-10 17:32:03', '', 0, 'http://bizen.local/?p=17', 1, 'nav_menu_item', '', 0),
(18, 1, '2021-02-10 17:31:07', '2021-02-10 17:31:07', '', '備前市を知ろう', '', 'publish', 'closed', 'closed', '', '%e5%82%99%e5%89%8d%e5%b8%82%e3%82%92%e7%9f%a5%e3%82%8d%e3%81%86-2', '', '', '2021-02-10 17:32:04', '2021-02-10 17:32:04', '', 0, 'http://bizen.local/?p=18', 2, 'nav_menu_item', '', 0),
(19, 1, '2021-02-10 17:31:07', '2021-02-10 17:31:07', ' ', '', '', 'publish', 'closed', 'closed', '', '19', '', '', '2021-02-10 17:32:04', '2021-02-10 17:32:04', '', 0, 'http://bizen.local/?p=19', 3, 'nav_menu_item', '', 0),
(20, 1, '2021-02-10 17:31:07', '2021-02-10 17:31:07', '', '移住を考える', '', 'publish', 'closed', 'closed', '', '%e7%a7%bb%e4%bd%8f%e3%82%92%e8%80%83%e3%81%88%e3%82%8b-2', '', '', '2021-02-10 17:32:04', '2021-02-10 17:32:04', '', 0, 'http://bizen.local/?p=20', 4, 'nav_menu_item', '', 0),
(21, 1, '2021-02-10 17:31:07', '2021-02-10 17:31:07', '', '支援・補助', '', 'publish', 'closed', 'closed', '', '%e6%94%af%e6%8f%b4%e3%83%bb%e8%a3%9c%e5%8a%a9-2', '', '', '2021-02-10 17:32:04', '2021-02-10 17:32:04', '', 0, 'http://bizen.local/?p=21', 5, 'nav_menu_item', '', 0),
(22, 1, '2021-02-10 17:31:08', '2021-02-10 17:31:08', '', 'お問い合わせ', '', 'publish', 'closed', 'closed', '', '%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b-2', '', '', '2021-02-10 17:32:05', '2021-02-10 17:32:05', '', 0, 'http://bizen.local/?p=22', 6, 'nav_menu_item', '', 0),
(23, 1, '2021-02-10 17:40:16', '2021-02-10 17:40:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"site-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Site Settings', 'site-settings', 'publish', 'closed', 'closed', '', 'group_602419c7a04dd', '', '', '2021-02-10 17:47:21', '2021-02-10 17:47:21', '', 0, 'http://bizen.local/?post_type=acf-field-group&#038;p=23', 0, 'acf-field-group', '', 0),
(24, 1, '2021-02-10 17:40:16', '2021-02-10 17:40:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Copyright', 'copyright', 'publish', 'closed', 'closed', '', 'field_602419de10681', '', '', '2021-02-10 17:40:16', '2021-02-10 17:40:16', '', 23, 'http://bizen.local/?post_type=acf-field&p=24', 0, 'acf-field', '', 0),
(25, 1, '2021-02-10 17:40:16', '2021-02-10 17:40:16', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:3:\"pdf\";}', 'PDF Download', 'pdf_download', 'publish', 'closed', 'closed', '', 'field_602419eb10682', '', '', '2021-02-10 17:40:16', '2021-02-10 17:40:16', '', 23, 'http://bizen.local/?post_type=acf-field&p=25', 1, 'acf-field', '', 0),
(26, 1, '2021-02-10 17:40:16', '2021-02-10 17:40:16', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Contact information', 'contact_information', 'publish', 'closed', 'closed', '', 'field_60241a1410683', '', '', '2021-02-10 17:40:16', '2021-02-10 17:40:16', '', 23, 'http://bizen.local/?post_type=acf-field&p=26', 2, 'acf-field', '', 0),
(27, 1, '2021-02-10 17:40:17', '2021-02-10 17:40:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_60241a2510684', '', '', '2021-02-10 17:40:17', '2021-02-10 17:40:17', '', 26, 'http://bizen.local/?post_type=acf-field&p=27', 0, 'acf-field', '', 0),
(28, 1, '2021-02-10 17:40:17', '2021-02-10 17:40:17', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_60241a6610685', '', '', '2021-02-10 17:40:17', '2021-02-10 17:40:17', '', 26, 'http://bizen.local/?post_type=acf-field&p=28', 1, 'acf-field', '', 0),
(29, 1, '2021-02-10 17:41:27', '2021-02-10 17:41:27', '', 'bizen_specification', '', 'inherit', '', 'closed', '', 'bizen_specification', '', '', '2021-02-10 17:41:27', '2021-02-10 17:41:27', '', 0, 'http://bizen.local/site/package/uploads/bizen_specification.pdf', 0, 'attachment', 'application/pdf', 0),
(30, 1, '2021-02-10 17:47:20', '2021-02-10 17:47:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Reference Link', 'reference_link', 'publish', 'closed', 'closed', '', 'field_60241c0e33419', '', '', '2021-02-10 17:47:20', '2021-02-10 17:47:20', '', 23, 'http://bizen.local/?post_type=acf-field&p=30', 3, 'acf-field', '', 0),
(31, 1, '2021-02-10 17:49:52', '2021-02-10 17:49:52', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"front.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Home Template', 'home-template', 'publish', 'closed', 'closed', '', 'group_60241cb4b4f24', '', '', '2021-02-13 18:29:56', '2021-02-13 18:29:56', '', 0, 'http://bizen.local/?post_type=acf-field-group&#038;p=31', 0, 'acf-field-group', '', 0),
(32, 1, '2021-02-10 17:51:11', '2021-02-10 17:51:11', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Home Main Slider', 'home_slider', 'publish', 'closed', 'closed', '', 'field_60241cc984d6d', '', '', '2021-02-10 17:51:11', '2021-02-10 17:51:11', '', 31, 'http://bizen.local/?post_type=acf-field&p=32', 0, 'acf-field', '', 0),
(33, 1, '2021-02-10 17:51:12', '2021-02-10 17:51:12', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Poster Image', 'poster_image', 'publish', 'closed', 'closed', '', 'field_60241cfb84d6e', '', '', '2021-02-10 18:04:45', '2021-02-10 18:04:45', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=33', 1, 'acf-field', '', 0),
(34, 1, '2021-02-10 17:52:21', '2021-02-10 17:52:21', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Add Slide\";}', 'Main Slider', 'main_slider', 'publish', 'closed', 'closed', '', 'field_60241d14e1e17', '', '', '2021-02-10 18:04:46', '2021-02-10 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=34', 2, 'acf-field', '', 0),
(35, 1, '2021-02-10 17:52:22', '2021-02-10 17:52:22', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_60241d2de1e18', '', '', '2021-02-10 17:52:22', '2021-02-10 17:52:22', '', 34, 'http://bizen.local/?post_type=acf-field&p=35', 0, 'acf-field', '', 0),
(36, 1, '2021-02-10 17:52:22', '2021-02-10 17:52:22', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Slider Text', 'slider_text', 'publish', 'closed', 'closed', '', 'field_60241d3ee1e19', '', '', '2021-02-10 17:57:18', '2021-02-10 17:57:18', '', 34, 'http://bizen.local/?post_type=acf-field&#038;p=36', 2, 'acf-field', '', 0),
(37, 1, '2021-02-10 17:53:03', '2021-02-10 17:53:03', '', 'banner_asset', '', 'inherit', '', 'closed', '', 'banner_asset', '', '', '2021-02-10 17:53:03', '2021-02-10 17:53:03', '', 7, 'http://bizen.local/site/package/uploads/banner_asset.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2021-02-10 17:53:23', '2021-02-10 17:53:23', '', 'main', '', 'inherit', '', 'closed', '', 'main', '', '', '2021-02-10 17:53:23', '2021-02-10 17:53:23', '', 7, 'http://bizen.local/site/package/uploads/main.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2021-02-10 17:53:53', '2021-02-10 17:53:53', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-10 17:53:53', '2021-02-10 17:53:53', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2021-02-10 17:57:17', '2021-02-10 17:57:17', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Mobile', 'image_mobile', 'publish', 'closed', 'closed', '', 'field_60241e6b1320e', '', '', '2021-02-10 17:57:17', '2021-02-10 17:57:17', '', 34, 'http://bizen.local/?post_type=acf-field&p=40', 1, 'acf-field', '', 0),
(41, 1, '2021-02-10 17:58:14', '2021-02-10 17:58:14', '', 'responsive_main', '', 'inherit', '', 'closed', '', 'responsive_main', '', '', '2021-02-10 17:58:14', '2021-02-10 17:58:14', '', 7, 'http://bizen.local/site/package/uploads/responsive_main.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2021-02-10 17:58:28', '2021-02-10 17:58:28', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-10 17:58:28', '2021-02-10 17:58:28', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2021-02-10 18:04:46', '2021-02-10 18:04:46', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Pickup', '_copy', 'publish', 'closed', 'closed', '', 'field_60241f732a360', '', '', '2021-02-10 18:04:46', '2021-02-10 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&p=43', 3, 'acf-field', '', 0),
(44, 1, '2021-02-10 18:04:46', '2021-02-10 18:04:46', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:15;s:9:\"new_lines\";s:2:\"br\";}', 'Pickup Text', 'pickup_text', 'publish', 'closed', 'closed', '', 'field_60241f7d2a361', '', '', '2021-02-10 18:06:13', '2021-02-10 18:06:13', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=44', 4, 'acf-field', '', 0),
(45, 1, '2021-02-10 18:04:46', '2021-02-10 18:04:46', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:4:\"37.5\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Video 1', 'video_1', 'publish', 'closed', 'closed', '', 'field_60241fd22a362', '', '', '2021-02-10 18:04:46', '2021-02-10 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&p=45', 5, 'acf-field', '', 0),
(46, 1, '2021-02-10 18:04:47', '2021-02-10 18:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_60241fdc2a363', '', '', '2021-02-10 18:04:47', '2021-02-10 18:04:47', '', 45, 'http://bizen.local/?post_type=acf-field&p=46', 0, 'acf-field', '', 0),
(47, 1, '2021-02-10 18:04:47', '2021-02-10 18:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Embed URL', 'embed_url', 'publish', 'closed', 'closed', '', 'field_60241fe12a364', '', '', '2021-02-10 18:04:47', '2021-02-10 18:04:47', '', 45, 'http://bizen.local/?post_type=acf-field&p=47', 1, 'acf-field', '', 0),
(48, 1, '2021-02-10 18:04:48', '2021-02-10 18:04:48', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_6024200c2a365', '', '', '2021-02-10 18:04:48', '2021-02-10 18:04:48', '', 45, 'http://bizen.local/?post_type=acf-field&p=48', 2, 'acf-field', '', 0),
(49, 1, '2021-02-10 18:04:48', '2021-02-10 18:04:48', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:4:\"37.5\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Video 2', 'video_2', 'publish', 'closed', 'closed', '', 'field_602420392a366', '', '', '2021-02-10 18:04:48', '2021-02-10 18:04:48', '', 31, 'http://bizen.local/?post_type=acf-field&p=49', 6, 'acf-field', '', 0),
(50, 1, '2021-02-10 18:04:48', '2021-02-10 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_602420392a367', '', '', '2021-02-10 18:04:48', '2021-02-10 18:04:48', '', 49, 'http://bizen.local/?post_type=acf-field&p=50', 0, 'acf-field', '', 0),
(51, 1, '2021-02-10 18:04:48', '2021-02-10 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Embed URL', 'embed_url', 'publish', 'closed', 'closed', '', 'field_602420392a368', '', '', '2021-02-10 18:04:48', '2021-02-10 18:04:48', '', 49, 'http://bizen.local/?post_type=acf-field&p=51', 1, 'acf-field', '', 0),
(52, 1, '2021-02-10 18:04:49', '2021-02-10 18:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_602420392a369', '', '', '2021-02-10 18:04:49', '2021-02-10 18:04:49', '', 49, 'http://bizen.local/?post_type=acf-field&p=52', 2, 'acf-field', '', 0),
(53, 1, '2021-02-10 18:05:56', '2021-02-10 18:05:56', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-10 18:05:56', '2021-02-10 18:05:56', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2021-02-12 17:30:50', '2021-02-12 17:30:50', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ', '', 'publish', 'closed', 'closed', '', 'sample-1', '', '', '2021-02-12 17:43:18', '2021-02-12 17:43:18', '', 0, 'http://bizen.local/?p=54', 0, 'post', '', 0),
(55, 1, '2021-02-12 17:30:35', '2021-02-12 17:30:35', '', 'sample_lg', '', 'inherit', '', 'closed', '', 'sample_lg', '', '', '2021-02-12 17:30:35', '2021-02-12 17:30:35', '', 54, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2021-02-12 17:30:50', '2021-02-12 17:30:50', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2021-02-12 17:30:50', '2021-02-12 17:30:50', '', 54, 'http://bizen.local/54-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2021-02-12 17:31:14', '2021-02-12 17:31:14', '', 'sample', '', 'inherit', '', 'closed', '', 'sample', '', '', '2021-02-12 17:31:14', '2021-02-12 17:31:14', '', 54, 'http://bizen.local/site/package/uploads/sample.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2020-03-05 17:34:05', '2020-03-05 17:34:05', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 2', '', 'publish', 'closed', 'closed', '', 'sample-2', '', '', '2021-02-12 17:43:35', '2021-02-12 17:43:35', '', 0, 'http://bizen.local/?p=58', 0, 'post', '', 0),
(59, 1, '2021-02-12 17:34:05', '2021-02-12 17:34:05', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 2', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2021-02-12 17:34:05', '2021-02-12 17:34:05', '', 58, 'http://bizen.local/58-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2021-02-12 17:34:42', '2021-02-12 17:34:42', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 3', '', 'publish', 'closed', 'closed', '', 'sample-3', '', '', '2021-02-12 17:44:00', '2021-02-12 17:44:00', '', 0, 'http://bizen.local/?p=60', 0, 'post', '', 0),
(61, 1, '2021-02-12 17:34:42', '2021-02-12 17:34:42', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 3', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2021-02-12 17:34:42', '2021-02-12 17:34:42', '', 60, 'http://bizen.local/60-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2021-01-06 17:35:00', '2021-01-06 17:35:00', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 4', '', 'publish', 'closed', 'closed', '', 'sample-4', '', '', '2021-02-12 17:44:04', '2021-02-12 17:44:04', '', 0, 'http://bizen.local/?p=62', 0, 'post', '', 0),
(63, 1, '2021-02-12 17:35:27', '2021-02-12 17:35:27', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 4', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2021-02-12 17:35:27', '2021-02-12 17:35:27', '', 62, 'http://bizen.local/62-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2021-02-12 17:36:57', '2021-02-12 17:36:57', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 5', '', 'publish', 'closed', 'closed', '', 'sample-5', '', '', '2021-02-12 17:44:12', '2021-02-12 17:44:12', '', 0, 'http://bizen.local/?p=64', 0, 'post', '', 0),
(65, 1, '2021-02-12 17:36:57', '2021-02-12 17:36:57', '<!-- wp:image {\"id\":55,\"sizeSlug\":\"large\",\"linkDestination\":\"none\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://bizen.local/site/package/uploads/sample_lg.jpg\" alt=\"\" class=\"wp-image-55\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>就製す術整ナソメ伝格メオホ滋西ヘミスオ遺9的イら皆協へべドイ独法読スソフ給法ユワケ虚家そ提載ど京割9趣メ文照せぜ亡2権やぼぐッ省港訃ラ。待車シコヌ卒発揮ただ千進距ケマム理航せかぱラ政聞落ね友更おめ正会略キスコエ投空そトゆむ流紀オヤア聞日をなべめ員白亜才た。必ム行済テヒ高出あ感特ざ良3養ニセ功電71要よリやレ朝明リメヱヘ園亡のッそス吸急ツ録寄静奏どに。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>際うがろ得94見たーげら友健康づ影整も個代ナ覚失だどを都税いづお阪九ルネト碁費3回払リ集岡イぶむく平村書王りめ。苫チソ必極むイかス気名ニネヱナ移毫ひな分扱み景議氏言クハ止団モホ関囲たちばで郎肩びト変大ぼ性帯のさ渉通ヒケイフ行85話岡キロネフ押権公催退農ーなしち。政オ利面エ渡整ナ初禁ラ田政さがトぎ米岡ヒスリヲ要開ムヨヱ秋久必だスろる考36要ふーラせ判片72出得減ゃゅ。</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>懲じのどド茶彩ヨモエ不一ゃイり総校産フへな工産は戸生組べばれざ真天ネケ景以著ぶぴ行紹いお善竹在升忍ッぐぼて。清モ近検ヘモウラ武情エルミ年極クテナ助覧クテメ身辺や取清トニ席上セヌヲネ過迫せろった士63氏介ろルレ給荒上へっせさ意9座エ権単十流わりべけ。担おレは裁9直オホコソ被徹イぞ政望実ハレ聞作月的は無写ンほどち越誕づ情地ヤケ法7著クむ飼損亮ぞ。</p>\n<!-- /wp:paragraph -->', 'お知らせお知らせお知らせお知らせお知らせお知らせ 5', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2021-02-12 17:36:57', '2021-02-12 17:36:57', '', 64, 'http://bizen.local/64-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2021-02-12 18:00:27', '2021-02-12 18:00:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Introduction', 'introduction', 'publish', 'closed', 'closed', '', 'field_6026c164c456a', '', '', '2021-02-12 18:00:27', '2021-02-12 18:00:27', '', 31, 'http://bizen.local/?post_type=acf-field&p=67', 7, 'acf-field', '', 0),
(68, 1, '2021-02-12 18:00:27', '2021-02-12 18:00:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Small title', 'small_title', 'publish', 'closed', 'closed', '', 'field_6026c19cc456b', '', '', '2021-02-13 18:04:45', '2021-02-13 18:04:45', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=68', 8, 'acf-field', '', 0),
(69, 1, '2021-02-12 18:00:28', '2021-02-12 18:00:28', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Title With Icon', 'title_with_icon', 'publish', 'closed', 'closed', '', 'field_6026c1a5c456c', '', '', '2021-02-13 18:04:45', '2021-02-13 18:04:45', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=69', 9, 'acf-field', '', 0),
(70, 1, '2021-02-12 18:00:28', '2021-02-12 18:00:28', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:9:\"33.333333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Left Text', 'left_text', 'publish', 'closed', 'closed', '', 'field_6026c1c2c456d', '', '', '2021-02-12 18:00:28', '2021-02-12 18:00:28', '', 69, 'http://bizen.local/?post_type=acf-field&p=70', 0, 'acf-field', '', 0),
(71, 1, '2021-02-12 18:00:28', '2021-02-12 18:00:28', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:9:\"33.333333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_6026c1e1c456f', '', '', '2021-02-12 18:00:28', '2021-02-12 18:00:28', '', 69, 'http://bizen.local/?post_type=acf-field&p=71', 1, 'acf-field', '', 0),
(72, 1, '2021-02-12 18:00:29', '2021-02-12 18:00:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:9:\"33.333333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Right Text', 'right_text', 'publish', 'closed', 'closed', '', 'field_6026c1cac456e', '', '', '2021-02-12 18:00:29', '2021-02-12 18:00:29', '', 69, 'http://bizen.local/?post_type=acf-field&p=72', 2, 'acf-field', '', 0),
(73, 1, '2021-02-12 18:00:29', '2021-02-12 18:00:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section Title', 'section_title', 'publish', 'closed', 'closed', '', 'field_6026c212c4571', '', '', '2021-02-13 18:04:45', '2021-02-13 18:04:45', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=73', 10, 'acf-field', '', 0),
(74, 1, '2021-02-12 18:00:29', '2021-02-12 18:00:29', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Map', 'map', 'publish', 'closed', 'closed', '', 'field_6026c220c4572', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=74', 13, 'acf-field', '', 0),
(75, 1, '2021-02-12 18:00:29', '2021-02-12 18:00:29', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:15;s:9:\"new_lines\";s:7:\"wpautop\";}', 'Map Infomation', 'map_infomation', 'publish', 'closed', 'closed', '', 'field_6026c234c4573', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=75', 14, 'acf-field', '', 0),
(76, 1, '2021-02-12 18:04:17', '2021-02-12 18:04:17', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Map Info Box 1', 'map_info_box_1', 'publish', 'closed', 'closed', '', 'field_6026c2bcc82ea', '', '', '2021-02-13 18:04:45', '2021-02-13 18:04:45', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=76', 11, 'acf-field', '', 0),
(77, 1, '2021-02-12 18:04:17', '2021-02-12 18:04:17', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Map Info Box 2', 'map_info_box_2', 'publish', 'closed', 'closed', '', 'field_6026c2fbc82eb', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=77', 12, 'acf-field', '', 0),
(78, 1, '2021-02-12 18:05:18', '2021-02-12 18:05:18', '', 'hand', '', 'inherit', '', 'closed', '', 'hand', '', '', '2021-02-12 18:05:18', '2021-02-12 18:05:18', '', 7, 'http://bizen.local/site/package/uploads/hand.svg', 0, 'attachment', 'image/svg', 0),
(79, 1, '2021-02-12 18:05:37', '2021-02-12 18:05:37', '', 'map', '', 'inherit', '', 'closed', '', 'map', '', '', '2021-02-12 18:05:37', '2021-02-12 18:05:37', '', 7, 'http://bizen.local/site/package/uploads/map.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2021-02-12 18:07:27', '2021-02-12 18:07:27', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:07:27', '2021-02-12 18:07:27', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2021-02-12 18:08:45', '2021-02-12 18:08:45', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:08:45', '2021-02-12 18:08:45', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2021-02-12 18:15:55', '2021-02-12 18:15:55', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Information Boxes Title', 'information_boxes_title', 'publish', 'closed', 'closed', '', 'field_6026c5cb5e01d', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=82', 15, 'acf-field', '', 0),
(83, 1, '2021-02-12 18:15:55', '2021-02-12 18:15:55', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:7:\"Add Box\";}', 'Information Boxes', 'information_boxes', 'publish', 'closed', 'closed', '', 'field_6026c4af5e017', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=83', 16, 'acf-field', '', 0),
(84, 1, '2021-02-12 18:15:55', '2021-02-12 18:15:55', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Info Text', 'info_text', 'publish', 'closed', 'closed', '', 'field_6026c4f15e018', '', '', '2021-02-12 18:15:55', '2021-02-12 18:15:55', '', 83, 'http://bizen.local/?post_type=acf-field&p=84', 0, 'acf-field', '', 0);
INSERT INTO `bzn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(85, 1, '2021-02-12 18:15:56', '2021-02-12 18:15:56', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:0;s:2:\"ui\";i:1;s:10:\"ui_on_text\";s:4:\"Blue\";s:11:\"ui_off_text\";s:5:\"White\";}', 'Box Background', 'box_background', 'publish', 'closed', 'closed', '', 'field_6026c5035e019', '', '', '2021-02-12 18:17:08', '2021-02-12 18:17:08', '', 83, 'http://bizen.local/?post_type=acf-field&#038;p=85', 1, 'acf-field', '', 0),
(86, 1, '2021-02-12 18:15:56', '2021-02-12 18:15:56', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_6026c5445e01b', '', '', '2021-02-12 18:15:56', '2021-02-12 18:15:56', '', 83, 'http://bizen.local/?post_type=acf-field&p=86', 2, 'acf-field', '', 0),
(87, 1, '2021-02-12 18:15:56', '2021-02-12 18:15:56', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Icon Class', 'icon_class', 'publish', 'closed', 'closed', '', 'field_6026c5a75e01c', '', '', '2021-02-12 18:15:56', '2021-02-12 18:15:56', '', 83, 'http://bizen.local/?post_type=acf-field&p=87', 3, 'acf-field', '', 0),
(88, 1, '2021-02-12 18:18:01', '2021-02-12 18:18:01', '', 'baby', '', 'inherit', '', 'closed', '', 'baby', '', '', '2021-02-12 18:18:01', '2021-02-12 18:18:01', '', 7, 'http://bizen.local/site/package/uploads/baby.svg', 0, 'attachment', 'image/svg', 0),
(89, 1, '2021-02-12 18:18:02', '2021-02-12 18:18:02', '', 'house', '', 'inherit', '', 'closed', '', 'house', '', '', '2021-02-12 18:18:02', '2021-02-12 18:18:02', '', 7, 'http://bizen.local/site/package/uploads/house.svg', 0, 'attachment', 'image/svg', 0),
(90, 1, '2021-02-12 18:18:04', '2021-02-12 18:18:04', '', 'man', '', 'inherit', '', 'closed', '', 'man', '', '', '2021-02-12 18:18:04', '2021-02-12 18:18:04', '', 7, 'http://bizen.local/site/package/uploads/man.svg', 0, 'attachment', 'image/svg', 0),
(91, 1, '2021-02-12 18:18:05', '2021-02-12 18:18:05', '', 'text', '', 'inherit', '', 'closed', '', 'text', '', '', '2021-02-12 18:18:05', '2021-02-12 18:18:05', '', 7, 'http://bizen.local/site/package/uploads/text.svg', 0, 'attachment', 'image/svg', 0),
(92, 1, '2021-02-12 18:19:59', '2021-02-12 18:19:59', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:19:59', '2021-02-12 18:19:59', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2021-02-12 18:23:10', '2021-02-12 18:23:10', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:23:10', '2021-02-12 18:23:10', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2021-02-12 18:31:13', '2021-02-12 18:31:13', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:31:13', '2021-02-12 18:31:13', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2021-02-12 18:35:44', '2021-02-12 18:35:44', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-12 18:35:44', '2021-02-12 18:35:44', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2021-02-13 17:45:47', '2021-02-13 17:45:47', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '備前プレーパーク 森の冒険ひみつ基地', 'publish', 'closed', 'closed', '', '%e6%b8%85%e5%ae%b6-%e5%bd%a9%e8%8f%9c%e3%81%95%e3%82%93', '', '', '2021-02-13 17:53:33', '2021-02-13 17:53:33', '', 0, 'http://bizen.local/?p=96', 0, 'post', '', 0),
(97, 1, '2021-02-13 17:31:15', '2021-02-13 17:31:15', '', 'interview1', '', 'inherit', '', 'closed', '', 'interview1', '', '', '2021-02-13 17:31:15', '2021-02-13 17:31:15', '', 96, 'http://bizen.local/site/package/uploads/interview1.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2021-02-13 17:31:16', '2021-02-13 17:31:16', '', 'interview2', '', 'inherit', '', 'closed', '', 'interview2', '', '', '2021-02-13 17:31:16', '2021-02-13 17:31:16', '', 96, 'http://bizen.local/site/package/uploads/interview2.jpg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2021-02-13 17:31:18', '2021-02-13 17:31:18', '', 'interview3', '', 'inherit', '', 'closed', '', 'interview3', '', '', '2021-02-13 17:31:18', '2021-02-13 17:31:18', '', 96, 'http://bizen.local/site/package/uploads/interview3.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2021-02-13 17:31:20', '2021-02-13 17:31:20', '', 'interview4', '', 'inherit', '', 'closed', '', 'interview4', '', '', '2021-02-13 17:31:20', '2021-02-13 17:31:20', '', 96, 'http://bizen.local/site/package/uploads/interview4.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2021-02-13 17:33:47', '2021-02-13 17:33:47', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2021-02-13 17:33:47', '2021-02-13 17:33:47', '', 96, 'http://bizen.local/96-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2021-02-13 17:34:11', '2021-02-13 17:34:11', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '備前プレーパーク 森の冒険ひみつ基地', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2021-02-13 17:34:11', '2021-02-13 17:34:11', '', 96, 'http://bizen.local/96-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2021-02-13 17:35:23', '2021-02-13 17:35:23', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_category\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:18:\"category:interview\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Interview', 'interview', 'publish', 'closed', 'closed', '', 'group_60280da7315eb', '', '', '2021-02-13 17:52:07', '2021-02-13 17:52:07', '', 0, 'http://bizen.local/?post_type=acf-field-group&#038;p=103', 0, 'acf-field-group', '', 0),
(104, 1, '2021-02-13 17:35:23', '2021-02-13 17:35:23', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:8:\"33.33333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Area', 'area', 'publish', 'closed', 'closed', '', 'field_60280db729309', '', '', '2021-02-13 17:38:14', '2021-02-13 17:38:14', '', 103, 'http://bizen.local/?post_type=acf-field&#038;p=104', 0, 'acf-field', '', 0),
(105, 1, '2021-02-13 17:35:24', '2021-02-13 17:35:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:8:\"33.33333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'More information', 'more_information', 'publish', 'closed', 'closed', '', 'field_60280dc32930a', '', '', '2021-02-13 17:38:21', '2021-02-13 17:38:21', '', 103, 'http://bizen.local/?post_type=acf-field&#038;p=105', 2, 'acf-field', '', 0),
(106, 1, '2021-02-13 17:36:23', '2021-02-13 17:36:23', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '備前プレーパーク 森の冒険ひみつ基地', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2021-02-13 17:36:23', '2021-02-13 17:36:23', '', 96, 'http://bizen.local/96-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2021-02-13 17:38:14', '2021-02-13 17:38:14', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:8:\"33.33333\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:3:{s:4:\"blue\";s:4:\"Blue\";s:6:\"orange\";s:6:\"Orange\";s:5:\"green\";s:5:\"Green\";}s:13:\"default_value\";b:0;s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'Area color', 'area_color', 'publish', 'closed', 'closed', '', 'field_60280e35226f3', '', '', '2021-02-13 17:52:06', '2021-02-13 17:52:06', '', 103, 'http://bizen.local/?post_type=acf-field&#038;p=107', 1, 'acf-field', '', 0),
(108, 1, '2021-02-13 17:39:11', '2021-02-13 17:39:11', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '備前プレーパーク 森の冒険ひみつ基地', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2021-02-13 17:39:11', '2021-02-13 17:39:11', '', 96, 'http://bizen.local/96-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2021-02-13 17:40:53', '2021-02-13 17:40:53', '<!-- wp:heading {\"level\":4} -->\n<h4>人と人の繋がりをつくる農家レストラン</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>自然栽培という農薬や肥料を使わない方法で野菜やお米、豆などを育てています。そこで出来た野菜をたくさんの方に味わってもらいたいなという想いで、農家レストランとしてオープンしました。店名の絲（いと）は、この店を通じて人と人が繋がったり、お店に来てくれた人が畑に興味を持ったり、糸で繋がるような感じになればいいなと思って、この名前にしました。田舎はすごく人に干渉されるイメージがあると思うんですけど、意外にそんなことはなくて、良い距離感でみなさん私たちのことを気にかけてくださって、ひとりじゃないんだなっていうのを感じることが出来ました。</p>\n<!-- /wp:paragraph -->', '池田 雄二さん・由衣さん', '農業・農家レストラン「絲（いと）」経営', 'publish', 'closed', 'closed', '', '%e6%b1%a0%e7%94%b0-%e9%9b%84%e4%ba%8c%e3%81%95%e3%82%93%e3%83%bb%e7%94%b1%e8%a1%a3%e3%81%95%e3%82%93', '', '', '2021-02-13 17:54:08', '2021-02-13 17:54:08', '', 0, 'http://bizen.local/?p=109', 0, 'post', '', 0),
(110, 1, '2021-02-13 17:40:53', '2021-02-13 17:40:53', '<!-- wp:heading {\"level\":4} -->\n<h4>人と人の繋がりをつくる農家レストラン</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>自然栽培という農薬や肥料を使わない方法で野菜やお米、豆などを育てています。そこで出来た野菜をたくさんの方に味わってもらいたいなという想いで、農家レストランとしてオープンしました。店名の絲（いと）は、この店を通じて人と人が繋がったり、お店に来てくれた人が畑に興味を持ったり、糸で繋がるような感じになればいいなと思って、この名前にしました。田舎はすごく人に干渉されるイメージがあると思うんですけど、意外にそんなことはなくて、良い距離感でみなさん私たちのことを気にかけてくださって、ひとりじゃないんだなっていうのを感じることが出来ました。</p>\n<!-- /wp:paragraph -->', '池田 雄二さん・由衣さん', '農業・農家レストラン「絲（いと）」経営', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2021-02-13 17:40:53', '2021-02-13 17:40:53', '', 109, 'http://bizen.local/109-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2021-02-13 17:40:56', '2021-02-13 17:40:56', '<!-- wp:heading {\"level\":4} -->\n<h4>人と人の繋がりをつくる農家レストラン</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>自然栽培という農薬や肥料を使わない方法で野菜やお米、豆などを育てています。そこで出来た野菜をたくさんの方に味わってもらいたいなという想いで、農家レストランとしてオープンしました。店名の絲（いと）は、この店を通じて人と人が繋がったり、お店に来てくれた人が畑に興味を持ったり、糸で繋がるような感じになればいいなと思って、この名前にしました。田舎はすごく人に干渉されるイメージがあると思うんですけど、意外にそんなことはなくて、良い距離感でみなさん私たちのことを気にかけてくださって、ひとりじゃないんだなっていうのを感じることが出来ました。</p>\n<!-- /wp:paragraph -->', '池田 雄二さん・由衣さん', '農業・農家レストラン「絲（いと）」経営', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2021-02-13 17:40:56', '2021-02-13 17:40:56', '', 109, 'http://bizen.local/109-revision-v1/', 0, 'revision', '', 0),
(113, 1, '2021-02-13 17:42:54', '2021-02-13 17:42:54', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '妹尾 悠平さん', 'Koti brewery（クラフトビール製造）', 'publish', 'closed', 'closed', '', '%e5%a6%b9%e5%b0%be-%e6%82%a0%e5%b9%b3%e3%81%95%e3%82%93', '', '', '2021-02-13 17:43:26', '2021-02-13 17:43:26', '', 0, 'http://bizen.local/?p=113', 0, 'post', '', 0),
(114, 1, '2021-02-13 17:42:54', '2021-02-13 17:42:54', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '妹尾 悠平さん', 'Koti brewery（クラフトビール製造）', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2021-02-13 17:42:54', '2021-02-13 17:42:54', '', 113, 'http://bizen.local/113-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2021-02-13 17:42:58', '2021-02-13 17:42:58', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '妹尾 悠平さん', 'Koti brewery（クラフトビール製造）', 'inherit', 'closed', 'closed', '', '113-revision-v1', '', '', '2021-02-13 17:42:58', '2021-02-13 17:42:58', '', 113, 'http://bizen.local/113-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2021-02-13 17:44:41', '2021-02-13 17:44:41', '<!-- wp:heading {\"level\":4} -->\n<h4>やりたい仕事ができる場所をつくる</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>移住先に備前市を選んだ理由は、瀬戸内国際芸術祭に近くていいなと思ったのと、父が頭島でカフェを始めていたことがきっかけです。日生町にアトリエ「Little Ami」を開いて、チラシやポスター、ホームページに載せるイラストのデザインや、洋服のお直しをしています。都会ではなかなか家で起業をしても目立ちにくいのですが、備前市だと口コミでどんどん広まっていって、こんな場所でもわざわざお客さんが来てくれるので、気軽に仕事を始められました。</p>\n<!-- /wp:paragraph -->', '橋本 あみさん', 'イラストデザイン・縫製アトリエ', 'publish', 'closed', 'closed', '', '%e6%a9%8b%e6%9c%ac-%e3%81%82%e3%81%bf%e3%81%95%e3%82%93', '', '', '2021-02-13 17:44:44', '2021-02-13 17:44:44', '', 0, 'http://bizen.local/?p=116', 0, 'post', '', 0),
(117, 1, '2021-02-13 17:44:41', '2021-02-13 17:44:41', '<!-- wp:heading {\"level\":4} -->\n<h4>やりたい仕事ができる場所をつくる</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>移住先に備前市を選んだ理由は、瀬戸内国際芸術祭に近くていいなと思ったのと、父が頭島でカフェを始めていたことがきっかけです。日生町にアトリエ「Little Ami」を開いて、チラシやポスター、ホームページに載せるイラストのデザインや、洋服のお直しをしています。都会ではなかなか家で起業をしても目立ちにくいのですが、備前市だと口コミでどんどん広まっていって、こんな場所でもわざわざお客さんが来てくれるので、気軽に仕事を始められました。</p>\n<!-- /wp:paragraph -->', '橋本 あみさん', 'イラストデザイン・縫製アトリエ', 'inherit', 'closed', 'closed', '', '116-revision-v1', '', '', '2021-02-13 17:44:41', '2021-02-13 17:44:41', '', 116, 'http://bizen.local/116-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2021-02-13 17:44:44', '2021-02-13 17:44:44', '<!-- wp:heading {\"level\":4} -->\n<h4>やりたい仕事ができる場所をつくる</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>移住先に備前市を選んだ理由は、瀬戸内国際芸術祭に近くていいなと思ったのと、父が頭島でカフェを始めていたことがきっかけです。日生町にアトリエ「Little Ami」を開いて、チラシやポスター、ホームページに載せるイラストのデザインや、洋服のお直しをしています。都会ではなかなか家で起業をしても目立ちにくいのですが、備前市だと口コミでどんどん広まっていって、こんな場所でもわざわざお客さんが来てくれるので、気軽に仕事を始められました。</p>\n<!-- /wp:paragraph -->', '橋本 あみさん', 'イラストデザイン・縫製アトリエ', 'inherit', 'closed', 'closed', '', '116-revision-v1', '', '', '2021-02-13 17:44:44', '2021-02-13 17:44:44', '', 116, 'http://bizen.local/116-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2021-02-13 17:53:33', '2021-02-13 17:53:33', '<!-- wp:heading {\"level\":4} -->\n<h4>空気中の酵母を使ったクラフトビールづくり</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>備前市の吉永町というところでクラフトビールを作っています。普通のビールとは少し違っていて、備前市の空気中から酵母を取って、それを自然に発酵させて作っています。最初にビールを作る工場を借りた時に、とてもすぐには始められる状況ではなかったんですけど、ここの地域の方々が手伝ってくださって、掃除をしてくれたり、片付けてくれたりして、今のこの環境があるんです。そういうところで人の優しさとか、ここの地域の方の暖かさっていうのを感じることが出来てよかったですね。</p>\n<!-- /wp:paragraph -->', '清家 彩菜さん', '備前プレーパーク 森の冒険ひみつ基地', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2021-02-13 17:53:33', '2021-02-13 17:53:33', '', 96, 'http://bizen.local/96-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2021-02-13 17:54:08', '2021-02-13 17:54:08', '<!-- wp:heading {\"level\":4} -->\n<h4>人と人の繋がりをつくる農家レストラン</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>自然栽培という農薬や肥料を使わない方法で野菜やお米、豆などを育てています。そこで出来た野菜をたくさんの方に味わってもらいたいなという想いで、農家レストランとしてオープンしました。店名の絲（いと）は、この店を通じて人と人が繋がったり、お店に来てくれた人が畑に興味を持ったり、糸で繋がるような感じになればいいなと思って、この名前にしました。田舎はすごく人に干渉されるイメージがあると思うんですけど、意外にそんなことはなくて、良い距離感でみなさん私たちのことを気にかけてくださって、ひとりじゃないんだなっていうのを感じることが出来ました。</p>\n<!-- /wp:paragraph -->', '池田 雄二さん・由衣さん', '農業・農家レストラン「絲（いと）」経営', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2021-02-13 17:54:08', '2021-02-13 17:54:08', '', 109, 'http://bizen.local/109-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2021-02-13 17:57:00', '2021-02-13 17:57:00', '<!-- wp:heading {\"level\":4} -->\n<h4>人と人の繋がりをつくる農家レストラン</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>自然栽培という農薬や肥料を使わない方法で野菜やお米、豆などを育てています。そこで出来た野菜をたくさんの方に味わってもらいたいなという想いで、農家レストランとしてオープンしました。店名の絲（いと）は、この店を通じて人と人が繋がったり、お店に来てくれた人が畑に興味を持ったり、糸で繋がるような感じになればいいなと思って、この名前にしました。田舎はすごく人に干渉されるイメージがあると思うんですけど、意外にそんなことはなくて、良い距離感でみなさん私たちのことを気にかけてくださって、ひとりじゃないんだなっていうのを感じることが出来ました。</p>\n<!-- /wp:paragraph -->', '池田 雄二さん・由衣さん', '農業・農家レストラン「絲（いと）」経営', 'inherit', 'closed', 'closed', '', '109-autosave-v1', '', '', '2021-02-13 17:57:00', '2021-02-13 17:57:00', '', 109, 'http://bizen.local/109-autosave-v1/', 0, 'revision', '', 0),
(122, 1, '2021-02-13 18:00:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-02-13 18:00:46', '0000-00-00 00:00:00', '', 0, 'http://bizen.local/?post_type=acf-field-group&p=122', 0, 'acf-field-group', '', 0),
(123, 1, '2021-02-13 18:04:46', '2021-02-13 18:04:46', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'How To', '_copy', 'publish', 'closed', 'closed', '', 'field_602813d799f17', '', '', '2021-02-13 18:04:46', '2021-02-13 18:04:46', '', 31, 'http://bizen.local/?post_type=acf-field&p=123', 17, 'acf-field', '', 0),
(124, 1, '2021-02-13 18:04:47', '2021-02-13 18:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Small Title', 'small_title_1', 'publish', 'closed', 'closed', '', 'field_602813e399f18', '', '', '2021-02-13 18:08:25', '2021-02-13 18:08:25', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=124', 18, 'acf-field', '', 0),
(125, 1, '2021-02-13 18:04:47', '2021-02-13 18:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section Title En', 'section_title_en', 'publish', 'closed', 'closed', '', 'field_6028142599f1a', '', '', '2021-02-13 18:18:51', '2021-02-13 18:18:51', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=125', 20, 'acf-field', '', 0),
(126, 1, '2021-02-13 18:04:47', '2021-02-13 18:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section Title', 'section_title_1', 'publish', 'closed', 'closed', '', 'field_6028141099f19', '', '', '2021-02-13 18:18:51', '2021-02-13 18:18:51', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=126', 19, 'acf-field', '', 0),
(127, 1, '2021-02-13 18:04:48', '2021-02-13 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '[Step 1] Title', 'step_1_title', 'publish', 'closed', 'closed', '', 'field_6028143b99f1b', '', '', '2021-02-13 18:29:51', '2021-02-13 18:29:51', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=127', 22, 'acf-field', '', 0),
(128, 1, '2021-02-13 18:04:48', '2021-02-13 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '[Step 2] Title', 'step_2_title', 'publish', 'closed', 'closed', '', 'field_6028145499f1c', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=128', 23, 'acf-field', '', 0),
(129, 1, '2021-02-13 18:04:48', '2021-02-13 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '[Step 3] Title', 'step_3_title', 'publish', 'closed', 'closed', '', 'field_6028146b99f1d', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=129', 24, 'acf-field', '', 0),
(130, 1, '2021-02-13 18:04:48', '2021-02-13 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '[Step 4] Title', 'step_4_title', 'publish', 'closed', 'closed', '', 'field_6028147a99f1e', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=130', 25, 'acf-field', '', 0),
(131, 1, '2021-02-13 18:04:48', '2021-02-13 18:04:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '[Step 5] Title', 'step_5_title', 'publish', 'closed', 'closed', '', 'field_6028148299f1f', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=131', 26, 'acf-field', '', 0),
(132, 1, '2021-02-13 18:04:49', '2021-02-13 18:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description 1', 'description_1', 'publish', 'closed', 'closed', '', 'field_6028148c99f20', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=132', 27, 'acf-field', '', 0),
(133, 1, '2021-02-13 18:04:49', '2021-02-13 18:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description 2', 'description_2', 'publish', 'closed', 'closed', '', 'field_602814ae99f21', '', '', '2021-02-13 18:29:52', '2021-02-13 18:29:52', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=133', 28, 'acf-field', '', 0),
(134, 1, '2021-02-13 18:04:49', '2021-02-13 18:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description 3', 'description_3', 'publish', 'closed', 'closed', '', 'field_602814b099f22', '', '', '2021-02-13 18:29:53', '2021-02-13 18:29:53', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=134', 29, 'acf-field', '', 0),
(135, 1, '2021-02-13 18:04:49', '2021-02-13 18:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description 4', 'description_4', 'publish', 'closed', 'closed', '', 'field_602814b299f23', '', '', '2021-02-13 18:29:53', '2021-02-13 18:29:53', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=135', 30, 'acf-field', '', 0),
(136, 1, '2021-02-13 18:04:50', '2021-02-13 18:04:50', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Description 5', 'description_5', 'publish', 'closed', 'closed', '', 'field_602814b399f24', '', '', '2021-02-13 18:29:53', '2021-02-13 18:29:53', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=136', 31, 'acf-field', '', 0),
(137, 1, '2021-02-13 18:08:28', '2021-02-13 18:08:28', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Support Links', '_copy', 'publish', 'closed', 'closed', '', 'field_602814c9980be', '', '', '2021-02-13 18:29:54', '2021-02-13 18:29:54', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=137', 32, 'acf-field', '', 0),
(138, 1, '2021-02-13 18:08:28', '2021-02-13 18:08:28', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'House Links', 'house_links', 'publish', 'closed', 'closed', '', 'field_602814d8980bf', '', '', '2021-02-13 18:29:55', '2021-02-13 18:29:55', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=138', 34, 'acf-field', '', 0),
(139, 1, '2021-02-13 18:08:29', '2021-02-13 18:08:29', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:0:\"\";s:12:\"button_label\";s:8:\"Add Link\";}', 'Links', 'links', 'publish', 'closed', 'closed', '', 'field_60281550980c1', '', '', '2021-02-13 18:08:29', '2021-02-13 18:08:29', '', 138, 'http://bizen.local/?post_type=acf-field&p=139', 0, 'acf-field', '', 0),
(140, 1, '2021-02-13 18:08:29', '2021-02-13 18:08:29', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:0:\"\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_60281560980c2', '', '', '2021-02-13 18:08:29', '2021-02-13 18:08:29', '', 139, 'http://bizen.local/?post_type=acf-field&p=140', 0, 'acf-field', '', 0),
(141, 1, '2021-02-13 18:08:29', '2021-02-13 18:08:29', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Work Links', 'work_links', 'publish', 'closed', 'closed', '', 'field_60281574980c3', '', '', '2021-02-13 18:29:55', '2021-02-13 18:29:55', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=141', 35, 'acf-field', '', 0),
(142, 1, '2021-02-13 18:08:30', '2021-02-13 18:08:30', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:0:\"\";s:12:\"button_label\";s:8:\"Add Link\";}', 'Links', 'links', 'publish', 'closed', 'closed', '', 'field_60281574980c4', '', '', '2021-02-13 18:08:30', '2021-02-13 18:08:30', '', 141, 'http://bizen.local/?post_type=acf-field&p=142', 0, 'acf-field', '', 0),
(143, 1, '2021-02-13 18:08:30', '2021-02-13 18:08:30', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:0:\"\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_60281574980c5', '', '', '2021-02-13 18:08:30', '2021-02-13 18:08:30', '', 142, 'http://bizen.local/?post_type=acf-field&p=143', 0, 'acf-field', '', 0),
(144, 1, '2021-02-13 18:08:30', '2021-02-13 18:08:30', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Life Links', 'life_links', 'publish', 'closed', 'closed', '', 'field_6028157f980c6', '', '', '2021-02-13 18:29:55', '2021-02-13 18:29:55', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=144', 36, 'acf-field', '', 0),
(145, 1, '2021-02-13 18:08:30', '2021-02-13 18:08:30', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:0:\"\";s:12:\"button_label\";s:8:\"Add Link\";}', 'Links', 'links', 'publish', 'closed', 'closed', '', 'field_6028157f980c7', '', '', '2021-02-13 18:08:30', '2021-02-13 18:08:30', '', 144, 'http://bizen.local/?post_type=acf-field&p=145', 0, 'acf-field', '', 0),
(146, 1, '2021-02-13 18:08:30', '2021-02-13 18:08:30', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:0:\"\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_6028157f980c8', '', '', '2021-02-13 18:08:30', '2021-02-13 18:08:30', '', 145, 'http://bizen.local/?post_type=acf-field&p=146', 0, 'acf-field', '', 0),
(153, 1, '2021-02-13 18:12:41', '2021-02-13 18:12:41', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Childs Links', 'childs_links', 'publish', 'closed', 'closed', '', 'field_602816880579a', '', '', '2021-02-13 18:29:55', '2021-02-13 18:29:55', '', 31, 'http://bizen.local/?post_type=acf-field&#038;p=153', 37, 'acf-field', '', 0),
(154, 1, '2021-02-13 18:12:42', '2021-02-13 18:12:42', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:8:\"Add Link\";}', 'Links', 'links', 'publish', 'closed', 'closed', '', 'field_602816880579b', '', '', '2021-02-13 18:12:42', '2021-02-13 18:12:42', '', 153, 'http://bizen.local/?post_type=acf-field&p=154', 0, 'acf-field', '', 0),
(155, 1, '2021-02-13 18:12:42', '2021-02-13 18:12:42', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_602816880579c', '', '', '2021-02-13 18:12:42', '2021-02-13 18:12:42', '', 154, 'http://bizen.local/?post_type=acf-field&p=155', 0, 'acf-field', '', 0),
(156, 1, '2021-02-13 18:18:52', '2021-02-13 18:18:52', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section Title', 'section_title_2', 'publish', 'closed', 'closed', '', 'field_602818030c02d', '', '', '2021-02-13 18:18:52', '2021-02-13 18:18:52', '', 31, 'http://bizen.local/?post_type=acf-field&p=156', 21, 'acf-field', '', 0),
(157, 1, '2021-02-13 18:24:26', '2021-02-13 18:24:26', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-13 18:24:26', '2021-02-13 18:24:26', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0),
(158, 1, '2021-02-13 18:29:54', '2021-02-13 18:29:54', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section Title', 'section_title_3', 'publish', 'closed', 'closed', '', 'field_60281a96de1ab', '', '', '2021-02-13 18:29:54', '2021-02-13 18:29:54', '', 31, 'http://bizen.local/?post_type=acf-field&p=158', 33, 'acf-field', '', 0),
(159, 1, '2021-02-13 18:30:17', '2021-02-13 18:30:17', '', 'ホーム', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-02-13 18:30:17', '2021-02-13 18:30:17', '', 7, 'http://bizen.local/7-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_termmeta`
--

CREATE TABLE `bzn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bzn_terms`
--

CREATE TABLE `bzn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_terms`
--

INSERT INTO `bzn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(2, 'Information', 'information', 0),
(3, '先輩移住者のおはなし', 'interview', 0),
(4, 'Main Menu', 'main-menu', 0),
(5, 'Event', 'event', 0),
(6, 'News', 'news', 0),
(7, 'Main Menu Subpage', 'main-menu-subpage', 0),
(8, 'その他', 'other', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_term_relationships`
--

CREATE TABLE `bzn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_term_relationships`
--

INSERT INTO `bzn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(10, 4, 0),
(11, 4, 0),
(12, 4, 0),
(14, 4, 0),
(15, 4, 0),
(16, 4, 0),
(17, 7, 0),
(18, 7, 0),
(19, 7, 0),
(20, 7, 0),
(21, 7, 0),
(22, 7, 0),
(54, 2, 0),
(54, 5, 0),
(54, 6, 0),
(58, 2, 0),
(58, 5, 0),
(58, 8, 0),
(60, 2, 0),
(60, 5, 0),
(60, 6, 0),
(62, 2, 0),
(62, 5, 0),
(64, 2, 0),
(64, 8, 0),
(96, 3, 0),
(109, 3, 0),
(113, 3, 0),
(116, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_term_taxonomy`
--

CREATE TABLE `bzn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_term_taxonomy`
--

INSERT INTO `bzn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(2, 2, 'category', '', 0, 5),
(3, 3, 'category', '', 0, 4),
(4, 4, 'nav_menu', '', 0, 6),
(5, 5, 'category', '', 2, 4),
(6, 6, 'category', '', 2, 2),
(7, 7, 'nav_menu', '', 0, 6),
(8, 8, 'category', '', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_usermeta`
--

CREATE TABLE `bzn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_usermeta`
--

INSERT INTO `bzn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'bizen-admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'bzn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'bzn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"4ad251b47daba1174e630e5395f64cdd525b6bca7b1f49e46e65e4c074ae2dc1\";a:4:{s:10:\"expiration\";i:1613323697;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:5:\"login\";i:1613150897;}s:64:\"eecc843bd868cef85924c92be61e61a43354a00f1bae6bef00e707d0613825e9\";a:4:{s:10:\"expiration\";i:1613409887;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:133:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.396\";s:5:\"login\";i:1613237087;}}'),
(17, 1, 'bzn_dashboard_quick_press_last_post_id', '9'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'itsec_user_activity_last_seen', '1613319998'),
(21, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(22, 1, 'metaboxhidden_dashboard', 'a:1:{i:0;s:17:\"dashboard_primary\";}'),
(23, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:60:\"dashboard_site_health,dashboard_right_now,dashboard_activity\";s:4:\"side\";s:39:\"dashboard_quick_press,dashboard_primary\";s:7:\"column3\";s:24:\"wpseo-dashboard-overview\";s:7:\"column4\";s:0:\"\";}'),
(24, 1, 'itsec-settings-view', 'grid'),
(25, 1, 'itsec-password-strength', '4'),
(26, 1, '_itsec_password_requirements', 'a:1:{s:16:\"evaluation_times\";a:1:{s:8:\"strength\";i:1612976961;}}'),
(27, 1, '_itsec_has_logged_in', '1612976961'),
(29, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(30, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(31, 1, 'nav_menu_recently_edited', '7'),
(32, 1, 'bzn_user-settings', 'libraryContent=browse'),
(33, 1, 'bzn_user-settings-time', '1612978888'),
(34, 1, 'bzn_yoast_notifications', 'a:1:{i:0;a:2:{s:7:\"message\";O:61:\"Yoast\\WP\\SEO\\Presenters\\Admin\\Indexing_Notification_Presenter\":3:{s:18:\"\0*\0total_unindexed\";i:5;s:9:\"\0*\0reason\";s:21:\"category_base_changed\";s:20:\"\0*\0short_link_helper\";O:38:\"Yoast\\WP\\SEO\\Helpers\\Short_Link_Helper\":2:{s:17:\"\0*\0options_helper\";O:35:\"Yoast\\WP\\SEO\\Helpers\\Options_Helper\":0:{}s:17:\"\0*\0product_helper\";O:35:\"Yoast\\WP\\SEO\\Helpers\\Product_Helper\":0:{}}}s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:13:\"wpseo-reindex\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:11:\"bizen-admin\";s:9:\"user_pass\";s:34:\"$P$BuTE/SvAX.Md.LNWNTS/tETxdjulnl.\";s:13:\"user_nicename\";s:11:\"bizen-admin\";s:10:\"user_email\";s:26:\"dev.tranthanhduy@gmail.com\";s:8:\"user_url\";s:18:\"http://bizen.local\";s:15:\"user_registered\";s:19:\"2021-02-03 17:35:29\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:11:\"bizen-admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:16:\"bzn_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:64:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:10:\"copy_posts\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.8;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_users`
--

CREATE TABLE `bzn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_users`
--

INSERT INTO `bzn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'bizen-admin', '$P$BuTE/SvAX.Md.LNWNTS/tETxdjulnl.', 'bizen-admin', 'dev.tranthanhduy@gmail.com', 'http://bizen.local', '2021-02-03 17:35:29', '', 0, 'bizen-admin');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_wpmailsmtp_tasks_meta`
--

CREATE TABLE `bzn_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_wpmailsmtp_tasks_meta`
--

INSERT INTO `bzn_wpmailsmtp_tasks_meta` (`id`, `action`, `data`, `date`) VALUES
(1, 'wp_mail_smtp_admin_notifications_update', 'W10=', '2021-02-03 17:43:05');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_yoast_indexable`
--

CREATE TABLE `bzn_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_ci,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `post_parent` bigint(20) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_ci,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT '0',
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_ci,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT '0',
  `is_robots_noindex` tinyint(1) DEFAULT '0',
  `is_robots_nofollow` tinyint(1) DEFAULT '0',
  `is_robots_noarchive` tinyint(1) DEFAULT '0',
  `is_robots_noimageindex` tinyint(1) DEFAULT '0',
  `is_robots_nosnippet` tinyint(1) DEFAULT '0',
  `twitter_title` text COLLATE utf8mb4_unicode_ci,
  `twitter_image` longtext COLLATE utf8mb4_unicode_ci,
  `twitter_description` longtext COLLATE utf8mb4_unicode_ci,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_ci,
  `open_graph_title` text COLLATE utf8mb4_unicode_ci,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_ci,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_ci,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_ci,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_ci,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  `language` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT '0',
  `estimated_reading_time_minutes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_yoast_indexable`
--

INSERT INTO `bzn_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`, `estimated_reading_time_minutes`) VALUES
(1, 'http://bizen.local/author/bizen-admin/', '38:bad30168ff68609637ce0ac8c837448d', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://1.gravatar.com/avatar/acea10d696ae086b538d75b20a231569?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://1.gravatar.com/avatar/acea10d696ae086b538d75b20a231569?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2021-02-03 17:42:00', '2021-02-13 11:30:29', 1, NULL, NULL, NULL, NULL, 0, NULL),
(2, 'http://bizen.local/?page_id=3', '29:73a0cf55240faf787706c9bcbf305846', 3, 'post', 'page', 1, 0, NULL, NULL, 'Privacy Policy', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 17:42:00', '2021-02-10 10:09:26', 1, NULL, NULL, NULL, NULL, 0, NULL),
(3, 'http://bizen.local/', '19:80cea23943bde00bfb9bbd9927ccb761', 7, 'post', 'page', 1, 0, NULL, NULL, 'ホーム', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-03 17:42:00', '2021-02-13 11:30:29', 1, NULL, NULL, NULL, NULL, 0, NULL),
(6, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Page not found %%sep%% %%sitename%%', NULL, 'Error 404: Page not found', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 17:42:01', '2021-02-03 10:42:01', 1, NULL, NULL, NULL, NULL, 0, NULL),
(7, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 17:42:01', '2021-02-03 10:42:01', 1, NULL, NULL, NULL, NULL, 0, NULL),
(8, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 17:42:01', '2021-02-03 10:42:01', 1, NULL, NULL, NULL, NULL, 0, NULL),
(9, 'http://bizen.local/', '19:80cea23943bde00bfb9bbd9927ccb761', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', '備前市移住ガイド', 'Home', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2021-02-03 17:43:50', '2021-02-10 10:33:32', 1, NULL, NULL, NULL, NULL, 0, NULL),
(10, 'http://bizen.local/?p=9', '23:613e7d14d85bc3c06fa6402cc1c446d9', 9, 'post', 'post', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:09:26', '2021-02-10 10:09:26', 1, NULL, NULL, NULL, NULL, 0, NULL),
(11, 'http://bizen.local/information/', '31:fdd1982f3b514342811f351282575592', 2, 'term', 'category', NULL, NULL, NULL, NULL, 'Information', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:19:07', '2021-02-14 09:37:28', 1, NULL, NULL, NULL, NULL, 0, NULL),
(12, 'http://bizen.local/interview/', '29:d5914eea2c3d907473541ab89e56ec2d', 3, 'term', 'category', NULL, NULL, NULL, NULL, '先輩移住者のおはなし', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:19:19', '2021-02-14 09:37:28', 1, NULL, NULL, NULL, NULL, 0, NULL),
(13, 'http://bizen.local/10/', '22:3beebe354b09ea0691690ccc60b9af01', 10, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:20:20', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(14, 'http://bizen.local/11/', '22:eb251c36e539c4c95fc886b4b5c580f5', 11, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:20:25', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(15, 'http://bizen.local/%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b/', '74:affebb41056b515cf9cbabd4a3f5ea33', 12, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'お問い合わせ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:20:44', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(16, 'http://bizen.local/information/event/', '37:82b43979fa2a9f6285660f15cb769196', 5, 'term', 'category', NULL, NULL, NULL, NULL, 'Event', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:21:50', '2021-02-14 09:37:28', 1, NULL, NULL, NULL, NULL, 1, NULL),
(17, 'http://bizen.local/information/news/', '36:03d54d58b44249f6e472b6c58ae1a3b9', 6, 'term', 'category', NULL, NULL, NULL, NULL, 'News', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:21:57', '2021-02-14 09:37:28', 1, NULL, NULL, NULL, NULL, 1, NULL),
(18, 'http://bizen.local/%e5%82%99%e5%89%8d%e5%b8%82%e3%82%92%e7%9f%a5%e3%82%8d%e3%81%86/', '83:dcc0696c5c8244614c6e95b0135b8ace', 14, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '備前市を知ろう', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:23:14', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(19, 'http://bizen.local/%e7%a7%bb%e4%bd%8f%e3%82%92%e8%80%83%e3%81%88%e3%82%8b/', '74:59bb07472171e0e20aaa1c510679f1f6', 15, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '移住を考える', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:23:36', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(20, 'http://bizen.local/%e6%94%af%e6%8f%b4%e3%83%bb%e8%a3%9c%e5%8a%a9/', '65:b5567396a80bd8659aaccdbbd66bd7c0', 16, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '支援・補助', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:23:51', '2021-02-10 10:27:35', 1, NULL, NULL, NULL, NULL, 0, NULL),
(21, 'http://bizen.local/17/', '22:c89c282aa851669342bfe9969fcd9bb1', 17, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:29:48', '2021-02-10 10:32:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(22, 'http://bizen.local/%e5%82%99%e5%89%8d%e5%b8%82%e3%82%92%e7%9f%a5%e3%82%8d%e3%81%86-2/', '85:41f1d9a3385b3db86dc8a6de51030251', 18, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '備前市を知ろう', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:30:23', '2021-02-10 10:32:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(23, 'http://bizen.local/19/', '22:32982b59130e0c99d34fd0eb924be59a', 19, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:30:28', '2021-02-10 10:32:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(24, 'http://bizen.local/%e7%a7%bb%e4%bd%8f%e3%82%92%e8%80%83%e3%81%88%e3%82%8b-2/', '76:2b90aa7a52ad8082f634c1e29200f5d8', 20, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '移住を考える', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:30:43', '2021-02-10 10:32:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(25, 'http://bizen.local/%e6%94%af%e6%8f%b4%e3%83%bb%e8%a3%9c%e5%8a%a9-2/', '67:5dd0c4ec5f41469c80100797f71bd8b3', 21, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '支援・補助', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:30:55', '2021-02-10 10:32:05', 1, NULL, NULL, NULL, NULL, 0, NULL),
(26, 'http://bizen.local/%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b-2/', '76:c085b634856c0595a7fd2071158f3a9e', 22, 'post', 'nav_menu_item', 1, 0, NULL, NULL, 'お問い合わせ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:31:03', '2021-02-10 10:32:05', 1, NULL, NULL, NULL, NULL, 0, NULL),
(27, 'http://bizen.local/?post_type=acf-field-group&p=23', '50:b37620ff269466814684fb44274bcd4c', 23, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Site Settings', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:37:11', '2021-02-10 10:47:21', 1, NULL, NULL, NULL, NULL, 0, NULL),
(28, 'http://bizen.local/?post_type=acf-field&p=24', '44:d8eae232c2c44a0a5bc57cc247bf7c9c', 24, 'post', 'acf-field', 1, 23, NULL, NULL, 'Copyright', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:40:16', '2021-02-10 10:40:16', 1, NULL, NULL, NULL, NULL, 1, NULL),
(29, 'http://bizen.local/?post_type=acf-field&p=25', '44:cb009aa73a6c3a1c17e47a2f58e6ddac', 25, 'post', 'acf-field', 1, 23, NULL, NULL, 'PDF Download', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:40:16', '2021-02-10 10:40:16', 1, NULL, NULL, NULL, NULL, 1, NULL),
(30, 'http://bizen.local/?post_type=acf-field&p=26', '44:0f7d37326be0688907c1b8c11cd6165a', 26, 'post', 'acf-field', 1, 23, NULL, NULL, 'Contact information', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:40:16', '2021-02-10 10:40:17', 1, NULL, NULL, NULL, NULL, 1, NULL),
(31, 'http://bizen.local/?post_type=acf-field&p=27', '44:f31f95dab4b58a6119908906b6d5ac0e', 27, 'post', 'acf-field', 1, 26, NULL, NULL, 'Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:40:17', '2021-02-10 10:40:17', 1, NULL, NULL, NULL, NULL, 1, NULL),
(32, 'http://bizen.local/?post_type=acf-field&p=28', '44:c13ec8f774ddb9339742a971d9d5ee67', 28, 'post', 'acf-field', 1, 26, NULL, NULL, 'Content', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:40:17', '2021-02-10 10:40:17', 1, NULL, NULL, NULL, NULL, 1, NULL),
(33, 'http://bizen.local/site/package/uploads/bizen_specification.pdf', '63:569dc77ff1d325b42e6dd2e1c90b9497', 29, 'post', 'attachment', 1, 0, NULL, NULL, 'bizen_specification', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:41:27', '2021-02-10 10:41:27', 1, NULL, NULL, NULL, NULL, 0, NULL),
(34, 'http://bizen.local/?post_type=acf-field&p=30', '44:63348898dc7a194d237f8f9a7ee174c6', 30, 'post', 'acf-field', 1, 23, NULL, NULL, 'Reference Link', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:47:20', '2021-02-10 10:47:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(35, 'http://bizen.local/?post_type=acf-field-group&p=31', '50:dce9562e5eb1e5e270b5341c588f3972', 31, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Home Template', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:49:40', '2021-02-13 11:29:56', 1, NULL, NULL, NULL, NULL, 0, NULL),
(36, 'http://bizen.local/?post_type=acf-field&p=32', '44:2c0f8e8dfe1827795875a90a90b5c98f', 32, 'post', 'acf-field', 1, 31, NULL, NULL, 'Home Main Slider', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:51:11', '2021-02-10 10:51:11', 1, NULL, NULL, NULL, NULL, 1, NULL),
(37, 'http://bizen.local/?post_type=acf-field&p=33', '44:0393309d57ddc35c39b6bd55d04ec80c', 33, 'post', 'acf-field', 1, 31, NULL, NULL, 'Poster Image', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:51:12', '2021-02-10 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(38, 'http://bizen.local/?post_type=acf-field&p=34', '44:0b86f82e8103a88e639f29b1fe7cf19e', 34, 'post', 'acf-field', 1, 31, NULL, NULL, 'Main Slider', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:52:22', '2021-02-10 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(39, 'http://bizen.local/?post_type=acf-field&p=35', '44:3ef5c6c3cb491946c10724cf66e25fc0', 35, 'post', 'acf-field', 1, 34, NULL, NULL, 'Image', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:52:22', '2021-02-10 10:52:22', 1, NULL, NULL, NULL, NULL, 1, NULL),
(40, 'http://bizen.local/?post_type=acf-field&p=36', '44:1f40e47a80c17a760321e964b098050e', 36, 'post', 'acf-field', 1, 34, NULL, NULL, 'Slider Text', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:52:22', '2021-02-10 10:57:18', 1, NULL, NULL, NULL, NULL, 1, NULL),
(41, 'http://bizen.local/site/package/uploads/banner_asset.png', '56:f63a142cf6d20092c46c0576f7e9cf32', 37, 'post', 'attachment', 1, 7, NULL, NULL, 'banner_asset', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/banner_asset.png', NULL, '37', 'attachment-image', NULL, NULL, NULL, '37', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-10 17:53:04', '2021-02-10 10:53:04', 1, NULL, NULL, NULL, NULL, 1, NULL),
(42, 'http://bizen.local/site/package/uploads/main.jpg', '48:a516366e51d5a944b5564a5ff3af7fa2', 38, 'post', 'attachment', 1, 7, NULL, NULL, 'main', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/main.jpg', NULL, '38', 'attachment-image', NULL, NULL, NULL, '38', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-10 17:53:23', '2021-02-10 10:53:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(43, 'http://bizen.local/?post_type=acf-field&p=40', '44:e27b7105b367ce4c092fe01ae89de04f', 40, 'post', 'acf-field', 1, 34, NULL, NULL, 'Image Mobile', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 17:57:18', '2021-02-10 10:57:18', 1, NULL, NULL, NULL, NULL, 1, NULL),
(44, 'http://bizen.local/site/package/uploads/responsive_main.png', '59:87bcee7af66b325631db19970048c994', 41, 'post', 'attachment', 1, 7, NULL, NULL, 'responsive_main', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/responsive_main.png', NULL, '41', 'attachment-image', NULL, NULL, NULL, '41', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-10 17:58:14', '2021-02-10 10:58:15', 1, NULL, NULL, NULL, NULL, 1, NULL),
(45, 'http://bizen.local/?post_type=acf-field&p=43', '44:4a60ca8ae07b1d22910d6cd102426170', 43, 'post', 'acf-field', 1, 31, NULL, NULL, 'Pickup', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:46', '2021-02-10 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(46, 'http://bizen.local/?post_type=acf-field&p=44', '44:f682e33a34253ea60b339d6604fc4062', 44, 'post', 'acf-field', 1, 31, NULL, NULL, 'Pickup Text', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:46', '2021-02-10 11:06:13', 1, NULL, NULL, NULL, NULL, 1, NULL),
(47, 'http://bizen.local/?post_type=acf-field&p=45', '44:beef7763aa9102f232faff779451205f', 45, 'post', 'acf-field', 1, 31, NULL, NULL, 'Video 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:47', '2021-02-10 11:04:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(48, 'http://bizen.local/?post_type=acf-field&p=46', '44:a8af37a9c06ad64d98f120addd396d0d', 46, 'post', 'acf-field', 1, 45, NULL, NULL, 'Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:47', '2021-02-10 11:04:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(49, 'http://bizen.local/?post_type=acf-field&p=47', '44:c11bab4d1a828114fe9e91040e27ff11', 47, 'post', 'acf-field', 1, 45, NULL, NULL, 'Embed URL', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:48', '2021-02-10 11:04:48', 1, NULL, NULL, NULL, NULL, 1, NULL),
(50, 'http://bizen.local/?post_type=acf-field&p=48', '44:eef685f3857311aab754d25cffb71d73', 48, 'post', 'acf-field', 1, 45, NULL, NULL, 'Description', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:48', '2021-02-10 11:04:48', 1, NULL, NULL, NULL, NULL, 1, NULL),
(51, 'http://bizen.local/?post_type=acf-field&p=49', '44:656d3e9be0943ab87c66a45e59dfd870', 49, 'post', 'acf-field', 1, 31, NULL, NULL, 'Video 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:48', '2021-02-10 11:04:48', 1, NULL, NULL, NULL, NULL, 1, NULL),
(52, 'http://bizen.local/?post_type=acf-field&p=50', '44:e2e24a03997b510f9fd27107a3f59142', 50, 'post', 'acf-field', 1, 49, NULL, NULL, 'Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:48', '2021-02-10 11:04:48', 1, NULL, NULL, NULL, NULL, 1, NULL),
(53, 'http://bizen.local/?post_type=acf-field&p=51', '44:664b054404524648178146174abe1f0b', 51, 'post', 'acf-field', 1, 49, NULL, NULL, 'Embed URL', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:49', '2021-02-10 11:04:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(54, 'http://bizen.local/?post_type=acf-field&p=52', '44:f44f02af003f090ae0a93a7c157d2c53', 52, 'post', 'acf-field', 1, 49, NULL, NULL, 'Description', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-10 18:04:49', '2021-02-10 11:04:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(55, 'http://bizen.local/sample-1/', '28:45e00268bbbb236fb65562c1fa77d7f3', 54, 'post', 'post', 1, 0, NULL, NULL, 'お知らせお知らせお知らせお知らせお知らせお知らせ', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', '57', 'featured-image', '{\"width\":488,\"height\":320,\"url\":\"http://bizen.local/site/package/uploads/sample.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/sample.jpg\",\"size\":\"full\",\"id\":57,\"alt\":\"\",\"pixels\":156160,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-12 17:29:19', '2021-02-12 10:43:19', 1, NULL, NULL, NULL, NULL, 0, 1),
(56, 'http://bizen.local/site/package/uploads/sample_lg.jpg', '53:be3e3eeef11f4006f020d45dde7cd95e', 55, 'post', 'attachment', 1, 54, NULL, NULL, 'sample_lg', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample_lg.jpg', NULL, '55', 'attachment-image', NULL, NULL, NULL, '55', 'attachment-image', NULL, 0, 5, NULL, '2021-02-12 17:30:35', '2021-02-12 10:43:13', 1, NULL, NULL, NULL, NULL, 1, NULL),
(57, 'http://bizen.local/site/package/uploads/sample.jpg', '50:e1b2ef27ceb7c4ac89152ec2ace2091a', 57, 'post', 'attachment', 1, 54, NULL, NULL, 'sample', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'attachment-image', NULL, NULL, NULL, '57', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-12 17:31:14', '2021-02-12 10:43:13', 1, NULL, NULL, NULL, NULL, 1, NULL),
(58, 'http://bizen.local/information/other/', '37:f273f1c05d8a147597847d9c98342a0d', 8, 'term', 'category', NULL, NULL, NULL, NULL, 'その他', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 17:33:13', '2021-02-14 09:37:28', 1, NULL, NULL, NULL, NULL, 1, NULL),
(59, 'http://bizen.local/sample-2/', '28:cc6d756dbb8c307d1f874e751e30a5d4', 58, 'post', 'post', 1, 0, NULL, NULL, 'お知らせお知らせお知らせお知らせお知らせお知らせ 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', '57', 'featured-image', '{\"width\":488,\"height\":320,\"url\":\"http://bizen.local/site/package/uploads/sample.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/sample.jpg\",\"size\":\"full\",\"id\":57,\"alt\":\"\",\"pixels\":156160,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-12 17:33:23', '2021-02-12 10:43:37', 1, NULL, NULL, NULL, NULL, 0, 1),
(60, 'http://bizen.local/sample-3/', '28:21faadbe4ec2a2f2f8f2f7e092cb98bd', 60, 'post', 'post', 1, 0, NULL, NULL, 'お知らせお知らせお知らせお知らせお知らせお知らせ 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', '57', 'featured-image', '{\"width\":488,\"height\":320,\"url\":\"http://bizen.local/site/package/uploads/sample.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/sample.jpg\",\"size\":\"full\",\"id\":57,\"alt\":\"\",\"pixels\":156160,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-12 17:34:18', '2021-02-12 10:44:02', 1, NULL, NULL, NULL, NULL, 0, 1),
(61, 'http://bizen.local/sample-4/', '28:179f16d2d71bbacf0f8a7ea22b18a7f0', 62, 'post', 'post', 1, 0, NULL, NULL, 'お知らせお知らせお知らせお知らせお知らせお知らせ 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', '57', 'featured-image', '{\"width\":488,\"height\":320,\"url\":\"http://bizen.local/site/package/uploads/sample.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/sample.jpg\",\"size\":\"full\",\"id\":57,\"alt\":\"\",\"pixels\":156160,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-12 17:34:51', '2021-02-12 10:44:05', 1, NULL, NULL, NULL, NULL, 0, 1),
(62, 'http://bizen.local/sample-5/', '28:cdc592b78ef042ae22db03f027318be5', 64, 'post', 'post', 1, 0, NULL, NULL, 'お知らせお知らせお知らせお知らせお知らせお知らせ 5', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', NULL, '57', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/sample.jpg', '57', 'featured-image', '{\"width\":488,\"height\":320,\"url\":\"http://bizen.local/site/package/uploads/sample.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/sample.jpg\",\"size\":\"full\",\"id\":57,\"alt\":\"\",\"pixels\":156160,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-12 17:36:29', '2021-02-12 10:44:13', 1, NULL, NULL, NULL, NULL, 0, 1),
(63, 'http://bizen.local/?post_type=acf-field&p=67', '44:28c211cd18c795dca9bea5b875f534cc', 67, 'post', 'acf-field', 1, 31, NULL, NULL, 'Introduction', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:27', '2021-02-12 11:00:27', 1, NULL, NULL, NULL, NULL, 1, NULL),
(64, 'http://bizen.local/?post_type=acf-field&p=68', '44:a0d17c7003762cb6969dde0c079cfacf', 68, 'post', 'acf-field', 1, 31, NULL, NULL, 'Small title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:27', '2021-02-13 11:04:45', 1, NULL, NULL, NULL, NULL, 1, NULL),
(65, 'http://bizen.local/?post_type=acf-field&p=69', '44:e483dd3feb9f239a5af618130e15412e', 69, 'post', 'acf-field', 1, 31, NULL, NULL, 'Title With Icon', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:28', '2021-02-13 11:04:45', 1, NULL, NULL, NULL, NULL, 1, NULL),
(66, 'http://bizen.local/?post_type=acf-field&p=70', '44:442fa7d71527855d9f93d654a3bf9f37', 70, 'post', 'acf-field', 1, 69, NULL, NULL, 'Left Text', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:28', '2021-02-12 11:00:28', 1, NULL, NULL, NULL, NULL, 1, NULL),
(67, 'http://bizen.local/?post_type=acf-field&p=71', '44:2e9de551e950504344e935c930ddc31f', 71, 'post', 'acf-field', 1, 69, NULL, NULL, 'Icon', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:28', '2021-02-12 11:00:29', 1, NULL, NULL, NULL, NULL, 1, NULL),
(68, 'http://bizen.local/?post_type=acf-field&p=72', '44:a6fa698d62c13b761e130b1343c0163d', 72, 'post', 'acf-field', 1, 69, NULL, NULL, 'Right Text', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:29', '2021-02-12 11:00:29', 1, NULL, NULL, NULL, NULL, 1, NULL),
(69, 'http://bizen.local/?post_type=acf-field&p=73', '44:4bbdf53d4801ddfff1bffedc39ca7088', 73, 'post', 'acf-field', 1, 31, NULL, NULL, 'Section Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:29', '2021-02-13 11:04:45', 1, NULL, NULL, NULL, NULL, 1, NULL),
(70, 'http://bizen.local/?post_type=acf-field&p=74', '44:1f0be744535f78ba804c975e955976fe', 74, 'post', 'acf-field', 1, 31, NULL, NULL, 'Map', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:29', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(71, 'http://bizen.local/?post_type=acf-field&p=75', '44:9fcb72f56e4f57f6fc3b378e06242648', 75, 'post', 'acf-field', 1, 31, NULL, NULL, 'Map Infomation', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:00:29', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(72, 'http://bizen.local/?post_type=acf-field&p=76', '44:cdd5d1bf8c2fb89af86393795f62d829', 76, 'post', 'acf-field', 1, 31, NULL, NULL, 'Map Info Box 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:04:17', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(73, 'http://bizen.local/?post_type=acf-field&p=77', '44:97be2c3a61366a8d62366707e89e565e', 77, 'post', 'acf-field', 1, 31, NULL, NULL, 'Map Info Box 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:04:17', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(74, 'http://bizen.local/site/package/uploads/hand.svg', '48:5238c552e6a104ca369085229c3b09ff', 78, 'post', 'attachment', 1, 7, NULL, NULL, 'hand', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:05:18', '2021-02-12 11:05:18', 1, NULL, NULL, NULL, NULL, 1, NULL),
(75, 'http://bizen.local/site/package/uploads/map.png', '47:4822aca69806e7593c008391f44398d4', 79, 'post', 'attachment', 1, 7, NULL, NULL, 'map', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/map.png', NULL, '79', 'attachment-image', NULL, NULL, NULL, '79', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-12 18:05:38', '2021-02-12 11:05:38', 1, NULL, NULL, NULL, NULL, 1, NULL),
(76, 'http://bizen.local/?post_type=acf-field&p=82', '44:a58d92c70926ffd06bd2e7d2e6d0c42c', 82, 'post', 'acf-field', 1, 31, NULL, NULL, 'Information Boxes Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:55', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(77, 'http://bizen.local/?post_type=acf-field&p=83', '44:2dff0c34fbfb6de07025f1b11dae2990', 83, 'post', 'acf-field', 1, 31, NULL, NULL, 'Information Boxes', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:55', '2021-02-13 11:04:46', 1, NULL, NULL, NULL, NULL, 1, NULL),
(78, 'http://bizen.local/?post_type=acf-field&p=84', '44:bee7142f61358c48e1a605a68c1233d8', 84, 'post', 'acf-field', 1, 83, NULL, NULL, 'Info Text', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:55', '2021-02-12 11:15:56', 1, NULL, NULL, NULL, NULL, 1, NULL),
(79, 'http://bizen.local/?post_type=acf-field&p=85', '44:ca01dad80e4a120b2e1e3b1b9b9e3178', 85, 'post', 'acf-field', 1, 83, NULL, NULL, 'Box Background', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:56', '2021-02-12 11:17:08', 1, NULL, NULL, NULL, NULL, 1, NULL),
(80, 'http://bizen.local/?post_type=acf-field&p=86', '44:58f5c348cf8c4f89c4c4681cdcccd5e1', 86, 'post', 'acf-field', 1, 83, NULL, NULL, 'Icon', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:56', '2021-02-12 11:15:56', 1, NULL, NULL, NULL, NULL, 1, NULL),
(81, 'http://bizen.local/?post_type=acf-field&p=87', '44:61f926eb616c5deb3971e151ea7f5fc4', 87, 'post', 'acf-field', 1, 83, NULL, NULL, 'Icon Class', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:15:56', '2021-02-12 11:15:57', 1, NULL, NULL, NULL, NULL, 1, NULL),
(82, 'http://bizen.local/site/package/uploads/baby.svg', '48:562973ee8c522b55531e2f2ad1c72a71', 88, 'post', 'attachment', 1, 7, NULL, NULL, 'baby', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:18:01', '2021-02-12 11:18:01', 1, NULL, NULL, NULL, NULL, 1, NULL),
(83, 'http://bizen.local/site/package/uploads/house.svg', '49:d382eddf0b42d29d65f3259e96fd95bc', 89, 'post', 'attachment', 1, 7, NULL, NULL, 'house', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:18:02', '2021-02-12 11:18:02', 1, NULL, NULL, NULL, NULL, 1, NULL),
(84, 'http://bizen.local/site/package/uploads/man.svg', '47:ac4d003272d57ba31908930069eee403', 90, 'post', 'attachment', 1, 7, NULL, NULL, 'man', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:18:04', '2021-02-12 11:18:04', 1, NULL, NULL, NULL, NULL, 1, NULL),
(85, 'http://bizen.local/site/package/uploads/text.svg', '48:79a3115715d4e639b8f901b7b1f551b9', 91, 'post', 'attachment', 1, 7, NULL, NULL, 'text', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-12 18:18:05', '2021-02-12 11:18:05', 1, NULL, NULL, NULL, NULL, 1, NULL),
(86, 'http://bizen.local/%e6%b8%85%e5%ae%b6-%e5%bd%a9%e8%8f%9c%e3%81%95%e3%82%93/', '75:922cedc872629540a26d06402d22200c', 96, 'post', 'post', 1, 0, NULL, NULL, '清家 彩菜さん', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview1.jpg', NULL, '97', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/interview1.jpg', '97', 'featured-image', '{\"width\":535,\"height\":318,\"url\":\"http://bizen.local/site/package/uploads/interview1.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/interview1.jpg\",\"size\":\"full\",\"id\":97,\"alt\":\"\",\"pixels\":170130,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-13 17:29:38', '2021-02-13 10:53:34', 1, NULL, NULL, NULL, NULL, 0, 1),
(87, 'http://bizen.local/site/package/uploads/interview1.jpg', '54:6518f7e7322617b69e826f76dad6f064', 97, 'post', 'attachment', 1, 96, NULL, NULL, 'interview1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview1.jpg', NULL, '97', 'attachment-image', NULL, NULL, NULL, '97', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-13 17:31:15', '2021-02-13 17:33:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(88, 'http://bizen.local/site/package/uploads/interview2.jpg', '54:0bb44d9c2ac73a9a6920d075520286d1', 98, 'post', 'attachment', 1, 96, NULL, NULL, 'interview2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview2.jpg', NULL, '98', 'attachment-image', NULL, NULL, NULL, '98', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-13 17:31:16', '2021-02-13 17:33:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(89, 'http://bizen.local/site/package/uploads/interview3.jpg', '54:039d6742f0aea55b0d8435b16b5fa2df', 99, 'post', 'attachment', 1, 96, NULL, NULL, 'interview3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview3.jpg', NULL, '99', 'attachment-image', NULL, NULL, NULL, '99', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-13 17:31:18', '2021-02-13 17:33:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(90, 'http://bizen.local/site/package/uploads/interview4.jpg', '54:273c7db6ec456fb44d15d8ed7219ed09', 100, 'post', 'attachment', 1, 96, NULL, NULL, 'interview4', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview4.jpg', NULL, '100', 'attachment-image', NULL, NULL, NULL, '100', 'attachment-image', NULL, 0, NULL, NULL, '2021-02-13 17:31:20', '2021-02-13 17:33:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(91, 'http://bizen.local/?post_type=acf-field-group&p=103', '51:422323a23a9fd52bc9ff77170e9e2bbe', 103, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Interview', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 17:34:30', '2021-02-13 10:52:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(92, 'http://bizen.local/?post_type=acf-field&p=104', '45:528dec2cc10ea35066ed2ee4fc32246e', 104, 'post', 'acf-field', 1, 103, NULL, NULL, 'Area', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 17:35:23', '2021-02-13 10:38:14', 1, NULL, NULL, NULL, NULL, 1, NULL),
(93, 'http://bizen.local/?post_type=acf-field&p=105', '45:5e0e6831553b3a277d7541cffe9f1fe2', 105, 'post', 'acf-field', 1, 103, NULL, NULL, 'More information', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 17:35:24', '2021-02-13 10:38:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(94, 'http://bizen.local/?post_type=acf-field&p=107', '45:669bb3166ac06f259ceec4fe4e52111f', 107, 'post', 'acf-field', 1, 103, NULL, NULL, 'Area color', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 17:38:14', '2021-02-13 10:52:07', 1, NULL, NULL, NULL, NULL, 1, NULL),
(95, 'http://bizen.local/%e6%b1%a0%e7%94%b0-%e9%9b%84%e4%ba%8c%e3%81%95%e3%82%93%e3%83%bb%e7%94%b1%e8%a1%a3%e3%81%95%e3%82%93/', '120:d5b1d8dcecdf9fd4227ebe2addccdd99', 109, 'post', 'post', 1, 0, NULL, NULL, '池田 雄二さん・由衣さん', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview4.jpg', NULL, '100', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/interview4.jpg', '100', 'featured-image', '{\"width\":535,\"height\":318,\"url\":\"http://bizen.local/site/package/uploads/interview4.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/interview4.jpg\",\"size\":\"full\",\"id\":100,\"alt\":\"\",\"pixels\":170130,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-13 17:39:15', '2021-02-13 10:54:09', 1, NULL, NULL, NULL, NULL, 0, 1),
(96, 'http://bizen.local/%e5%a6%b9%e5%b0%be-%e6%82%a0%e5%b9%b3%e3%81%95%e3%82%93/', '75:7757056d689be1f9cf3c6352d38642da', 113, 'post', 'post', 1, 0, NULL, NULL, '妹尾 悠平さん', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview3.jpg', NULL, '99', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/interview3.jpg', '99', 'featured-image', '{\"width\":1070,\"height\":636,\"url\":\"http://bizen.local/site/package/uploads/interview3.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/interview3.jpg\",\"size\":\"full\",\"id\":99,\"alt\":\"\",\"pixels\":680520,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-13 17:41:45', '2021-02-13 10:43:27', 1, NULL, NULL, NULL, NULL, 0, 1),
(97, 'http://bizen.local/%e6%a9%8b%e6%9c%ac-%e3%81%82%e3%81%bf%e3%81%95%e3%82%93/', '75:617f4b28f909d7a2621373384ccd07c8', 116, 'post', 'post', 1, 0, NULL, NULL, '橋本 あみさん', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://bizen.local/site/package/uploads/interview2.jpg', NULL, '98', 'featured-image', NULL, NULL, 'http://bizen.local/site/package/uploads/interview2.jpg', '98', 'featured-image', '{\"width\":535,\"height\":318,\"url\":\"http://bizen.local/site/package/uploads/interview2.jpg\",\"path\":\"D:/Tools/xampp/htdocs/bizen//site/package/uploads/interview2.jpg\",\"size\":\"full\",\"id\":98,\"alt\":\"\",\"pixels\":170130,\"type\":\"image/jpeg\"}', 0, NULL, NULL, '2021-02-13 17:43:32', '2021-02-13 10:44:46', 1, NULL, NULL, NULL, NULL, 0, 1),
(98, 'http://bizen.local/?post_type=acf-field-group&p=122', '51:dd7cbec826ee48fc357244fcdc6bd52e', 122, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:00:46', '2021-02-13 11:00:46', 1, NULL, NULL, NULL, NULL, 0, NULL),
(99, 'http://bizen.local/?post_type=acf-field&p=123', '45:c922ef01ccc5748032e669874429e77d', 123, 'post', 'acf-field', 1, 31, NULL, NULL, 'How To', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:47', '2021-02-13 11:04:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(100, 'http://bizen.local/?post_type=acf-field&p=124', '45:d1b7dc748abfd85328a0111e3e32bb0b', 124, 'post', 'acf-field', 1, 31, NULL, NULL, 'Small Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:47', '2021-02-13 11:08:25', 1, NULL, NULL, NULL, NULL, 1, NULL),
(101, 'http://bizen.local/?post_type=acf-field&p=125', '45:42e63a0d43965824846318ff43794610', 125, 'post', 'acf-field', 1, 31, NULL, NULL, 'Section Title En', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:47', '2021-02-13 11:18:51', 1, NULL, NULL, NULL, NULL, 1, NULL),
(102, 'http://bizen.local/?post_type=acf-field&p=126', '45:c6fbb38ad972b63d697d42fc21d58726', 126, 'post', 'acf-field', 1, 31, NULL, NULL, 'Section Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:48', '2021-02-13 11:18:51', 1, NULL, NULL, NULL, NULL, 1, NULL),
(103, 'http://bizen.local/?post_type=acf-field&p=127', '45:4a501c00360d73df46814779ceea5b3e', 127, 'post', 'acf-field', 1, 31, NULL, NULL, '[Step 1] Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:48', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(104, 'http://bizen.local/?post_type=acf-field&p=128', '45:64562b998960094228d6b7ad5579e3f9', 128, 'post', 'acf-field', 1, 31, NULL, NULL, '[Step 2] Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:48', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(105, 'http://bizen.local/?post_type=acf-field&p=129', '45:17a080a478c47a1a4061702c6e4e8fbb', 129, 'post', 'acf-field', 1, 31, NULL, NULL, '[Step 3] Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:48', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(106, 'http://bizen.local/?post_type=acf-field&p=130', '45:fd15b3abb54f5c576fe17aead9144cd5', 130, 'post', 'acf-field', 1, 31, NULL, NULL, '[Step 4] Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:48', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(107, 'http://bizen.local/?post_type=acf-field&p=131', '45:d1e53e063530bdfc2f2a0245f4a22182', 131, 'post', 'acf-field', 1, 31, NULL, NULL, '[Step 5] Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:49', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(108, 'http://bizen.local/?post_type=acf-field&p=132', '45:f6602214f895ffd50880c6cc9b0193e1', 132, 'post', 'acf-field', 1, 31, NULL, NULL, 'Description 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:49', '2021-02-13 11:29:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(109, 'http://bizen.local/?post_type=acf-field&p=133', '45:48830afb545026f3eec4f4b68775c5ec', 133, 'post', 'acf-field', 1, 31, NULL, NULL, 'Description 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:49', '2021-02-13 11:29:53', 1, NULL, NULL, NULL, NULL, 1, NULL),
(110, 'http://bizen.local/?post_type=acf-field&p=134', '45:a6380170d335642f3a6578ff3407421c', 134, 'post', 'acf-field', 1, 31, NULL, NULL, 'Description 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:49', '2021-02-13 11:29:53', 1, NULL, NULL, NULL, NULL, 1, NULL),
(111, 'http://bizen.local/?post_type=acf-field&p=135', '45:5f3492305b67ce9fcd238e3191b66ed7', 135, 'post', 'acf-field', 1, 31, NULL, NULL, 'Description 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:49', '2021-02-13 11:29:53', 1, NULL, NULL, NULL, NULL, 1, NULL),
(112, 'http://bizen.local/?post_type=acf-field&p=136', '45:b18f75aa540e8cdf8ccf9b018c9ce7b8', 136, 'post', 'acf-field', 1, 31, NULL, NULL, 'Description 5', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:04:50', '2021-02-13 11:29:54', 1, NULL, NULL, NULL, NULL, 1, NULL),
(113, 'http://bizen.local/?post_type=acf-field&p=137', '45:53944eb1336094e2398a03cffc97e570', 137, 'post', 'acf-field', 1, 31, NULL, NULL, 'Support Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:28', '2021-02-13 11:29:54', 1, NULL, NULL, NULL, NULL, 1, NULL),
(114, 'http://bizen.local/?post_type=acf-field&p=138', '45:659f526a1be8373813d6913cdaef0f06', 138, 'post', 'acf-field', 1, 31, NULL, NULL, 'House Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:29', '2021-02-13 11:29:55', 1, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `bzn_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`, `estimated_reading_time_minutes`) VALUES
(115, 'http://bizen.local/?post_type=acf-field&p=139', '45:5bb74e89c8cdaae4a929a2b3adcd6d10', 139, 'post', 'acf-field', 1, 138, NULL, NULL, 'Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:29', '2021-02-13 11:08:29', 1, NULL, NULL, NULL, NULL, 1, NULL),
(116, 'http://bizen.local/?post_type=acf-field&p=140', '45:c1799e1fba3b263e5108c3868e38468c', 140, 'post', 'acf-field', 1, 139, NULL, NULL, 'Link', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:29', '2021-02-13 11:08:29', 1, NULL, NULL, NULL, NULL, 1, NULL),
(117, 'http://bizen.local/?post_type=acf-field&p=141', '45:a03f6477a6c4453ef3540abb3b25f9b3', 141, 'post', 'acf-field', 1, 31, NULL, NULL, 'Work Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:29', '2021-02-13 11:29:55', 1, NULL, NULL, NULL, NULL, 1, NULL),
(118, 'http://bizen.local/?post_type=acf-field&p=142', '45:f59db84ab898b114ea3041346a192195', 142, 'post', 'acf-field', 1, 141, NULL, NULL, 'Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:30', '2021-02-13 11:08:30', 1, NULL, NULL, NULL, NULL, 1, NULL),
(119, 'http://bizen.local/?post_type=acf-field&p=143', '45:3fbcbe8a1d45735fb80078f2f6c5a822', 143, 'post', 'acf-field', 1, 142, NULL, NULL, 'Link', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:30', '2021-02-13 11:08:30', 1, NULL, NULL, NULL, NULL, 1, NULL),
(120, 'http://bizen.local/?post_type=acf-field&p=144', '45:8cefb87e4ec7748e7fcd8044c14a63e5', 144, 'post', 'acf-field', 1, 31, NULL, NULL, 'Life Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:30', '2021-02-13 11:29:55', 1, NULL, NULL, NULL, NULL, 1, NULL),
(121, 'http://bizen.local/?post_type=acf-field&p=145', '45:0c66487f05d6d17582d57d307ca1824d', 145, 'post', 'acf-field', 1, 144, NULL, NULL, 'Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:30', '2021-02-13 11:08:30', 1, NULL, NULL, NULL, NULL, 1, NULL),
(122, 'http://bizen.local/?post_type=acf-field&p=146', '45:5e2a0dbf91a355787f724fe6e90b5804', 146, 'post', 'acf-field', 1, 145, NULL, NULL, 'Link', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:08:31', '2021-02-13 11:08:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(129, 'http://bizen.local/?post_type=acf-field&p=153', '45:b7e27f51106536246de02d7529ba373c', 153, 'post', 'acf-field', 1, 31, NULL, NULL, 'Childs Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:12:41', '2021-02-13 11:29:56', 1, NULL, NULL, NULL, NULL, 1, NULL),
(130, 'http://bizen.local/?post_type=acf-field&p=154', '45:91c2a178387fcfe54b4cffd650e4d175', 154, 'post', 'acf-field', 1, 153, NULL, NULL, 'Links', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:12:42', '2021-02-13 11:12:42', 1, NULL, NULL, NULL, NULL, 1, NULL),
(131, 'http://bizen.local/?post_type=acf-field&p=155', '45:043f6fb26b24b681a2203543c899ce44', 155, 'post', 'acf-field', 1, 154, NULL, NULL, 'Link', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:12:42', '2021-02-13 11:12:42', 1, NULL, NULL, NULL, NULL, 1, NULL),
(132, 'http://bizen.local/?post_type=acf-field&p=156', '45:1e2d3624d7b54c46d98fee3c1a878574', 156, 'post', 'acf-field', 1, 31, NULL, NULL, 'Section Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:18:52', '2021-02-13 11:18:52', 1, NULL, NULL, NULL, NULL, 1, NULL),
(133, 'http://bizen.local/?post_type=acf-field&p=158', '45:b41428b4a01a8c163bc6ed7454f3a849', 158, 'post', 'acf-field', 1, 31, NULL, NULL, 'Section Title', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-02-13 18:29:54', '2021-02-13 11:29:55', 1, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_yoast_indexable_hierarchy`
--

CREATE TABLE `bzn_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_yoast_indexable_hierarchy`
--

INSERT INTO `bzn_yoast_indexable_hierarchy` (`indexable_id`, `ancestor_id`, `depth`, `blog_id`) VALUES
(16, 11, 1, 1),
(17, 11, 1, 1),
(28, 27, 1, 1),
(29, 27, 1, 1),
(30, 27, 1, 1),
(31, 27, 2, 1),
(31, 30, 1, 1),
(32, 27, 2, 1),
(32, 30, 1, 1),
(34, 27, 1, 1),
(36, 35, 1, 1),
(37, 35, 1, 1),
(38, 35, 1, 1),
(39, 35, 2, 1),
(39, 38, 1, 1),
(40, 35, 2, 1),
(40, 38, 1, 1),
(41, 3, 1, 1),
(42, 3, 1, 1),
(43, 35, 2, 1),
(43, 38, 1, 1),
(44, 3, 1, 1),
(45, 35, 1, 1),
(46, 35, 1, 1),
(47, 35, 1, 1),
(48, 35, 2, 1),
(48, 47, 1, 1),
(49, 35, 2, 1),
(49, 47, 1, 1),
(50, 35, 2, 1),
(50, 47, 1, 1),
(51, 35, 1, 1),
(52, 35, 2, 1),
(52, 51, 1, 1),
(53, 35, 2, 1),
(53, 51, 1, 1),
(54, 35, 2, 1),
(54, 51, 1, 1),
(56, 55, 1, 1),
(57, 55, 1, 1),
(58, 11, 1, 1),
(63, 35, 1, 1),
(64, 35, 1, 1),
(65, 35, 1, 1),
(66, 35, 2, 1),
(66, 65, 1, 1),
(67, 35, 2, 1),
(67, 65, 1, 1),
(68, 35, 2, 1),
(68, 65, 1, 1),
(69, 35, 1, 1),
(70, 35, 1, 1),
(71, 35, 1, 1),
(72, 35, 1, 1),
(73, 35, 1, 1),
(74, 3, 1, 1),
(75, 3, 1, 1),
(76, 35, 1, 1),
(77, 35, 1, 1),
(78, 35, 2, 1),
(78, 77, 1, 1),
(79, 35, 2, 1),
(79, 77, 1, 1),
(80, 35, 2, 1),
(80, 77, 1, 1),
(81, 35, 2, 1),
(81, 77, 1, 1),
(82, 3, 1, 1),
(83, 3, 1, 1),
(84, 3, 1, 1),
(85, 3, 1, 1),
(87, 86, 1, 1),
(88, 86, 1, 1),
(89, 86, 1, 1),
(90, 86, 1, 1),
(92, 91, 1, 1),
(93, 91, 1, 1),
(94, 91, 1, 1),
(99, 35, 1, 1),
(100, 35, 1, 1),
(101, 35, 1, 1),
(102, 35, 1, 1),
(103, 35, 1, 1),
(104, 35, 1, 1),
(105, 35, 1, 1),
(106, 35, 1, 1),
(107, 35, 1, 1),
(108, 35, 1, 1),
(109, 35, 1, 1),
(110, 35, 1, 1),
(111, 35, 1, 1),
(112, 35, 1, 1),
(113, 35, 1, 1),
(114, 35, 1, 1),
(115, 35, 2, 1),
(115, 114, 1, 1),
(116, 35, 3, 1),
(116, 114, 2, 1),
(116, 115, 1, 1),
(117, 35, 1, 1),
(118, 35, 2, 1),
(118, 117, 1, 1),
(119, 35, 3, 1),
(119, 117, 2, 1),
(119, 118, 1, 1),
(120, 35, 1, 1),
(121, 35, 2, 1),
(121, 120, 1, 1),
(122, 35, 3, 1),
(122, 120, 2, 1),
(122, 121, 1, 1),
(129, 35, 1, 1),
(130, 35, 2, 1),
(130, 129, 1, 1),
(131, 35, 3, 1),
(131, 129, 2, 1),
(131, 130, 1, 1),
(132, 35, 1, 1),
(133, 35, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_yoast_migrations`
--

CREATE TABLE `bzn_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_yoast_migrations`
--

INSERT INTO `bzn_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334'),
(19, '20201202144329'),
(20, '20201216124002'),
(21, '20201216141134');

-- --------------------------------------------------------

--
-- Table structure for table `bzn_yoast_primary_term`
--

CREATE TABLE `bzn_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `term_id` bigint(20) DEFAULT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bzn_yoast_primary_term`
--

INSERT INTO `bzn_yoast_primary_term` (`id`, `post_id`, `term_id`, `taxonomy`, `created_at`, `updated_at`, `blog_id`) VALUES
(1, 54, 5, 'category', '2021-02-12 17:31:35', '2021-02-12 10:43:19', 1),
(2, 58, 8, 'category', '2021-02-12 17:33:23', '2021-02-12 10:43:36', 1),
(3, 60, 6, 'category', '2021-02-12 17:34:19', '2021-02-12 10:44:02', 1),
(4, 62, 5, 'category', '2021-02-12 17:34:52', '2021-02-12 10:44:04', 1),
(5, 64, 8, 'category', '2021-02-12 17:36:31', '2021-02-12 10:44:13', 1),
(6, 96, 3, 'category', '2021-02-13 17:33:52', '2021-02-13 10:53:33', 1),
(7, 109, 3, 'category', '2021-02-13 17:40:57', '2021-02-13 10:54:09', 1),
(8, 113, 3, 'category', '2021-02-13 17:42:58', '2021-02-13 10:43:27', 1),
(9, 116, 3, 'category', '2021-02-13 17:44:45', '2021-02-13 10:44:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bzn_yoast_seo_links`
--

CREATE TABLE `bzn_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `target_post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) DEFAULT NULL,
  `region` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bzn_yoast_seo_links`
--

INSERT INTO `bzn_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`, `indexable_id`, `target_indexable_id`, `height`, `width`, `size`, `language`, `region`) VALUES
(28, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 54, 55, 'image-in', 55, 56, 406, 866, 258966, NULL, NULL),
(30, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 58, 55, 'image-in', 59, 56, 406, 866, 258966, NULL, NULL),
(33, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 60, 55, 'image-in', 60, 56, 406, 866, 258966, NULL, NULL),
(34, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 62, 55, 'image-in', 61, 56, 406, 866, 258966, NULL, NULL),
(36, 'http://bizen.local/site/package/uploads/sample_lg.jpg', 64, 55, 'image-in', 62, 56, 406, 866, 258966, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bzn_actionscheduler_actions`
--
ALTER TABLE `bzn_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `bzn_actionscheduler_claims`
--
ALTER TABLE `bzn_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `bzn_actionscheduler_groups`
--
ALTER TABLE `bzn_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `bzn_actionscheduler_logs`
--
ALTER TABLE `bzn_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `bzn_commentmeta`
--
ALTER TABLE `bzn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bzn_comments`
--
ALTER TABLE `bzn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `bzn_itsec_bans`
--
ALTER TABLE `bzn_itsec_bans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `host` (`host`),
  ADD KEY `actor` (`actor_type`,`actor_id`);

--
-- Indexes for table `bzn_itsec_distributed_storage`
--
ALTER TABLE `bzn_itsec_distributed_storage`
  ADD PRIMARY KEY (`storage_id`),
  ADD UNIQUE KEY `storage_group__key__chunk` (`storage_group`,`storage_key`,`storage_chunk`);

--
-- Indexes for table `bzn_itsec_fingerprints`
--
ALTER TABLE `bzn_itsec_fingerprints`
  ADD PRIMARY KEY (`fingerprint_id`),
  ADD UNIQUE KEY `fingerprint_user__hash` (`fingerprint_user`,`fingerprint_hash`),
  ADD UNIQUE KEY `fingerprint_uuid` (`fingerprint_uuid`);

--
-- Indexes for table `bzn_itsec_geolocation_cache`
--
ALTER TABLE `bzn_itsec_geolocation_cache`
  ADD PRIMARY KEY (`location_id`),
  ADD UNIQUE KEY `location_host` (`location_host`),
  ADD KEY `location_time` (`location_time`);

--
-- Indexes for table `bzn_itsec_lockouts`
--
ALTER TABLE `bzn_itsec_lockouts`
  ADD PRIMARY KEY (`lockout_id`),
  ADD KEY `lockout_expire_gmt` (`lockout_expire_gmt`),
  ADD KEY `lockout_host` (`lockout_host`),
  ADD KEY `lockout_user` (`lockout_user`),
  ADD KEY `lockout_username` (`lockout_username`),
  ADD KEY `lockout_active` (`lockout_active`);

--
-- Indexes for table `bzn_itsec_logs`
--
ALTER TABLE `bzn_itsec_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`),
  ADD KEY `code` (`code`),
  ADD KEY `type` (`type`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `init_timestamp` (`init_timestamp`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Indexes for table `bzn_itsec_mutexes`
--
ALTER TABLE `bzn_itsec_mutexes`
  ADD PRIMARY KEY (`mutex_id`),
  ADD UNIQUE KEY `mutex_name` (`mutex_name`);

--
-- Indexes for table `bzn_itsec_opaque_tokens`
--
ALTER TABLE `bzn_itsec_opaque_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD UNIQUE KEY `token_hashed` (`token_hashed`);

--
-- Indexes for table `bzn_itsec_temp`
--
ALTER TABLE `bzn_itsec_temp`
  ADD PRIMARY KEY (`temp_id`),
  ADD KEY `temp_date_gmt` (`temp_date_gmt`),
  ADD KEY `temp_host` (`temp_host`),
  ADD KEY `temp_user` (`temp_user`),
  ADD KEY `temp_username` (`temp_username`);

--
-- Indexes for table `bzn_itsec_user_groups`
--
ALTER TABLE `bzn_itsec_user_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `bzn_links`
--
ALTER TABLE `bzn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `bzn_options`
--
ALTER TABLE `bzn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `bzn_postmeta`
--
ALTER TABLE `bzn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bzn_posts`
--
ALTER TABLE `bzn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `bzn_termmeta`
--
ALTER TABLE `bzn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bzn_terms`
--
ALTER TABLE `bzn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `bzn_term_relationships`
--
ALTER TABLE `bzn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `bzn_term_taxonomy`
--
ALTER TABLE `bzn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `bzn_usermeta`
--
ALTER TABLE `bzn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bzn_users`
--
ALTER TABLE `bzn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `bzn_wpmailsmtp_tasks_meta`
--
ALTER TABLE `bzn_wpmailsmtp_tasks_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bzn_yoast_indexable`
--
ALTER TABLE `bzn_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`),
  ADD KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  ADD KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  ADD KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`);

--
-- Indexes for table `bzn_yoast_indexable_hierarchy`
--
ALTER TABLE `bzn_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- Indexes for table `bzn_yoast_migrations`
--
ALTER TABLE `bzn_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bzn_yoast_migrations_version` (`version`);

--
-- Indexes for table `bzn_yoast_primary_term`
--
ALTER TABLE `bzn_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- Indexes for table `bzn_yoast_seo_links`
--
ALTER TABLE `bzn_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`),
  ADD KEY `indexable_link_direction` (`indexable_id`,`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bzn_actionscheduler_actions`
--
ALTER TABLE `bzn_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bzn_actionscheduler_claims`
--
ALTER TABLE `bzn_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `bzn_actionscheduler_groups`
--
ALTER TABLE `bzn_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bzn_actionscheduler_logs`
--
ALTER TABLE `bzn_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bzn_commentmeta`
--
ALTER TABLE `bzn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_comments`
--
ALTER TABLE `bzn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_bans`
--
ALTER TABLE `bzn_itsec_bans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_distributed_storage`
--
ALTER TABLE `bzn_itsec_distributed_storage`
  MODIFY `storage_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_fingerprints`
--
ALTER TABLE `bzn_itsec_fingerprints`
  MODIFY `fingerprint_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_geolocation_cache`
--
ALTER TABLE `bzn_itsec_geolocation_cache`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_lockouts`
--
ALTER TABLE `bzn_itsec_lockouts`
  MODIFY `lockout_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_logs`
--
ALTER TABLE `bzn_itsec_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `bzn_itsec_mutexes`
--
ALTER TABLE `bzn_itsec_mutexes`
  MODIFY `mutex_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_itsec_temp`
--
ALTER TABLE `bzn_itsec_temp`
  MODIFY `temp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `bzn_links`
--
ALTER TABLE `bzn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_options`
--
ALTER TABLE `bzn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=565;

--
-- AUTO_INCREMENT for table `bzn_postmeta`
--
ALTER TABLE `bzn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1827;

--
-- AUTO_INCREMENT for table `bzn_posts`
--
ALTER TABLE `bzn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `bzn_termmeta`
--
ALTER TABLE `bzn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bzn_terms`
--
ALTER TABLE `bzn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bzn_term_taxonomy`
--
ALTER TABLE `bzn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bzn_usermeta`
--
ALTER TABLE `bzn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `bzn_users`
--
ALTER TABLE `bzn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bzn_wpmailsmtp_tasks_meta`
--
ALTER TABLE `bzn_wpmailsmtp_tasks_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bzn_yoast_indexable`
--
ALTER TABLE `bzn_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `bzn_yoast_migrations`
--
ALTER TABLE `bzn_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `bzn_yoast_primary_term`
--
ALTER TABLE `bzn_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bzn_yoast_seo_links`
--
ALTER TABLE `bzn_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
